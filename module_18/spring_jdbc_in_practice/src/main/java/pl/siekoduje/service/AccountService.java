package pl.siekoduje.service;

import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import pl.siekoduje.model.Account;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final JdbcTemplate jdbcTemplate;
    private static final String URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASSWORD = "postgres";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS accounts";
    private static final String CREATE_TABLE = "CREATE TABLE accounts (\n" +
            "id SERIAL PRIMARY KEY,\n" +
            "accountNumber VARCHAR,\n" +
            "countryCode VARCHAR,\n" +
            "checkDigit INTEGER,\n" +
            "balance DECIMAL,\n" +
            "debitLimit DOUBLE PRECISION,\n" +
            "updateTimestamp TIMESTAMP,\n" +
            "creationDate DATE,\n" +
            "creationTime TIME\n" +
            ") ";
    private static final String INSERT_SQL = "INSERT INTO accounts ( accountNumber, countryCode, checkDigit, balance, debitLimit, updateTimestamp, creationDate, creationTime) VALUES (?,?,?,?,?,?,?,?)";
    private static final String UPDATE_SQL = "UPDATE accounts SET accountNumber = ?,  countryCode = ?, checkDigit = ?, balance = ?, debitLimit = ?, updateTimestamp = ?, creationDate = ?, creationTime = ?"
            + " WHERE id = ? ";
    private static final String DELETE_SQL = "DELETE FROM accounts WHERE id = ?";
    private static final String SELECT_ALL_SQL = "SELECT * FROM accounts";
    private static final String SELECT_BY_NATALIA_SQL = "SELECT * FROM accounts\n" +
            "WHERE balance > 2000\n" +
            "AND balance <= 300000\n" +
            "AND updateTimestamp >= '2021-03-01'";

    public void reCreateAccountsTable() {
        jdbcTemplate.execute(DROP_TABLE);
        jdbcTemplate.execute(CREATE_TABLE);
    }

    public int create(Account account) {
        return jdbcTemplate.update(INSERT_SQL,
                account.getAccountNumber(),
                account.getCountryCode(),
                account.getCheckDigit(),
                account.getBalance(),
                account.getDebitLimit(),
                account.getUpdateTimestamp(),
                account.getCreationDate(),
                account.getCreationTime());
    }

    public int update(int id, Account account) {
        return jdbcTemplate.update(UPDATE_SQL,
                account.getAccountNumber(),
                account.getCountryCode(),
                account.getCheckDigit(),
                account.getBalance(),
                account.getDebitLimit(),
                account.getUpdateTimestamp(),
                account.getCreationDate(),
                account.getCreationTime(),
                id);
    }

    public int delete(int id) {
        return jdbcTemplate.update(DELETE_SQL, id);
    }

    public List<Account> queryAll() {
        return getSelectedAccounts(SELECT_ALL_SQL);
    }

    public List<Account> queryByNataliaRequirements() {
        return getSelectedAccounts(SELECT_BY_NATALIA_SQL);
    }

    private List<Account> getSelectedAccounts(String selectSql) {
        return jdbcTemplate.query(
                selectSql,
                new RowMapper<Account>() {
                    @Override
                    public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return Account.builder()
                                .id(rs.getInt("id"))
                                .accountNumber(rs.getString("accountNumber"))
                                .balance(rs.getBigDecimal("balance"))
                                .checkDigit(rs.getInt("checkDigit"))
                                .countryCode(rs.getString("countryCode"))
                                .updateTimestamp(rs.getTimestamp("updateTimestamp"))
                                .creationDate(rs.getDate("creationDate"))
                                .creationTime(rs.getTime("creationTime"))
                                .build();
                    }
                });
    }
}
