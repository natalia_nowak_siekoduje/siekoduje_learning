package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;


@AllArgsConstructor
@Builder
@Data
public class Account {
    private final int id;
    private String accountNumber;
    private String countryCode;
    private int checkDigit;
    private BigDecimal balance;
    private double debitLimit;
    private Timestamp updateTimestamp;
    private Date creationDate;
    private Time creationTime;
}
