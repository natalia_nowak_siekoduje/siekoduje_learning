
INSERT INTO accounts (accountNumber, countryCode, checkDigit, balance, debitLimit, updateTimestamp, creationDate, creationTime)
VALUES ('111111111111111', 'PL', 20, 100000, 2000, TO_TIMESTAMP('2021-03-11 11:20:10',  'YYYY-MM-DD HH24:MI:SS'), '2009-11-11', '11:11');

INSERT INTO accounts (accountNumber, countryCode, checkDigit, balance, debitLimit, updateTimestamp, creationDate, creationTime)
VALUES ('111111111111112', 'PL', 20, 22222, 1000, TO_TIMESTAMP('2021-03-12 11:20:10',  'YYYY-MM-DD HH24:MI:SS'), '2020-11-11', '11:11');