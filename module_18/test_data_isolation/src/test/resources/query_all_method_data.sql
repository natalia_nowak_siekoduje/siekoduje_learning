
INSERT INTO accounts (accountNumber, countryCode, checkDigit, balance, debitLimit, updateTimestamp, creationDate, creationTime)
VALUES ('111111111111114', 'USD', 20, 111111, 10000, TO_TIMESTAMP('2021-02-11 11:20:10',  'YYYY-MM-DD HH24:MI:SS'), '2009-11-11', '11:11');

INSERT INTO accounts (accountNumber, countryCode, checkDigit, balance, debitLimit, updateTimestamp, creationDate, creationTime)
VALUES ('111111111111112', 'PL', 20, 22222, 1000, TO_TIMESTAMP('2020-12-12 11:20:10',  'YYYY-MM-DD HH24:MI:SS'), '2020-11-11', '12:12');