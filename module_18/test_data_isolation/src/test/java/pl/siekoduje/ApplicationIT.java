package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import pl.siekoduje.model.Account;
import pl.siekoduje.service.AccountService;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

@SpringBootTest
@Slf4j
@Sql("classpath:test_schema.sql")
@Sql(
        scripts = "classpath:clear_table.sql",
        executionPhase = AFTER_TEST_METHOD
)
@SqlMergeMode(MERGE)
class ApplicationIT {
    private final String SELECT_SQL = "SELECT * FROM accounts WHERE id = ?";
    @Autowired
    private AccountService accountService;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() throws SQLException {
        log.info("DB details: {}", dataSource.getConnection().getMetaData());
    }

    @Sql("classpath:insert_method_data.sql")
    @Test
    public void shouldInsert() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111113")
                .checkDigit(30)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
                .creationDate(Date.valueOf("2012-12-12"))
                .creationTime(Time.valueOf(LocalTime.of(10, 12)))
                .balance(BigDecimal.valueOf(20L))
                .debitLimit(500)
                .build();
        //when
        int affectedRows = accountService.create(account);
        //then
        assertThat(affectedRows).isEqualTo(1);
        assertThat(getSelectedAccount(2).get(0).getId()).isEqualTo(2);
        assertThat(getSelectedAccount(2).get(0).getAccountNumber()).isEqualTo(account.getAccountNumber());
        assertThat(getSelectedAccount(2).get(0).getCountryCode()).isEqualTo(account.getCountryCode());
        assertThat(getSelectedAccount(2).get(0).getCheckDigit()).isEqualTo(account.getCheckDigit());
        assertThat(getSelectedAccount(2).get(0).getBalance()).isEqualTo(account.getBalance());
        assertThat(getSelectedAccount(2).get(0).getCreationDate()).isEqualTo(account.getCreationDate());
        assertThat(getSelectedAccount(2).get(0).getCreationTime()).isEqualTo(account.getCreationTime());
        assertThat(getSelectedAccount(2).get(0).getUpdateTimestamp()).isEqualTo(account.getUpdateTimestamp());
    }


    @Sql("classpath:update_method_data.sql")
    @Test
    public void shouldUpdate() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111113")
                .checkDigit(20)
                .countryCode("USD")
                .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
                .creationDate(Date.valueOf("2012-12-12"))
                .creationTime(Time.valueOf(LocalTime.of(10, 12)))
                .balance(BigDecimal.valueOf(20L))
                .debitLimit(500)
                .build();
        //when
        int affectedRows = accountService.update(1, account);
        //then
        assertThat(affectedRows).isEqualTo(1);
        assertThat(getSelectedAccount(1).get(0).getId()).isEqualTo(1);
        assertThat(getSelectedAccount(1).get(0).getAccountNumber()).isEqualTo(account.getAccountNumber());
        assertThat(getSelectedAccount(1).get(0).getCountryCode()).isEqualTo(account.getCountryCode());
        assertThat(getSelectedAccount(1).get(0).getCheckDigit()).isEqualTo(account.getCheckDigit());
        assertThat(getSelectedAccount(1).get(0).getBalance()).isEqualTo(account.getBalance());
        assertThat(getSelectedAccount(1).get(0).getCreationDate()).isEqualTo(account.getCreationDate());
        assertThat(getSelectedAccount(1).get(0).getCreationTime()).isEqualTo(account.getCreationTime());
        assertThat(getSelectedAccount(1).get(0).getUpdateTimestamp()).isEqualTo(account.getUpdateTimestamp());
    }


    @Sql("classpath:delete_method_data.sql")
    @Test
    public void shouldDelete() {
        //given
        //when
        int affectedRows = accountService.delete(1);
        //then
        assertThat(affectedRows).isEqualTo(1);
        assertThat(getSelectedAccount(1)).isEmpty();

    }


    @Sql(scripts = "classpath:query_all_method_data.sql")
    @Test
    public void shouldQueryAll() {
        //given
        //when
        List<Account> customers = accountService.queryAll();
        //then
        assertThat(customers).hasSize(2);
    }


    @Sql("classpath:query_by_natalia_method_data.sql")
    @Test
    public void shouldQueryByNataliaRequirements() {
        //given
        accountService.create(
                Account.builder()
                        .accountNumber("111111111111118")
                        .checkDigit(00)
                        .countryCode("PL")
                        .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
                        .creationDate(Date.valueOf("2012-12-12"))
                        .creationTime(Time.valueOf(LocalTime.of(12, 12)))
                        .balance(BigDecimal.valueOf(20000L))
                        .debitLimit(5000)
                        .build());
        //when
        List<Account> customers = accountService.queryByNataliaRequirements();
        //then
        assertThat(customers).hasSize(2);
    }

    private List<Account> getSelectedAccount(int id) {

        return jdbcTemplate.query(
                SELECT_SQL, new Object[]{id},
                new RowMapper<Account>() {
                    @Override
                    public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return Account.builder()
                                .id(rs.getInt("id"))
                                .accountNumber(rs.getString("accountNumber"))
                                .balance(rs.getBigDecimal("balance"))
                                .checkDigit(rs.getInt("checkDigit"))
                                .countryCode(rs.getString("countryCode"))
                                .updateTimestamp(rs.getTimestamp("updateTimestamp"))
                                .creationDate(rs.getDate("creationDate"))
                                .creationTime(rs.getTime("creationTime"))
                                .build();
                    }
                });
    }

}