package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;
import org.springframework.test.annotation.DirtiesContext;
import pl.siekoduje.model.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.siekoduje.service.AccountService;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Slf4j
class ApplicationIT {
    @Autowired private AccountService accountService;

    @Autowired private DataSource dataSource;

    @BeforeEach
    public void setUp() throws SQLException {
        log.info("DB details: {}", dataSource.getConnection().getMetaData());
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @Test
    public void shouldInsert() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111113")
                .checkDigit(30)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
                .creationDate(Date.valueOf("2012-12-12"))
                .creationTime(Time.valueOf(LocalTime.of(10, 12)))
                .balance(BigDecimal.valueOf(20L))
                .debitLimit(500)
                .build();
        //when
        int affectedRows = accountService.create(account);
        //then
        assertThat(affectedRows).isEqualTo(1);
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @Test
    public void shouldUpdate() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111111")
                .checkDigit(20)
                .countryCode("USD")
                .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
                .creationDate(Date.valueOf("2012-12-12"))
                .creationTime(Time.valueOf(LocalTime.of(10, 12)))
                .balance(BigDecimal.valueOf(20L))
                .debitLimit(500)
                .build();
        //when
        int affectedRows = accountService.update(1, account);
        //then
        assertThat(affectedRows).isEqualTo(1);
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @Test
    public void shouldDelete() {
        //given
        //when
        int affectedRows = accountService.delete(2);
        //then
        assertThat(affectedRows).isEqualTo(1);
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @Test
    public void shouldQueryAll() {
        //given
        //when
        List<Account> customers = accountService.queryAll();
        //then
        assertThat(customers).hasSize(2);
    }

    @DirtiesContext(methodMode = DirtiesContext.MethodMode.AFTER_METHOD)
    @Test
    public void shouldQueryByNataliaRequirements() {
        //given
        accountService.create(
                Account.builder()
                        .accountNumber("111111111111118")
                        .checkDigit(00)
                        .countryCode("PL")
                        .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
                        .creationDate(Date.valueOf("2012-12-12"))
                        .creationTime(Time.valueOf(LocalTime.of(12, 12)))
                        .balance(BigDecimal.valueOf(20000L))
                        .debitLimit(5000)
                        .build());
        //when
        List<Account> customers = accountService.queryByNataliaRequirements();
        //then
        assertThat(customers).hasSize(2);
    }

}