DROP TABLE IF EXISTS accounts;
CREATE TABLE accounts (
            id SERIAL PRIMARY KEY,
            accountNumber VARCHAR,
            countryCode VARCHAR,
            checkDigit INTEGER,
            balance DECIMAL,
            debitLimit DOUBLE PRECISION,
            updateTimestamp TIMESTAMP,
            creationDate DATE,
            creationTime TIME
            );