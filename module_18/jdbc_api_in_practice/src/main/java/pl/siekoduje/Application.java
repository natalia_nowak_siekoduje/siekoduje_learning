package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalTime;


@Slf4j
public class Application {

    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "postgres";
        try (Connection con = DriverManager.getConnection(url, user, password)) {
            dropTable(con);
            createTable(con);
            insertData(con);
            updateData(con);
            queryAll(con);
            queryByNataliaRequirements(con);
            deleteData(con);

        } catch (SQLException e) {
            log.warn("SQL exception", e);
        }
    }

    private static void dropTable(Connection con) {
        String dropTable = "DROP TABLE IF EXISTS accounts";
        try (PreparedStatement pstmt = con.prepareStatement(dropTable)) {
            pstmt.execute();
        } catch (SQLException e) {
            log.warn("Unable to execute DROP TABLE SQL statement", e);
            throw new RuntimeException();
        }
    }

    private static void createTable(Connection con) {

        String createTable = "CREATE TABLE accounts (\n" +
                "id SERIAL PRIMARY KEY,\n" +
                "accountNumber VARCHAR,\n" +
                "countryCode VARCHAR,\n" +
                "checkDigit INTEGER,\n" +
                "balance DECIMAL,\n" +
                "debitLimit DOUBLE PRECISION,\n" +
                "updateTimestamp TIMESTAMP,\n" +
                "creationDate DATE,\n" +
                "creationTime TIME\n" +
                ") ";
        try (PreparedStatement pstmt = con.prepareStatement(createTable)) {
            pstmt.execute();
        } catch (SQLException e) {
            log.warn("Unable to execute CREATE TABLE SQL statement", e);
            throw new RuntimeException();
        }
    }

    private static void insertData(Connection con) {
        String insertSql = "INSERT INTO accounts ( accountNumber, countryCode, checkDigit, balance, debitLimit, updateTimestamp, creationDate, creationTime) VALUES (?,?,?,?,?,?,?,?)";
        try (PreparedStatement pstmt = con.prepareStatement(insertSql)) {
            pstmt.setString(1, "111111111111111");
            pstmt.setString(2, "PL");
            pstmt.setInt(3, 20);
            pstmt.setBigDecimal(4, BigDecimal.valueOf(10000L));
            pstmt.setDouble(5, 20000);
            pstmt.setTimestamp(6, Timestamp.valueOf("2021-02-02 11:11:11"));
            pstmt.setDate(7, Date.valueOf("2020-12-12"));
            pstmt.setTime(8, Time.valueOf(LocalTime.of(11, 12)));
            log.info("Inserted entries {}", pstmt.executeUpdate());
            pstmt.setString(1, "111111111111112");
            pstmt.setString(2, "USD");
            pstmt.setInt(3, 20);
            pstmt.setBigDecimal(4, BigDecimal.valueOf(10009L));
            pstmt.setDouble(5, 2000);
            pstmt.setTimestamp(6, Timestamp.valueOf("2021-02-28 11:11:11"));
            pstmt.setDate(7, Date.valueOf("2020-12-12"));
            pstmt.setTime(8, Time.valueOf(LocalTime.of(11, 12)));
            log.info("Inserted entries {}", pstmt.executeUpdate());
        } catch (SQLException e) {
            log.warn("Unable to execute INSERT INTO SQL statement", e);
            throw new RuntimeException();
        }
    }

    private static void updateData(Connection con) {
        String updateSql = "UPDATE accounts SET accountNumber = ?,  countryCode = ?, checkDigit = ?, balance = ?, debitLimit = ?, updateTimestamp = ?, creationDate = ?, creationTime = ?"
                + " WHERE id = ? ";
        try (PreparedStatement pstmt = con.prepareStatement(updateSql)) {
            pstmt.setString(1, "1111111111111133");
            pstmt.setString(2, "USD");
            pstmt.setInt(3, 20);
            pstmt.setBigDecimal(4, BigDecimal.valueOf(10009L));
            pstmt.setDouble(5, 2000);
            pstmt.setTimestamp(6, Timestamp.valueOf("2021-03-28 11:11:11"));
            pstmt.setDate(7, Date.valueOf("2020-12-12"));
            pstmt.setTime(8, Time.valueOf(LocalTime.of(11, 12)));
            pstmt.setInt(9, 1);
            log.info("Updated entries {}", pstmt.executeUpdate());
        } catch (SQLException e) {
            log.warn("Unable to execute UPDATE SQL statement", e);
            throw new RuntimeException();
        }

    }

    private static void deleteData(Connection con) {
        String deleteSql = "DELETE FROM accounts WHERE id = ?";
        try (PreparedStatement pstmt = con.prepareStatement(deleteSql)) {
            pstmt.setInt(1, 1);
            log.info("Deleted entries {}", pstmt.executeUpdate());
        } catch (SQLException e) {
            log.warn("Unable to execute DELETE SQL statement", e);
            throw new RuntimeException();
        }
    }

        private static void queryAll (Connection con) {
            String selectSql = "SELECT * FROM accounts";
            try (PreparedStatement pstmt = con.prepareStatement(selectSql)) {
                try (ResultSet rs = pstmt.executeQuery()) {
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        String accountNumber = rs.getString("accountNumber");
                        BigDecimal balance = rs.getBigDecimal("balance");
                        int checkDigit = rs.getInt("checkDigit");
                        String countryCode = rs.getString("countryCode");
                        Timestamp updateTimestamp = rs.getTimestamp("updateTimestamp");
                        Date creationDate = rs.getDate("creationDate");
                        Time creationTime = rs.getTime("creationTime");
                        log.info("id: {},\n number; {},\n balance: {},\n checkDigit: {},\n countryCode: {},\n updateTimestamp: {},\n creationDate: {},\n creationTime: {}", id, accountNumber, balance, checkDigit, countryCode, updateTimestamp, creationDate, creationTime);
                    }
                } catch (SQLException e) {
                    log.warn("Unable to read result set", e);
                    throw new RuntimeException();
                }
            } catch (SQLException e) {
                log.warn("Unable to execute SELECT SQL statement", e);
                throw new RuntimeException();
            }
        }
    private static void queryByNataliaRequirements (Connection con) {
        String selectSql = "SELECT * FROM accounts\n" +
                "WHERE balance > 2000\n" +
                "AND balance <= 300000\n" +
                "AND updateTimestamp >= '2021-03-01';";
        try (PreparedStatement pstmt = con.prepareStatement(selectSql)) {
            try (ResultSet rs = pstmt.executeQuery()) {
                while (rs.next()) {
                    int id = rs.getInt("id");
                    String accountNumber = rs.getString("accountNumber");
                    BigDecimal balance = rs.getBigDecimal("balance");
                    int checkDigit = rs.getInt("checkDigit");
                    String countryCode = rs.getString("countryCode");
                    Timestamp updateTimestamp = rs.getTimestamp("updateTimestamp");
                    Date creationDate = rs.getDate("creationDate");
                    Time creationTime = rs.getTime("creationTime");
                    log.info("id: {},\n number; {},\n balance: {},\n checkDigit: {},\n countryCode: {},\n updateTimestamp: {},\n creationDate: {},\n creationTime: {}", id, accountNumber, balance, checkDigit, countryCode, updateTimestamp, creationDate, creationTime);
                }
            } catch (SQLException e) {
                log.warn("Unable to read result set", e);
                throw new RuntimeException();
            }
        } catch (SQLException e) {
            log.warn("Unable to execute SELECT SQL statement", e);
            throw new RuntimeException();
        }
    }

    }
