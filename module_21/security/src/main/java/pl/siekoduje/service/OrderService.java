package pl.siekoduje.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ControllerAdvice;
import pl.siekoduje.model.Order;
import pl.siekoduje.repository.OrderRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderService {
    private final OrderRepository orderRepository;

    @Transactional(readOnly = true)
    public Order getOrder(Long orderId) {
        return orderRepository.findById(orderId).orElseThrow(() -> new OrderNotFoundException("There is no order with id: " + orderId + "."));
    }
    @Transactional(readOnly = true)
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public Order createOrder(Order order) {
        if (order.getId() != null && orderRepository.existsById(order.getId()))
            throw new OrderConflictException("Order already exists.");
        return orderRepository.save(order);
    }

    public void deleteOrder(Long orderId) {
        if (orderRepository.existsById(orderId))
            orderRepository.deleteById(orderId);
        else
            throw new BadOrderException("There is no order with id: " + orderId + ".");
    }

    public Order updateOrder(Long orderId, Order updatedOrder) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new OrderNotFoundException("There is no order with id: " + orderId + "."));
        order.setCreationDate(updatedOrder.getCreationDate());
        order.setDeliveryAddress(updatedOrder.deliveryAddress);
        order.setDeliveryDay(updatedOrder.getDeliveryDay());
        order.setDeliveryType(updatedOrder.getDeliveryType());
        order.setOrderAmount(updatedOrder.getOrderAmount());
        order.setPaymentType(updatedOrder.getPaymentType());
        return orderRepository.save(order);
    }
}
