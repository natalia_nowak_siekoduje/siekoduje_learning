package pl.siekoduje.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

//BAD REQUEST 400
public class BadOrderException extends ResponseStatusException {
    public BadOrderException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
