package pl.siekoduje.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.siekoduje.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
}
