DELETE FROM orders;
INSERT INTO orders (id, creation_date, order_amount, payment_type, delivery_type, delivery_day, postal_code, city, street, building_number,  apartment_number)
VALUES (1, '2021-04-20', 200, 'CASH' , 'PICKUP', '2021-05-22', '00-001', 'Warsaw', 'Sienna', '30', '300');

DELETE FROM users;
INSERT INTO users (username, password, role)
VALUES ('readAndWrite', '$2y$12$WFsrkQ/6TixR2VuZL77bA.c9yX2he6HJiQRYOcJYF2jr8dal1g2EG', 'READ_AND_WRITE');
INSERT INTO users (username, password, role)
VALUES ('readOnly', '$2y$12$WFsrkQ/6TixR2VuZL77bA.c9yX2he6HJiQRYOcJYF2jr8dal1g2EG', 'READ_ONLY');
