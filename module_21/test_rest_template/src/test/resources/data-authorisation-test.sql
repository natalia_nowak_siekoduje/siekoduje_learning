DELETE FROM orders;
INSERT INTO orders ( creation_date, order_amount, payment_type, delivery_type, delivery_day, postal_code, city, street, building_number,  apartment_number)
VALUES ( '2021-04-20', 200, 'CASH' , 'PICKUP', '2022-05-22', '00-001', 'Warsaw', 'Sienna', '30', '300');

DELETE FROM users;
INSERT INTO users (username, password, role)
VALUES ('readAndWrite', '{noop}siekoduje', 'READ_AND_WRITE');
INSERT INTO users (username, password, role)
VALUES ('readOnly', '{noop}siekoduje', 'READ_ONLY');
