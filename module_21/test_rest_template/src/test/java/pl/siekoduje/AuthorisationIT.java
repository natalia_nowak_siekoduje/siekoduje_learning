package pl.siekoduje;

import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import pl.siekoduje.config.TestConfig;
import pl.siekoduje.model.Address;
import pl.siekoduje.model.DeliveryType;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.OrdersList;
import pl.siekoduje.model.PaymentType;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
@Sql("classpath:schema-test.sql")
@Sql(scripts = {"classpath:data-authorisation-test.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Import(TestConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthorisationIT {
    @Autowired
    private TestRestTemplate restTemplate;
    private final Order ORDER_1 = Order.builder()
            .id(1L)
            .creationDate(LocalDate.of(2021, 4, 20))
            .orderAmount(BigDecimal.valueOf(200))
            .paymentType(PaymentType.CASH)
            .deliveryType(DeliveryType.PICKUP)
            .deliveryDay(LocalDate.of(2022, 5, 22))
            .deliveryAddress(Address.builder()
                    .postalCode("00-001")
                    .city("Warsaw")
                    .street("Sienna")
                    .buildingNumber("30")
                    .apartmentNumber("300")
                    .build())
            .build();

    @Test
    public void givenFakeUser_shouldGet401Unauthorized() {
        ResponseEntity<Void> result =
                restTemplate
                        .withBasicAuth("fakeUser", "wrongPassword")
                        .getForEntity("/api/orders", Void.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
        assertThat(result.getBody()).isNull();
    }

    @Test
    public void givenReadOnlyUser_shouldGet403Forbidden_forCreateOrderMethod() {
        //given
        ParameterizedTypeReference<Map<String, Object>> responseType = new ParameterizedTypeReference<>() {
        };
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Order order = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(100))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("UpdatedOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(order, headers);

        //when
        ResponseEntity<Map<String, Object>> result = restTemplate.withBasicAuth("readOnly", "siekoduje")
                .exchange("/api/orders", HttpMethod.POST, request, responseType);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(result.getBody()).isNotEmpty();
        Map<String, Object> error = result.getBody();
        assertThat(error)
                .containsEntry("error", "Forbidden")
                .containsEntry("message", "Forbidden")
                .containsEntry("path", "/api/orders")
                .containsEntry("status", 403)
                .containsKey("timestamp");
    }
    @Test
    public void givenReadOnlyUser_shouldGet403Forbidden_forUpdateOrderMethod() {
        //given
        ParameterizedTypeReference<Map<String, Object>> responseType = new ParameterizedTypeReference<>() {
        };
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Order order = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(100))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("UpdatedOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(order, headers);

        //when
        ResponseEntity<Map<String, Object>> result = restTemplate.withBasicAuth("readOnly", "siekoduje")
                .exchange("/api/orders/1", HttpMethod.PUT, request, responseType);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(result.getBody()).isNotEmpty();
        Map<String, Object> error = result.getBody();
        assertThat(error)
                .containsEntry("error", "Forbidden")
                .containsEntry("message", "Forbidden")
                .containsEntry("path", "/api/orders/1")
                .containsEntry("status", 403)
                .containsKey("timestamp");
    }

    @Test
    public void givenReadOnlyUser_shouldGet403Forbidden_forDeleteOrderMethod() {
        //given
        ParameterizedTypeReference<Map<String, Object>> responseType = new ParameterizedTypeReference<>() {
        };

        //when
        ResponseEntity<Map<String, Object>> result = restTemplate.withBasicAuth("readOnly", "siekoduje")
                .exchange("/api/orders/1", HttpMethod.DELETE, null, responseType);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
        assertThat(result.getBody()).isNotEmpty();
        Map<String, Object> error = result.getBody();
        assertThat(error)
                .containsEntry("error", "Forbidden")
                .containsEntry("message", "Forbidden")
                .containsEntry("path", "/api/orders/1")
                .containsEntry("status", 403)
                .containsKey("timestamp");
    }

    @Test
    public void givenReadOnlyUser_shouldGet200Ok_forGetOrdersMethod() {
        //given
        //when
        ResponseEntity<OrdersList> result =
                restTemplate
                        .withBasicAuth("readOnly", "siekoduje")
                        .getForEntity("/api/orders", OrdersList.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody().getOrders()).usingRecursiveFieldByFieldElementComparator(RecursiveComparisonConfiguration.builder().withIgnoredFields("id").build()).containsExactly(ORDER_1);
    }

    @Test
    public void givenReadAndWriteUser_shouldGet200Ok_forGetOrdersMethod() {
        //given
        //when
        ResponseEntity<OrdersList> result =
                restTemplate
                        .withBasicAuth("readAndWrite", "siekoduje")
                        .getForEntity("/api/orders", OrdersList.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody().getOrders()).usingRecursiveFieldByFieldElementComparator(RecursiveComparisonConfiguration.builder().withIgnoredFields("id").build()).containsExactly(ORDER_1);
    }

    @Test
    public void givenReadOnlyUser_shouldGet200Ok_forGetOrderMethod() {
        //given
        //when
        ResponseEntity<Order> result =
                restTemplate
                        .withBasicAuth("readOnly", "siekoduje")
                        .getForEntity("/api/orders/1", Order.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(ORDER_1);
    }

    @Test
    public void givenReadAndWriteUser_shouldGet200Ok_forGetOrderMethod() {
        //given
        //when
        ResponseEntity<Order> result =
                restTemplate
                        .withBasicAuth("readAndWrite", "siekoduje")
                        .getForEntity("/api/orders/1", Order.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(ORDER_1);
    }


    @Test
    public void givenReadAndWriteUser_shouldGet201Created_forCreateMethod() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Order newOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(100))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("newOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(newOrder, headers);

        //when
        ResponseEntity<Order> result = restTemplate.withBasicAuth("readAndWrite", "siekoduje")
                .exchange("/api/orders", HttpMethod.POST, request, Order.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(result.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(newOrder);

    }

    @Test
    public void givenReadAndWriteUser_shouldGet200Ok_forUpdateMethod() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Order updatedOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(100))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("updatedOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(updatedOrder, headers);

        //when
        ResponseEntity<Order> result = restTemplate.withBasicAuth("readAndWrite", "siekoduje")
                .exchange("/api/orders/1", HttpMethod.PUT, request, Order.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(updatedOrder);

    }

    @Test
    public void givenReadAndWriteUser_shouldGet204NoContent_forDeleteMethod() {
        //when
        ResponseEntity<Void> result = restTemplate.withBasicAuth("readAndWrite", "siekoduje")
                .exchange("/api/orders/1", HttpMethod.DELETE, null, Void.class);
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

}
