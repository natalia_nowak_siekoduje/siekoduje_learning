package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Embeddable
public class Address {

    @NotBlank(message = "Postal code must be provided.")
    @Pattern(regexp = "[0-9]{2}\\-[0-9]{3}",
            message = "Postal code must match polish standard.")
    @Column(name = "postalCode")
    private String postalCode;

    @NotBlank(message = "City must be provided.")
    @Column(name = "city")
    private String city;

    @NotBlank(message = "Street must be provided.")
    @Column(name = "street")
    private String street;

    @NotBlank(message = "Building number must be provided.")
    @Column(name = "buildingNumber")
    private String buildingNumber;

    @Column(name = "apartmentNumber")
    private String apartmentNumber;
}
