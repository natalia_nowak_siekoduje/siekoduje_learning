package pl.siekoduje;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.siekoduje.config.TestConfig;
import pl.siekoduje.model.Address;
import pl.siekoduje.model.DeliveryType;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.PaymentType;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql("classpath:schema-test.sql")
@Sql(scripts = {"classpath:data-authorisation-test.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Import(TestConfig.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthorisationIT {
    private final Order ORDER_1 = Order.builder()
            .id(1L)
            .creationDate(LocalDate.of(2021, 4, 20))
            .orderAmount(BigDecimal.valueOf(200))
            .paymentType(PaymentType.CASH)
            .deliveryType(DeliveryType.PICKUP)
            .deliveryDay(LocalDate.of(2022, 5, 22))
            .deliveryAddress(Address.builder()
                    .postalCode("00-001")
                    .city("Warsaw")
                    .street("Sienna")
                    .buildingNumber("30")
                    .apartmentNumber("300")
                    .build())
            .build();
    @Autowired
    private WebApplicationContext context;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    public void givenFakeUser_shouldGet401Unauthorized() throws Exception {
        mockMvc.perform(get("/api/orders")
                .with(httpBasic("fakeUser", "wrongPassword")).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void givenReadOnlyUser_shouldGet403Forbidden_forCreateOrderMethod() throws Exception {
        //given

        //when
        //given
        Order newOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(200))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("newOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        mockMvc.perform(post("/api/orders")
                .with(httpBasic("readOnly", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"creationDate\":\"2021-04-21\"," +
                        "\"orderAmount\":200," +
                        "\"paymentType\":\"CASH\"," +
                        "\"deliveryType\":\"PICKUP\"," +
                        "\"deliveryDay\":\"2022-05-22\"," +
                        "\"deliveryAddress\":" +
                        "{\"postalCode\":\"00-000\"," +
                        "\"city\":\"newOrder\"," +
                        "\"street\":\"Sienna\"," +
                        "\"buildingNumber\":\"30\"," +
                        "\"apartmentNumber\":\"300\"}}")
                .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void givenReadOnlyUser_shouldGet403Forbidden_forUpdateOrderMethod() throws Exception {
        mockMvc.perform(put("/api/orders/1")
                .with(httpBasic("readOnly", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"creationDate\":\"2021-04-21\"," +
                        "\"orderAmount\":200," +
                        "\"paymentType\":\"CASH\"," +
                        "\"deliveryType\":\"PICKUP\"," +
                        "\"deliveryDay\":\"2022-05-22\"," +
                        "\"deliveryAddress\":" +
                        "{\"postalCode\":\"00-000\"," +
                        "\"city\":\"newOrder\"," +
                        "\"street\":\"Sienna\"," +
                        "\"buildingNumber\":\"30\"," +
                        "\"apartmentNumber\":\"300\"}}")
                .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }

    @Test
    public void givenReadOnlyUser_shouldGet403Forbidden_forDeleteOrderMethod() throws Exception {
        mockMvc.perform(delete("/api/orders/1")
                .with(httpBasic("readOnly", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"creationDate\":\"2021-04-21\"," +
                        "\"orderAmount\":200," +
                        "\"paymentType\":\"CASH\"," +
                        "\"deliveryType\":\"PICKUP\"," +
                        "\"deliveryDay\":\"2022-05-22\"," +
                        "\"deliveryAddress\":" +
                        "{\"postalCode\":\"00-000\"," +
                        "\"city\":\"newOrder\"," +
                        "\"street\":\"Sienna\"," +
                        "\"buildingNumber\":\"30\"," +
                        "\"apartmentNumber\":\"300\"}}")
                .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isForbidden())
                .andExpect(status().reason(containsString("Forbidden")));
    }


    @Test
    public void givenReadOnlyUser_shouldGet200Ok_forGetOrdersMethod() throws Exception {
        //given
        //when
        mockMvc.perform(get("/api/orders")
                .with(httpBasic("readOnly", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders[0].id").isNotEmpty())
                .andExpect(jsonPath("$.orders[0].orderAmount").value(ORDER_1.orderAmount))
                .andExpect(jsonPath("$.orders[0].deliveryType").value(ORDER_1.deliveryType.toString()))
                .andExpect(jsonPath("$.orders[0].paymentType").value(ORDER_1.paymentType.toString()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.postalCode").value(ORDER_1.deliveryAddress.getPostalCode()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.city").value(ORDER_1.deliveryAddress.getCity()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.street").value(ORDER_1.deliveryAddress.getStreet()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.buildingNumber").value(ORDER_1.deliveryAddress.getBuildingNumber()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.apartmentNumber").value(ORDER_1.deliveryAddress.getApartmentNumber()))
                .andExpect(jsonPath("$.orders[0].creationDate").value(ORDER_1.creationDate.toString()))
                .andExpect(jsonPath("$.orders[0].deliveryDay").value(ORDER_1.deliveryDay.toString()));
    }

    @Test
    public void givenReadAndWriteUser_shouldGet200Ok_forGetOrdersMethod() throws Exception {
        //given
        //when
        mockMvc.perform(get("/api/orders")
                .with(httpBasic("readAndWrite", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders[0].id").isNotEmpty())
                .andExpect(jsonPath("$.orders[0].orderAmount").value(ORDER_1.orderAmount))
                .andExpect(jsonPath("$.orders[0].deliveryType").value(ORDER_1.deliveryType.toString()))
                .andExpect(jsonPath("$.orders[0].paymentType").value(ORDER_1.paymentType.toString()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.postalCode").value(ORDER_1.deliveryAddress.getPostalCode()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.city").value(ORDER_1.deliveryAddress.getCity()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.street").value(ORDER_1.deliveryAddress.getStreet()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.buildingNumber").value(ORDER_1.deliveryAddress.getBuildingNumber()))
                .andExpect(jsonPath("$.orders[0].deliveryAddress.apartmentNumber").value(ORDER_1.deliveryAddress.getApartmentNumber()))
                .andExpect(jsonPath("$.orders[0].creationDate").value(ORDER_1.creationDate.toString()))
                .andExpect(jsonPath("$.orders[0].deliveryDay").value(ORDER_1.deliveryDay.toString()));
    }

    @Test
    public void givenReadOnlyUser_shouldGet200Ok_forGetOrderMethod() throws Exception {
        //given
        //when
        mockMvc.perform(get("/api/orders/1")
                .with(httpBasic("readOnly", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.orderAmount").value(ORDER_1.orderAmount))
                .andExpect(jsonPath("$.deliveryType").value(ORDER_1.deliveryType.toString()))
                .andExpect(jsonPath("$.paymentType").value(ORDER_1.paymentType.toString()))
                .andExpect(jsonPath("$.deliveryAddress.postalCode").value(ORDER_1.deliveryAddress.getPostalCode()))
                .andExpect(jsonPath("$.deliveryAddress.city").value(ORDER_1.deliveryAddress.getCity()))
                .andExpect(jsonPath("$.deliveryAddress.street").value(ORDER_1.deliveryAddress.getStreet()))
                .andExpect(jsonPath("$.deliveryAddress.buildingNumber").value(ORDER_1.deliveryAddress.getBuildingNumber()))
                .andExpect(jsonPath("$.deliveryAddress.apartmentNumber").value(ORDER_1.deliveryAddress.getApartmentNumber()))
                .andExpect(jsonPath("$.creationDate").value(ORDER_1.creationDate.toString()))
                .andExpect(jsonPath("$.deliveryDay").value(ORDER_1.deliveryDay.toString()));
    }

    @Test
    public void givenReadAndWriteUser_shouldGet200Ok_forGetOrderMethod() throws Exception {
        //given
        //when
        mockMvc.perform(get("/api/orders/1")
                .with(httpBasic("readAndWrite", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.orderAmount").value(ORDER_1.orderAmount))
                .andExpect(jsonPath("$.deliveryType").value(ORDER_1.deliveryType.toString()))
                .andExpect(jsonPath("$.paymentType").value(ORDER_1.paymentType.toString()))
                .andExpect(jsonPath("$.deliveryAddress.postalCode").value(ORDER_1.deliveryAddress.getPostalCode()))
                .andExpect(jsonPath("$.deliveryAddress.city").value(ORDER_1.deliveryAddress.getCity()))
                .andExpect(jsonPath("$.deliveryAddress.street").value(ORDER_1.deliveryAddress.getStreet()))
                .andExpect(jsonPath("$.deliveryAddress.buildingNumber").value(ORDER_1.deliveryAddress.getBuildingNumber()))
                .andExpect(jsonPath("$.deliveryAddress.apartmentNumber").value(ORDER_1.deliveryAddress.getApartmentNumber()))
                .andExpect(jsonPath("$.creationDate").value(ORDER_1.creationDate.toString()))
                .andExpect(jsonPath("$.deliveryDay").value(ORDER_1.deliveryDay.toString()));
    }


    @Test
    public void givenReadAndWriteUser_shouldCreateNewUser() throws Exception {

        //given
        Order newOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(200))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("newOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        mockMvc.perform(post("/api/orders")
                .with(httpBasic("readAndWrite", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"creationDate\":\"2021-04-21\"," +
                        "\"orderAmount\":200," +
                        "\"paymentType\":\"CASH\"," +
                        "\"deliveryType\":\"PICKUP\"," +
                        "\"deliveryDay\":\"2022-05-22\"," +
                        "\"deliveryAddress\":" +
                        "{\"postalCode\":\"00-000\"," +
                        "\"city\":\"newOrder\"," +
                        "\"street\":\"Sienna\"," +
                        "\"buildingNumber\":\"30\"," +
                        "\"apartmentNumber\":\"300\"}}")
                .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.orderAmount").value(newOrder.orderAmount))
                .andExpect(jsonPath("$.deliveryType").value(newOrder.deliveryType.toString()))
                .andExpect(jsonPath("$.paymentType").value(newOrder.paymentType.toString()))
                .andExpect(jsonPath("$.deliveryAddress.postalCode").value(newOrder.deliveryAddress.getPostalCode()))
                .andExpect(jsonPath("$.deliveryAddress.city").value(newOrder.deliveryAddress.getCity()))
                .andExpect(jsonPath("$.deliveryAddress.street").value(newOrder.deliveryAddress.getStreet()))
                .andExpect(jsonPath("$.deliveryAddress.buildingNumber").value(newOrder.deliveryAddress.getBuildingNumber()))
                .andExpect(jsonPath("$.deliveryAddress.apartmentNumber").value(newOrder.deliveryAddress.getApartmentNumber()))
                .andExpect(jsonPath("$.creationDate").value(newOrder.creationDate.toString()))
                .andExpect(jsonPath("$.deliveryDay").value(newOrder.deliveryDay.toString()));

        //when

    }

    @Test
    public void givenReadAndWriteUser_shouldUpdateUser() throws Exception {
        //given
        Order newOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 04, 21))
                .orderAmount(BigDecimal.valueOf(200))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2022, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("newOrder")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        //when
        mockMvc.perform(put("/api/orders/1")
                .with(httpBasic("readAndWrite", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"creationDate\":\"2021-04-21\"," +
                        "\"orderAmount\":200," +
                        "\"paymentType\":\"CASH\"," +
                        "\"deliveryType\":\"PICKUP\"," +
                        "\"deliveryDay\":\"2022-05-22\"," +
                        "\"deliveryAddress\":" +
                        "{\"postalCode\":\"00-000\"," +
                        "\"city\":\"newOrder\"," +
                        "\"street\":\"Sienna\"," +
                        "\"buildingNumber\":\"30\"," +
                        "\"apartmentNumber\":\"300\"}}")
                .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.orderAmount").value(newOrder.orderAmount))
                .andExpect(jsonPath("$.deliveryType").value(newOrder.deliveryType.toString()))
                .andExpect(jsonPath("$.paymentType").value(newOrder.paymentType.toString()))
                .andExpect(jsonPath("$.deliveryAddress.postalCode").value(newOrder.deliveryAddress.getPostalCode()))
                .andExpect(jsonPath("$.deliveryAddress.city").value(newOrder.deliveryAddress.getCity()))
                .andExpect(jsonPath("$.deliveryAddress.street").value(newOrder.deliveryAddress.getStreet()))
                .andExpect(jsonPath("$.deliveryAddress.buildingNumber").value(newOrder.deliveryAddress.getBuildingNumber()))
                .andExpect(jsonPath("$.deliveryAddress.apartmentNumber").value(newOrder.deliveryAddress.getApartmentNumber()))
                .andExpect(jsonPath("$.creationDate").value(newOrder.creationDate.toString()))
                .andExpect(jsonPath("$.deliveryDay").value(newOrder.deliveryDay.toString()));
    }

    @Test
    public void givenReadAndWriteUser_shouldDeleteUser() throws Exception {
        mockMvc.perform(delete("/api/orders/1")
                .with(httpBasic("readAndWrite", "siekoduje"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\"creationDate\":\"2021-04-21\"," +
                        "\"orderAmount\":200," +
                        "\"paymentType\":\"CASH\"," +
                        "\"deliveryType\":\"PICKUP\"," +
                        "\"deliveryDay\":\"2022-05-22\"," +
                        "\"deliveryAddress\":" +
                        "{\"postalCode\":\"00-000\"," +
                        "\"city\":\"newOrder\"," +
                        "\"street\":\"Sienna\"," +
                        "\"buildingNumber\":\"30\"," +
                        "\"apartmentNumber\":\"300\"}}")
                .characterEncoding("utf-8"))
                .andDo(MockMvcResultHandlers.print())
                //then
                .andExpect(status().isNoContent());
    }

}
