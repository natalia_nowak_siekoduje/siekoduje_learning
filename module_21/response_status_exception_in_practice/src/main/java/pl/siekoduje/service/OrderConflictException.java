package pl.siekoduje.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

// CONFLICT 409
public class OrderConflictException extends ResponseStatusException {
    public OrderConflictException(String message) {
        super(HttpStatus.CONFLICT, message);
    }
}
