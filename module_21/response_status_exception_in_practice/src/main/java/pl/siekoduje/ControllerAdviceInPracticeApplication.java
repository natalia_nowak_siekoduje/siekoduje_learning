package pl.siekoduje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControllerAdviceInPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllerAdviceInPracticeApplication.class, args);
	}

}
