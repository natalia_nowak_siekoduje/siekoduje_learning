package pl.siekoduje.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import pl.siekoduje.model.Address;
import pl.siekoduje.model.DeliveryType;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.OrdersList;
import pl.siekoduje.model.PaymentType;
import pl.siekoduje.repository.OrderRepository;
import pl.siekoduje.service.BadOrderException;
import pl.siekoduje.service.OrderConflictException;
import pl.siekoduje.service.OrderNotFoundException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Sql("classpath:create-table.sql")
@Sql(scripts = {"classpath:data-test.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OrderIT {
    private final Order ORDER_1 = Order.builder()
            .id(1L)
            .creationDate(LocalDate.of(2021, 5, 20))
            .orderAmount(BigDecimal.valueOf(200))
            .paymentType(PaymentType.CASH)
            .deliveryType(DeliveryType.PICKUP)
            .deliveryDay(LocalDate.of(2021, 4, 22))
            .deliveryAddress(Address.builder()
                    .postalCode("00-001")
                    .city("Warsaw")
                    .street("Sienna")
                    .buildingNumber("30")
                    .apartmentNumber("300")
                    .build())
            .build();
    @Autowired
    private OrderRepository orderRepository;
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    private HttpHeaders headers;

    @BeforeEach
    public void setUp() {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @Test
    void shouldCreateOrder() {
        //given
        Order order = Order.builder()
                .id(2L)
                .creationDate(LocalDate.of(2021, 04, 20))
                .orderAmount(BigDecimal.valueOf(100))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2021, 04, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-000")
                        .city("Warsaw")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(order, headers);
        //when
        ResponseEntity<Order> result = this.restTemplate.postForEntity("http://localhost:" + port + "/api/orders", request, Order.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(result.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(order);
        assertThat(orderRepository.findById(2L).get()).isEqualTo(order);
    }

    @Test
    void shouldNotCreateOrderWhenOrderAlreadyExists() {
        //given
        HttpEntity<Order> request = new HttpEntity<>(ORDER_1, headers);
        //when
        ResponseEntity<LinkedHashMap> result = this.restTemplate.postForEntity("http://localhost:" + port + "/api/orders", request, LinkedHashMap.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.CONFLICT);
        assertThat(result.getBody().get("message")).isEqualTo("Order already exists.");

    }

    @Sql(statements = {
            "INSERT INTO orders (id, creation_date, order_amount, payment_type, delivery_type, delivery_day, postal_code, city, street, building_number,  apartment_number) " +
                    " VALUES (2,  '2021-04-20', 100, 'CASH' , 'PICKUP', '2021-04-22', '00-002', 'Warsaw', 'Sienna', '30', '300'); " +
                    "INSERT INTO orders (id, creation_date, order_amount, payment_type, delivery_type, delivery_day, postal_code, city, street, building_number,  apartment_number) " +
                    " VALUES (3,  '2021-04-20', 200, 'CASH' , 'PICKUP', '2021-04-22', '00-003', 'Warsaw', 'Sienna', '30', '300');"})
    @Test
    void shouldGetOrders() {
        //given

        //when
        ResponseEntity<OrdersList> result = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders",
                OrdersList.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        List<Order> resultOrders = result.getBody().getOrders();
        Order order2 = Order.builder()
                .id(2L)
                .creationDate(LocalDate.of(2021, 04, 20))
                .orderAmount(BigDecimal.valueOf(100))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2021, 04, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-002")
                        .city("Warsaw")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        Order order3 = Order.builder()
                .id(3L)
                .creationDate(LocalDate.of(2021, 04, 20))
                .orderAmount(BigDecimal.valueOf(200))
                .paymentType(PaymentType.CASH)
                .deliveryType(DeliveryType.PICKUP)
                .deliveryDay(LocalDate.of(2021, 04, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-003")
                        .city("Warsaw")
                        .street("Sienna")
                        .buildingNumber("30")
                        .apartmentNumber("300")
                        .build())
                .build();
        assertThat(resultOrders).containsExactlyInAnyOrder(order2, order3, ORDER_1);
    }

    @Test
    void shouldGetOrder() {
        //given

        //when
        ResponseEntity<Order> result = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/1",
                Order.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).usingRecursiveComparison().isEqualTo(ORDER_1);
    }

    @Test
    void shouldNotGetOrderWhenUnknownId() {
        //given

        //when
        ResponseEntity<LinkedHashMap> result = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/-1",
                LinkedHashMap.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(result.getBody().get("message")).isEqualTo("There is no order with id: -1.");
    }

    @Test
    void shouldUpdateOrder() {
        //given
        Order updatedOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 05, 20))
                .orderAmount(BigDecimal.valueOf(400))
                .paymentType(PaymentType.CARD)
                .deliveryType(DeliveryType.SHIPPING)
                .deliveryDay(LocalDate.of(2021, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-001")
                        .city("Szczecin")
                        .street("Kwiatowa")
                        .buildingNumber("10")
                        .apartmentNumber("100")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(updatedOrder, headers);
        //when
        ResponseEntity<Order> result = restTemplate.exchange("http://localhost:" + port + "/api/orders/" + ORDER_1.getId(), HttpMethod.PUT, request, Order.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(result.getBody()).isNotNull();
        assertThat(result.getBody()).usingRecursiveComparison().ignoringFields("id").isEqualTo(updatedOrder);
        assertThat(orderRepository.findById(1L).get()).usingRecursiveComparison().ignoringFields("id").isEqualTo(updatedOrder);
    }

    @Test
    void shouldNotUpdateOrderWhenUnknownId() {
        //given
        Order updatedOrder = Order.builder()
                .creationDate(LocalDate.of(2021, 05, 20))
                .orderAmount(BigDecimal.valueOf(400))
                .paymentType(PaymentType.CARD)
                .deliveryType(DeliveryType.SHIPPING)
                .deliveryDay(LocalDate.of(2021, 05, 22))
                .deliveryAddress(Address.builder()
                        .postalCode("00-001")
                        .city("Szczecin")
                        .street("Kwiatowa")
                        .buildingNumber("10")
                        .apartmentNumber("100")
                        .build())
                .build();
        HttpEntity<Order> request = new HttpEntity<>(updatedOrder, headers);
        //when
        ResponseEntity<LinkedHashMap> result =  restTemplate.exchange("http://localhost:" + port + "/api/orders/-1", HttpMethod.PUT, request, LinkedHashMap.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(result.getBody().get("message")).isEqualTo("There is no order with id: -1.");
        assertThat(orderRepository.findById(1L).get()).usingRecursiveComparison().ignoringFields("id").isNotEqualTo(updatedOrder);
    }

    @Test
    void shouldDeleteOrder() {
        //given
        //when
        ResponseEntity<Void> result = this.restTemplate.exchange("http://localhost:" + port + "/api/orders/" + ORDER_1.getId(), HttpMethod.DELETE, new HttpEntity<>(new HttpHeaders()), Void.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(orderRepository.findById(1L)).isEmpty();
    }

    @Test
    void shouldNotDeleteOrderWhenUnknownId() {
        //given
        //when
        ResponseEntity<LinkedHashMap> result = this.restTemplate.exchange("http://localhost:" + port + "/api/orders/-1", HttpMethod.DELETE, new HttpEntity<>(new HttpHeaders()), LinkedHashMap.class);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(result.getBody().get("message")).isEqualTo("There is no order with id: -1.");
        assertThat(orderRepository.findById(1L)).isNotEmpty();
        assertThat(orderRepository.findById(1L).get()).usingRecursiveComparison().isEqualTo(ORDER_1);
    }
}