package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity(name = "Order")
@Table(name = "orders")
@EqualsAndHashCode(of = "id")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @NotNull(message = "Creation date must be provided.")
    @PastOrPresent(message = "Creation date must be past or present date.")
    @Column(name = "creation_date")
    public LocalDate creationDate;

    @NotNull(message = "Order amount must be provided.")
    @PositiveOrZero(message = "Order amount must be positive or zero value.")
    @Column(name = "order_amount")
    public BigDecimal orderAmount;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Payment type must be provided.")
    @Column(name = "payment_type")
    public PaymentType paymentType;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Delivery type must be provided.")
    @Column(name = "delivery_type")
    public DeliveryType deliveryType;

    @NotNull(message = "Delivery day must be provided.")
    @Future(message = "Delivery day must be future date.")
    @Column(name = "delivery_day")
    public LocalDate deliveryDay;

    @Embedded
    @NotNull(message = "Delivery address must be provided.")
    public @Valid Address deliveryAddress;

}
