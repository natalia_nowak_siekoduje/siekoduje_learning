DELETE FROM orders;
INSERT INTO orders (id, creation_date, order_amount, payment_type, delivery_type, delivery_day, postal_code, city, street, building_number,  apartment_number)
VALUES (1, '2021-04-20', 200, 'CASH' , 'PICKUP', '2021-05-22', '00-001', 'Warsaw', 'Sienna', '30', '300');

-- INSERT INTO orders (id, creation_date, order_amount, payment_type, delivery_type, delivery_day, postal_code, city, street, building_number,  apartment_number)
-- VALUES (100, '2023-05-20', 200, 'CASH' , 'PICKUP', '2020-04-22', '00-001', 'Warsaw', 'Sienna', '30', '300');
-- czemu dodaje się takie zamowienie do bazy danych, skoro nie spełnia wymagan ? ustawiłąm złe daty (creation_date ma datę do przodu o dwa lata, a delivery_day do tyłu o rok)