DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
                        id  SERIAL PRIMARY KEY,
                        creation_date DATE,
                        order_amount DECIMAL,
                        payment_type VARCHAR,
                        delivery_type VARCHAR,
                        delivery_day DATE,
                        postal_code VARCHAR,
                        city VARCHAR,
                        street VARCHAR,
                        building_number VARCHAR,
                        apartment_number VARCHAR
);
