package pl.siekoduje.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.siekoduje.model.ApiError;
import pl.siekoduje.service.BadOrderException;
import pl.siekoduje.service.OrderConflictException;
import pl.siekoduje.service.OrderNotFoundException;

@ControllerAdvice
public class ApiErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({OrderNotFoundException.class})
    public ResponseEntity<ApiError> handleNotFoundError(OrderNotFoundException exception) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage()));
    }

    @ExceptionHandler({OrderConflictException.class})
    public ResponseEntity<ApiError> handleConflictError(OrderConflictException exception) {
        HttpStatus status = HttpStatus.CONFLICT;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage()));
    }

    @ExceptionHandler({BadOrderException.class})
    public ResponseEntity<ApiError> handleBadOrderError(BadOrderException exception) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage()));
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ApiError> handleInternalError(Exception exception) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        return ResponseEntity.status(status).body(new ApiError(status, exception.getMessage()));
    }
}
