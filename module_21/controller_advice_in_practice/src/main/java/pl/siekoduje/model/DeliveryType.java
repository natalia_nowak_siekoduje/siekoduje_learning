package pl.siekoduje.model;

public enum DeliveryType {
    SHIPPING,
    PICKUP
}
