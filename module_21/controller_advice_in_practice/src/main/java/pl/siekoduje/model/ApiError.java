package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.Instant;
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ApiError {
    private  int status;
    private String error;
    private String message;
    private Instant timestamp;

    public ApiError(HttpStatus httpStatus, String message) {
        this.status = httpStatus.value();
        this.error = httpStatus.getReasonPhrase();
        this.message = message;
        this.timestamp = Instant.now();
    }
}
