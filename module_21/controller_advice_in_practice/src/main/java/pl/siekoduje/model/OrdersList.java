package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class OrdersList {
    private final List<Order> orders;

    public OrdersList() {
        orders = new ArrayList<>();
    }
}
