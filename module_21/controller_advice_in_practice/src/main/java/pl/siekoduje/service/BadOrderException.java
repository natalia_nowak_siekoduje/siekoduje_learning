package pl.siekoduje.service;

//BAD REQUEST 400
public class BadOrderException extends RuntimeException {
    public BadOrderException(String message) {
        super(message);
    }
}
