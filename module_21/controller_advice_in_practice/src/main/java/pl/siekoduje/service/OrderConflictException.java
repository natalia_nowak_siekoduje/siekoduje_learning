package pl.siekoduje.service;

// CONFLICT 409
public class OrderConflictException extends RuntimeException {
    public OrderConflictException(String message) {
        super(message);
    }
}
