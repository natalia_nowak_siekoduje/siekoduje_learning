package pl.siekoduje.copy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.siekoduje.compress.CompressService;
import pl.siekoduje.config.BackupProperties;
import pl.siekoduje.config.MailProperties;
import pl.siekoduje.email.EmailSenderService;
import pl.siekoduje.service.MoveFileService;

import java.io.File;

@Component
@Slf4j
public class CopyTaskImpl implements CopyTask {

    private final EmailSenderService emailSenderService;
    private final MoveFileService moveFileService;
    private final CompressService compressService;
    private final File source;
    private final String senderLogin;
    private final String receiverLogin;
    private final String messageSubject;
    private final String messageContent;

    public CopyTaskImpl(EmailSenderService emailSenderService, MoveFileService moveFileService, CompressService compressService, BackupProperties backupProperties, MailProperties mailProperties) {
        this.emailSenderService = emailSenderService;
        this.moveFileService = moveFileService;
        this.compressService = compressService;
        this.source = backupProperties.getSource();
        this.receiverLogin = mailProperties.getReceiver().getLogin();
        this.senderLogin = mailProperties.getSender().getLogin();
        this.messageSubject = mailProperties.getSubject();
        this.messageContent = mailProperties.getContent();
    }

    @Override
    public void run() {
        try {
            File compressedFile = compressService.compress(source);
            File newLocation = moveFileService.move(compressedFile);
            emailSenderService.sendEmail(senderLogin, receiverLogin, messageSubject, messageContent);
        } catch (Exception e) {
            log.error("Couldn't compress file ", e);
        }

    }
}
