package pl.siekoduje.copy;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
@RequiredArgsConstructor
public class CopyTaskExecutor {
    private final CopyTask copyTask;
    private final ScheduledExecutorService executor;

    @PostConstruct
    public void start() {
        executor.scheduleAtFixedRate(copyTask, 0, 1, TimeUnit.HOURS);
    }

}
