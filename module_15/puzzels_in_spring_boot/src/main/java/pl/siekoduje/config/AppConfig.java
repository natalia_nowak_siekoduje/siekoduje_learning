package pl.siekoduje.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import pl.siekoduje.copy.CopyTask;

import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Getter
@Slf4j
@Configuration
public class AppConfig {

    @Bean("executor")
    public ScheduledExecutorService ScheduledExecutorService() {
        return Executors.newSingleThreadScheduledExecutor();

    }

    @Bean("javaMailSender")
    public JavaMailSender javaMailSender(MailProperties mailProperties) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailProperties.getSmtp().getServer());
        mailSender.setPort(mailProperties.getSmtp().getPort());
        mailSender.setUsername(mailProperties.getSender().getLogin());
        mailSender.setPassword(mailProperties.getSender().getPassword());
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", mailProperties.getSmtp().isAuth());
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.enable", mailProperties.getSmtp().isSslEnable());
        props.put("mail.debug", "true");
        return mailSender;
    }

}
