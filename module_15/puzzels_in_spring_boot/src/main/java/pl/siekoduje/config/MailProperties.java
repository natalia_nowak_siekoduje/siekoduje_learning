package pl.siekoduje.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "mail")
@Getter
@Setter
public class MailProperties {

    private Smtp smtp;
    private Receiver receiver;
    private Sender sender;
    private String content;
    private String subject;


    @Getter
    @Setter
    public static class Smtp {
        private String server;
        private Integer port;
        private boolean auth;
        private boolean sslEnable;
    }

    @Getter
    @Setter
    public static class Receiver {
        private String login;
    }

    @Getter
    @Setter
    public static class Sender {
        private String password;
        private String login;
    }
}
