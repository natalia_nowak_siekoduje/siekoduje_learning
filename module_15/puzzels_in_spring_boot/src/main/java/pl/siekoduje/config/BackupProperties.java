package pl.siekoduje.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;
import java.util.concurrent.TimeUnit;

@ConfigurationProperties(prefix = "application.backup")
@Getter
@Setter
public class BackupProperties {

    private Interval interval;
    private File source;
    private File destination;

    @Getter
    @Setter
    public static class Interval {
        private TimeUnit unit;
        private Integer value;
    }
}
