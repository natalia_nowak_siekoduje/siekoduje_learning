package pl.siekoduje.compress;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
@Slf4j
public class CompressServiceImpl implements CompressService {
    @Override
    public File compress(File directory) throws Exception {

        File file = Paths.get(directory.getAbsolutePath(), "test.zip").toFile();
        try (ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(file))) {
            Files.walk(directory.toPath())
                    .filter(path -> {
                        return !path.toFile().getName().endsWith(".zip");
                    })
                    .filter(path -> {
                        return Files.isRegularFile(path);
                    })
                    .forEach(path -> {
                        try {
                            ZipEntry zipEntry = createZipEntry(path);
                            zipOut.putNextEntry(zipEntry);
                            try (FileInputStream fis = new FileInputStream(path.toFile())) {
                                byte[] bytes = new byte[1024];
                                int length;
                                while ((length = fis.read(bytes)) >= 0) {
                                    zipOut.write(bytes, 0, length);
                                }
                            }
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    });
        } catch (Exception e) {
            log.warn("", e);
            throw new RuntimeException(e);
        }
        return file;
    }

    private ZipEntry createZipEntry(Path path) {
        return new ZipEntry(path.toFile().getName());
    }
}
