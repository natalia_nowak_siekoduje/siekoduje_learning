package pl.siekoduje.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.siekoduje.config.BackupProperties;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Component
public class MoveFileServiceImpl implements MoveFileService {
    private final File destination;


    @Autowired
    public MoveFileServiceImpl(BackupProperties backupProperties) throws IOException {
        this.destination = backupProperties.getDestination();
    }

    @Override
    public File move(File file) throws IOException, URISyntaxException {
        Path targetPath = Paths.get(destination.getAbsolutePath(), file.getName());
        Files.move(file.toPath(), targetPath, StandardCopyOption.REPLACE_EXISTING);
        return targetPath.toFile();
    }
}
