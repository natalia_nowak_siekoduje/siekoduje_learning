package pl.siekoduje.service;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public interface MoveFileService {
    File move(File file) throws URISyntaxException, IOException;
}