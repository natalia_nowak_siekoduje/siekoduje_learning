package pl.siekoduje;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import pl.siekoduje.config.BackupProperties;
import pl.siekoduje.config.MailProperties;

@EnableConfigurationProperties({BackupProperties.class , MailProperties.class})
@SpringBootApplication
public class PuzzlesInSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(PuzzlesInSpringBootApplication.class, args);
	}

}
