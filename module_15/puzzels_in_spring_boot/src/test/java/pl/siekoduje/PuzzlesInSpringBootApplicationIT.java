package pl.siekoduje;

import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit5.GreenMailExtension;
import com.icegreen.greenmail.util.ServerSetupTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.siekoduje.config.MailProperties;
import pl.siekoduje.config.TestConfig;
import pl.siekoduje.copy.CopyTask;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@Slf4j
@SpringBootTest
public class PuzzlesInSpringBootApplicationIT {
    private static final String LOGIN = "kkowalski11214@gmail.com";
    private static final String PASSWORD = "Qwerty1234!";

    @TempDir
    public static File folder;

    @RegisterExtension
    public static GreenMailExtension greenMailRule = new GreenMailExtension(ServerSetupTest.SMTP).withConfiguration(GreenMailConfiguration.aConfig().withUser(LOGIN, PASSWORD));

    @Autowired
    private CopyTask copyTask;

    @BeforeAll
    public static void setUp() {
        when(TestConfig.backupProperties.getSource()).thenReturn(createNewFolder("from-dir"));
        when(TestConfig.backupProperties.getDestination()).thenReturn(createNewFolder("to-dir"));

        MailProperties.Smtp smtpMock = mock(MailProperties.Smtp.class);
        when(TestConfig.mailProperties.getSmtp()).thenReturn(smtpMock);
        when(smtpMock.getServer()).thenReturn("127.0.0.1");
        when(smtpMock.getPort()).thenReturn(3025);
        when(smtpMock.isSslEnable()).thenReturn(false);
        when(smtpMock.isAuth()).thenReturn(false);

        MailProperties.Sender senderMock = mock(MailProperties.Sender.class);
        when(TestConfig.mailProperties.getSender()).thenReturn(senderMock);
        when(senderMock.getLogin()).thenReturn("kkowalski11214@gmail.com");
        when(senderMock.getPassword()).thenReturn("Qwerty1234!");

        MailProperties.Receiver receiverMock = mock(MailProperties.Receiver.class);
        when(TestConfig.mailProperties.getReceiver()).thenReturn(receiverMock);
        when(receiverMock.getLogin()).thenReturn("llachows@gmail.com");
        when(TestConfig.mailProperties.getSubject()).thenReturn("test subject");
        when(TestConfig.mailProperties.getContent()).thenReturn("test content");
    }

    private static File createNewFolder(String s) {
        File newFile = new File(folder, s);
        newFile.mkdir();
        return newFile;
    }


    @Test
    public void shouldCallMethodsInRunMethod() throws Exception {
        //given
        File targetDirectory = new File(folder, "to-dir");
        //when
        copyTask.run();
        //then
        MimeMessage[] receivedMessages = greenMailRule.getReceivedMessages();
        MimeMessage receivedMessage = receivedMessages[0];

        assertThat(receivedMessages.length).isEqualTo(1);

        String sender = receivedMessage.getFrom()[0].toString();
        String receiver = receivedMessage.getAllRecipients()[0].toString();
        assertThat(sender).isEqualTo(TestConfig.mailProperties.getSender().getLogin());
        assertThat(receiver).isEqualTo(TestConfig.mailProperties.getReceiver().getLogin());

    }

    private boolean ifZipExist(File directory) {
        return Arrays.stream(directory.listFiles()).anyMatch(f -> {
            log.info(f.getName());
            return f.getName().equals("test.zip");
        });
    }

}
