package pl.siekoduje.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import static org.mockito.Mockito.mock;


@Configuration
@Import(AppConfig.class)
public class TestConfig {

    public static BackupProperties backupProperties = mock(BackupProperties.class);
    public static MailProperties mailProperties = mock(MailProperties.class);


    @Primary
    @Bean
    public BackupProperties backupPropertiesMock() {
        return backupProperties;
    }


    @Primary
    @Bean
    public MailProperties mailPropertiesMock() {
        return mailProperties;
    }

}
