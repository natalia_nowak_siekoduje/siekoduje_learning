package pl.siekoduje;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import pl.siekoduje.service.HelloWorldService;
import pl.siekoduje.utils.HelloWorldSentence;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class FirstSpringBootApplicationIT {

	@Autowired
	ApplicationContext applicationContext;


	@Test
	public void shouldReturnBeanHelloWorldSentence() {
		//when
		 HelloWorldSentence result = applicationContext.getBean(HelloWorldSentence.class);
		 //then
		 assertThat(result).isInstanceOf(HelloWorldSentence.class);
	}

	@Test
	public void shouldReturnBeanHelloWorldService() {
		//when
		HelloWorldService result = applicationContext.getBean(HelloWorldService.class);
		//then
		assertThat(result).isInstanceOf(HelloWorldService.class);
	}

	@Test
	public void shouldInjectDependency() {
		//when
		HelloWorldService service = applicationContext.getBean(HelloWorldService.class);
		HelloWorldSentence sentence = applicationContext.getBean(HelloWorldSentence.class);
		HelloWorldSentence result = service.getHelloWorldSentence();
		//then
		assertThat(result).isEqualTo(sentence);
	}


}
