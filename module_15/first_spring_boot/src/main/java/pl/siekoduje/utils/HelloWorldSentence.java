package pl.siekoduje.utils;

import org.springframework.stereotype.Component;

@Component
public class HelloWorldSentence {

    public String getHelloWorld() {
        return "Hello World!";
    }

}
