package pl.siekoduje.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.siekoduje.utils.HelloWorldSentence;

@Component
@Getter
public class HelloWorldService {
    @Autowired private HelloWorldSentence helloWorldSentence;

    public String helloWorld() {
        return helloWorldSentence.getHelloWorld();
    }
}