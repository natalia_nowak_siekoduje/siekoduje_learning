package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ApplicationContextImpl implements ApplicationContext {
    private final Map<String, Object> beans = new HashMap<>();
    @Override
    public <T> T createBean(String name, Class<T> type) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<T> declaredConstructor = type.getDeclaredConstructor();
        T object = declaredConstructor.newInstance();
        checkIfBeanAlreadyExsists(name);
        beans.put(name, object);

//        Class<ApplicationContextImpl> applicationContextClass = ApplicationContextImpl.class; //zmienna przechowująca obiekt reprezentujacy klase
        return object;
    }

    private <T> void checkIfBeanAlreadyExsists(String name) {
        if (beans.containsKey(name))
            throw new IllegalStateException("Bean '" + name + "' exists");
    }

    @Override
    public <T> T getBean(String name) {
         T bean = (T) beans.get(name);
        if(bean == null)
            throw new IllegalStateException("No bean '" + name + "'");
        return bean;
    }

    @Override
    public void injectBean(Object targetBean, String targetFieldName, String injectedBeanName) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {

//         opcja 1
//        Field[] declaredFields = targetBean.getClass().getDeclaredFields();
//        Field field = Arrays.stream(declaredFields).filter(f -> {
//            return f.getName().equals(targetFieldName);
//        }).findFirst().orElseThrow(NoSuchFieldError::new);
//        field.setAccessible(true);

//        opcja 2
        String setterName = getSetterNameFor(targetFieldName);
        log.info("Setter name: " + setterName);
        Method method = Arrays.stream(targetBean.getClass()
                .getDeclaredMethods())
                .filter(m -> {
                    return m.getName().equals(setterName);
                }).findFirst().orElseThrow(NoSuchMethodException::new);
        method.invoke(targetBean, beans.get(injectedBeanName));

    }

    private String getSetterNameFor(String name) {
        log.info("SetterName is: " + "set" + StringUtils.capitalize(name));
        return "set" + StringUtils.capitalize(name);
    }
}
