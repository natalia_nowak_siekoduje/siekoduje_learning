package pl.siekoduje;

import java.lang.reflect.InvocationTargetException;

public interface ApplicationContext {
    /**
     * Creates and caches bean.
     *
     * <p>Throws exception if bean for given name already exists or in case of any other problems.
     *
     * @param name bean name
     * @param type bean class
     * @param <T> the type of the bean
     * @return newly created bean
     */
    <T> T createBean(String name, Class<T> type) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
    /**
     * Returns cached bean.
     *
     * <p>Throws exception if bean doesn't exist or in case of any other problems.
     *
     * @param name bean name
     * @param <T> the type of the bean
     * @return cached bean
     */
    <T> T getBean(String name);
    /**
     * Injects bean into target bean.
     *
     * <p>Throws exception if injected bean doesn't exist or in case of any other problems.
     *
     * @param targetBean target bean where to inject other bean
     * @param targetFieldName field name where to inject bean
     * @param injectedBeanName the name of a bean which is being injected
     */
    void injectBean(Object targetBean, String targetFieldName, String injectedBeanName) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException;
}
