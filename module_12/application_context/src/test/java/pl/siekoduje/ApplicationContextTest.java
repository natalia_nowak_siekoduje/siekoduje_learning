package pl.siekoduje;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class ApplicationContextTest {
    private ApplicationContext applicationContext = new ApplicationContextImpl();

    @Test
    public void shouldCreateBean() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        // given
        // when
        Service service = applicationContext.createBean("my-service-bean", Service.class);
        // then
        assertThat(service).isNotNull();
    }

    @Test
    public void shouldThrowExceptionWhenBeanExists() throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        // given
        shouldCreateBean();
        // when
        Throwable throwable =
                catchThrowable(
                        () -> applicationContext.createBean("my-service-bean", Service.class));
        // then
        assertThat(throwable)
                .isNotNull()
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Bean 'my-service-bean' exists");
    }

    @Test
    public void shouldThrowExceptionWhenMissingBean() {
        // given
        // when
        Throwable throwable = catchThrowable(() -> applicationContext.getBean("bean-list")); // tu chyba powinno być "bean-list"
        // then
        assertThat(throwable)
                .isNotNull()
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("No bean 'bean-list'");
    }

    @Test
    public void shouldReturnBean() throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        // given
        shouldCreateBean();
        // when
        Service service = applicationContext.getBean("my-service-bean");
        // then
        assertThat(service).isNotNull();
    }

    @Test
    public void shouldInjectBean() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        // given
        Controller controller =
                applicationContext.createBean("my-controller-bean", Controller.class);
        Service service = applicationContext.createBean("my-service-bean", Service.class);

        // when
        applicationContext.injectBean(controller, "service", "my-service-bean");

        // then
        assertThat(controller.getService()).isEqualTo(service);
    }
}
