package pl.siekoduje;

public class Game {

    public static void main(String[] args)  {
        Ball ball = new Ball();
        Player player1 = new Player("John Smith", ball);
        Player player2 = new Player("Thomas Jones", ball);
        Player player3 = new Player("Conner Brown", ball);
        ball.setThrower(player1);
        Thread t1 = new Thread(() -> player1.play(player2));
        Thread t2 = new Thread(() -> player2.play(player3));
        Thread t3 = new Thread(() -> player3.play(player1));
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
