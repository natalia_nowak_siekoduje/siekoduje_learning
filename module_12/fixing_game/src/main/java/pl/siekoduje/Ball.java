package pl.siekoduje;

public class Ball {
    private Player thrower;

    public Player getThrower() {
        return thrower;
    }

    public void setThrower(Player thrower) {
        this.thrower = thrower;
    }
}
