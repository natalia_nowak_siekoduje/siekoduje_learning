package pl.siekoduje;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class Player {
    private static final Logger logger = LoggerFactory.getLogger(Player.class);
    private final String name;
//    private static final AtomicInteger throwsCounter = new AtomicInteger(); - liczniki nie są potrrzebne (YAGNI)
//    private static final AtomicInteger caughtCounter = new AtomicInteger();
    private  Ball ball;

    public Player(String name, Ball ball) {
        this.name = name;
        this.ball = ball;
    }

    public String getName() {
        return this.name;
    }

    public void play(Player catcher) {
        synchronized(ball) {
        while(!ball.getThrower().equals(this)){
            try {
                ball.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
            prepareThrow();
            throwBall(catcher);

        }
    }

    private void throwBall(Player catcher) {
//        throwsCounter.incrementAndGet();
        logger.info(this.getName() + " threw ball to " + catcher.getName());
        catcher.catchBall(this);
        ball.setThrower(catcher);
        logger.info("Now thrower is " + catcher.getName());
        ball.notifyAll();
    }

    private void prepareThrow() {
        try {
            Thread.sleep(100);
            logger.info(this.getName() + " is waiting");
        } catch (InterruptedException e) {
//            e.printStackTrace();
            logger.error("failed preparing");
        }
    }

    public void catchBall(Player thrower) {
//        caughtCounter.incrementAndGet();
        logger.info(this.getName() +" caught ball from " +  thrower.getName());
    }
}
