package pl.siekoduje;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.siekoduje.service.Service1;
import pl.siekoduje.service.Service2;

public class Application {

    public static void main(String[] args) {
        Service1 service1 = new Service1();
        Service2 service2 = new Service2();
        service1.process();
        service2.process();
    }
}