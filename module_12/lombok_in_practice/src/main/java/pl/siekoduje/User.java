package pl.siekoduje;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@AllArgsConstructor
@Getter
public class User {
    private final String firstName;
    private final String lastName;
    private String email;
    private final String phoneNumber;




}
