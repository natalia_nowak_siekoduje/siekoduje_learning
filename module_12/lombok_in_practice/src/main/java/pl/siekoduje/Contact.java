package pl.siekoduje;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Contact {
    private String phoneNumber;
    private String emailAddress;
}
