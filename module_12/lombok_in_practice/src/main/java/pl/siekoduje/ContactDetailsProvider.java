package pl.siekoduje;

import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class ContactDetailsProvider {
    private final UserRepository userRepository;

    public Contact getContactDetails(String firstName, String lastName) {
        Optional<User> userOptional = userRepository.findUser(firstName, lastName);
        return mapUserToContact(userOptional);
    }

    public Contact getContactDetails(String email) {
        Optional<User> userOptional = userRepository.findUser(email);
         return mapUserToContact(userOptional);
    }

    private Contact mapUserToContact(Optional<User> userOptional) {
        return  userOptional.map(this::getContactFromUser).orElseThrow(IllegalArgumentException::new);
    }
    private Contact getContactFromUser(User user) {
        return new Contact(user.getPhoneNumber(), user.getEmail());
    }

    public void addUser(User user) {
        userRepository.addUser(user);
    }
}
