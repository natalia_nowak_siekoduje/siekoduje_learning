package pl.siekoduje;

import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class UserRepository {
    private final List<User> userList;

    public Optional<User> findUser(String firstName, String lastName) {
        return userList.stream()
                .filter(u -> {
                   return  u.getFirstName().equals(firstName) && u.getLastName().equals(lastName);
                }).findFirst();
    }

    public Optional<User> findUser(String email){
        return userList.stream()
                .filter(u -> {
                    return  u.getEmail().equals(email);
                }).findFirst();
    }

    public void addUser(User user) {
        userList.add(user);
    }
}
