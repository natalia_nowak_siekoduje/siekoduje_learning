package pl.siekoduje;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ContactDetailsAppTest {
    private ContactDetailsProvider contactDetailsProvider =
            new ContactDetailsProvider(new UserRepository(new ArrayList<>()));


    @Test
    public void shouldReturnContactDetailsForName() {
        // given
        contactDetailsProvider.addUser(new User("John", "Smith", "john.smith@mail.com", "+48 552 242 723"));
        // when
        Contact contact = contactDetailsProvider.getContactDetails("John", "Smith");
        // then
        assertThat(contact).isNotNull();
        assertThat(contact.getPhoneNumber()).isEqualTo("+48 552 242 723");
        assertThat(contact.getEmailAddress()).isEqualTo("john.smith@mail.com");
    }

    @Test
    public void shouldReturnContactDetailsForEmail() {
        // given
        contactDetailsProvider.addUser(new User("John", "Smith", "john.smith@mail.com", "+48 552 242 723"));
        // when
        Contact contact = contactDetailsProvider.getContactDetails("john.smith@mail.com");
        // then
        assertThat(contact).isNotNull();
        assertThat(contact.getPhoneNumber()).isEqualTo("+48 552 242 723");
        assertThat(contact.getEmailAddress()).isEqualTo("john.smith@mail.com");
    }
}
