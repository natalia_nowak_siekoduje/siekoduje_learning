package pl.siekoduje.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Service1 {
    private static final Logger logger = LoggerFactory.getLogger(Service1.class);


    public void process() {
        logger.error("this is error");
        logger.warn("this is a warning");
        logger.info("this is info");
        logger.debug("this is debug");
        logger.trace("this is trace");
        Exception ex = new IllegalArgumentException("wrong argument");
        logger.error("Exception example", ex);
    }
}
