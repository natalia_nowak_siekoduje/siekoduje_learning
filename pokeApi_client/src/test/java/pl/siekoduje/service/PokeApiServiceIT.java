package pl.siekoduje.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pl.siekoduje.connector.PokeApiConnector;
import pl.siekoduje.model.NamedApiResource;
import pl.siekoduje.model.PokemonType;
import pl.siekoduje.model.TypeRelations;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;

@SpringBootTest
class PokeApiServiceIT {
    @Autowired
    PokeApiService service;
    @MockBean
    PokeApiConnector pokeApiConnector;

    private static Stream<Arguments> provideArgumentsForOneTypePokemonVictim() {
        return Stream.of(
                Arguments.of(PokemonType.builder().name("fire").build(), PokemonType.builder().name("grass").build(), 2, TypeRelations.builder()
                        .doubleDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("grass").build()))
                        .noDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("poison").build()))
                        .halfDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("steel").build())).build()),
                Arguments.of(PokemonType.builder().name("water").build(), PokemonType.builder().name("normal").build(), 1, TypeRelations.builder()
                        .noDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("grass").build()))
                        .doubleDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("poison").build()))
                        .halfDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("steel").build())).build()),
                Arguments.of(PokemonType.builder().name("fire").build(), PokemonType.builder().name("rock").build(), 0.5, TypeRelations.builder()
                        .halfDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("rock").build()))
                        .doubleDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("poison").build()))
                        .noDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("steel").build())).build())
        );
    }

    private static Stream<Arguments> provideArgumentsForFewTypesPokemonVictim() {
        return Stream.of(
                Arguments.of(PokemonType.builder().name("psychic").build(), Arrays.asList(PokemonType.builder().name("poison").build(), PokemonType.builder().name("dark").build()), 0.0, TypeRelations.builder()
                        .noDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("dark").build()))
                        .doubleDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("poison").build()))
                        .halfDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("steel").build())).build()),
                Arguments.of(PokemonType.builder().name("fighting").build(), Arrays.asList(PokemonType.builder().name("ice").build(), PokemonType.builder().name("rock").build()), 4, TypeRelations.builder()
                        .doubleDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("ice").build(), NamedApiResource.builder()
                                .name("rock").build()))
                        .noDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("poison").build()))
                        .halfDamageTo(Arrays.asList(NamedApiResource.builder()
                                .name("steel").build())).build())

        );
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForOneTypePokemonVictim")
    public void shouldCountScoreInAttackForOneTypePokemon(PokemonType attacker, PokemonType victim, double score, TypeRelations typeRelations) {
        //given
        given(pokeApiConnector.getTypeRelations(attacker)).willReturn(typeRelations);
        //when
        double result = service.attack(attacker, victim);
        //then
        assertThat(result).isEqualTo(score);
    }

    @ParameterizedTest
    @MethodSource("provideArgumentsForFewTypesPokemonVictim")
    public void shouldCountScoreInAttackForFewTypesPokemon(PokemonType attacker, List<PokemonType> victim, double score, TypeRelations typeRelations) {
        //given
        given(pokeApiConnector.getTypeRelations(attacker)).willReturn(typeRelations);
        //when
        double result = service.attack(attacker, victim);
        //then
        assertThat(result).isEqualTo(score);
    }

    @Test
    public void shouldNotReturnTypeRelationsWhenWrongAttackerType() {
        //given
        PokemonType wrongTypePokemon = PokemonType.builder().name("wrongType").build();
        given(pokeApiConnector.getTypeRelations(wrongTypePokemon)).willThrow(new ConnectorException("404 Not Found: [Not Found]"));
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            service.attack(wrongTypePokemon, PokemonType.builder().name("poison").build());
                        });
        //then
        assertThat(thrown).isInstanceOf(ConnectorException.class);
        assertThat(thrown.getMessage()).isEqualTo("404 Not Found: [Not Found]");
    }
}