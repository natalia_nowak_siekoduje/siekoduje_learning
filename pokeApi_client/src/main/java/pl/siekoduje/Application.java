package pl.siekoduje;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.siekoduje.service.PokeApiService;


@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    PokeApiService pokeApiService;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
    }
}
