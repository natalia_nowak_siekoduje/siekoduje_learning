package pl.siekoduje.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.siekoduje.connector.PokeApiConnector;
import pl.siekoduje.model.NamedApiResource;
import pl.siekoduje.model.PokemonType;
import pl.siekoduje.model.TypeRelations;

import java.util.List;

@RequiredArgsConstructor
@Component
public class PokeApiService {

    private final PokeApiConnector pokeApiConnector;

    public double attack(PokemonType attacker, PokemonType victim) {
        return countScore(attacker, victim);
    }

    public double attack(PokemonType attacker, List<PokemonType> victim) {
        double damageScore = 1;
        return victim.stream()
                .map(pokemon -> countScore(attacker, pokemon))
                .reduce(damageScore, (overallScore, score) -> overallScore * score);
    }

    private double countScore(PokemonType attacker, PokemonType victim) {
        TypeRelations attackerDamageRelations = pokeApiConnector.getTypeRelations(attacker);
        if (containsType(attackerDamageRelations.getDoubleDamageTo(), victim.getName())) {
            return 2.0;
        } else if (containsType(attackerDamageRelations.getHalfDamageTo(), victim.getName())) {
            return 0.5;
        } else if (containsType(attackerDamageRelations.getNoDamageTo(), victim.getName())) {
            return 0.0;
        }
        return 1.0;
    }

    private boolean containsType(List<NamedApiResource> damageToList, String victim) {
        return damageToList.stream()
                .map(namedApiResource -> namedApiResource.getName())
                .anyMatch(n -> n.equals(victim));
    }
}
