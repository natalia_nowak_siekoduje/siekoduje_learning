package pl.siekoduje.service;

public class ConnectorException extends RuntimeException {
    public ConnectorException(String message) {
        super(message);
    }
}
