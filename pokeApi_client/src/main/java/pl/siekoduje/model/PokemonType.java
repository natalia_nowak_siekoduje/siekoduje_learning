package pl.siekoduje.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PokemonType {
    private Integer id;
    private String name;
    @JsonProperty("damage_relations")
    private TypeRelations damageRelations;
}
