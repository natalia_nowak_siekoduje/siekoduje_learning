package pl.siekoduje.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class TypeRelations {
    @JsonProperty("no_damage_to")
    private List<NamedApiResource> noDamageTo;

    @JsonProperty("half_damage_to")
    private List<NamedApiResource> halfDamageTo;

    @JsonProperty("double_damage_to")
    private List<NamedApiResource> doubleDamageTo;
}
