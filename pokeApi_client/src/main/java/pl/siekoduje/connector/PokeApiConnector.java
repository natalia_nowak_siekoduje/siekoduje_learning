package pl.siekoduje.connector;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import pl.siekoduje.model.PokemonType;
import pl.siekoduje.model.TypeRelations;
import pl.siekoduje.service.ConnectorException;

@Component
@Getter
@Setter
@Slf4j
public class PokeApiConnector {

    private final RestTemplate restTemplate;
    private final  String pokeApiUrl;

    @Autowired
    public PokeApiConnector(RestTemplateBuilder builder, @Value("${pokeApi.url}") String pokeApiUrl) {
        this.restTemplate = builder.build();
        this.pokeApiUrl = pokeApiUrl;
    }

    public TypeRelations getTypeRelations(PokemonType pokemon) {
        String pokemonType = pokemon.getName();
        String url = pokeApiUrl + "/" + pokemonType;
        try{
            ResponseEntity<PokemonType> responseEntity = restTemplate.getForEntity(url, PokemonType.class);
            return responseEntity.getBody().getDamageRelations();
        }catch (HttpClientErrorException e) {
            log.warn(e.getMessage());
            throw new ConnectorException(e.getMessage());
        }

    }

}
