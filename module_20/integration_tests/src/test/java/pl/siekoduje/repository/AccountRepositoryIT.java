package pl.siekoduje.repository;

import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import pl.siekoduje.model.Account;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.context.jdbc.SqlMergeMode.MergeMode.MERGE;

@Slf4j
@DataJpaTest
@Sql(scripts = { "classpath:clear_table.sql","classpath:data.sql"},
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@SqlMergeMode(MERGE)
public class AccountRepositoryIT {
    private final Account account1 = Account.builder()
            .accountNumber("111111111111113")
            .checkDigit(30)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(10, 12))
            .balance(BigDecimal.valueOf(20.00))
            .debitLimit(1000.0)
            .build();
    private final Account account2 = Account.builder()
            .accountNumber("111111111111118")
            .checkDigit(00)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(12, 12))
            .balance(BigDecimal.valueOf(20000.00))
            .debitLimit(5000.0)
            .build();
    private final RecursiveComparisonConfiguration recursiveComparisonConfig = RecursiveComparisonConfiguration.builder()
            .withIgnoredFields("balance", "id")
            .build();
    @Autowired
    AccountRepository accountRepository;

    @Test
    public void shouldFindByAccountNumber() {
        //given
        //when
        Account result = accountRepository.findByAccountNumber(account1.getAccountNumber());
        //then
        assertThat(result).usingRecursiveComparison(recursiveComparisonConfig)
                .isEqualTo(account1);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'PL', 10, 20000.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldReturnAccountsWithNotNullDebitLimit() {
        //given
        String accountWithNullDebitLimit = "222222222222222";
        //when
        List<Account> result = accountRepository.findByDebitLimitIsNotNull();
        //then
        Account unexpectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        assertThat(result).hasSize(2);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).doesNotContain(unexpectedAccount);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactlyInAnyOrder(account1, account2);

    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, balance, debit_limit, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'PL',30, 20000.00, 5000, TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldFindByCheckDigitAndCountryCode() {
        //given
        //when
        List<Account> result = accountRepository.findByCheckDigitAndCountryCode(account1.getCheckDigit(), account1.getCountryCode());
        //then
        Account expectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(account1.getCheckDigit())
                .countryCode(account1.getCountryCode())
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(5000.0)
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        assertThat(result).hasSize(2);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactlyInAnyOrder(expectedAccount, account1);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'DE', 10, 5000.0,  20000.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldFindByCountryCodeIsNot() {
        //given
        String countryCode = "DE";
        //when
        List<Account> result = accountRepository.findByCountryCodeIsNot(countryCode);
        //then
        Account expectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode(countryCode)
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(5000.0)
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        assertThat(result).hasSize(2);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactlyInAnyOrder(account1, account2);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'DE', 10, 1000,  20.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldFindByDebitLimit() {
        //given
        //when
        List<Account> result = accountRepository.findByDebitLimitIs(account1.getDebitLimit());
        //then
        Account expectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("DE")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(account1.getDebitLimit())
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        assertThat(result).hasSize(2);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactlyInAnyOrder(expectedAccount, account1);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'DE', 10, 1000,  20.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2018-12-12', '12:12')"
    })
    @Test
    public void shouldFindByCreationDateAfter() {
        //given
        //when
        List<Account> result = accountRepository.findByCreationDateAfter(LocalDate.of(2018, 10, 10));
        //then
        Account expectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("DE")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2018, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(account1.getDebitLimit())
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        assertThat(result).hasSize(1);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).contains(expectedAccount);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222218', 'DE', 10, 1000,  20.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2018-12-12', '12:12')"
    })
    @Test
    public void shouldDeleteByAccountNumberEndingWith() {
        //given
        //when
        accountRepository.deleteByAccountNumberEndingWith("18");
        //then
        assertThat(accountRepository.findAll()).hasSize(1);
        assertThat(accountRepository.findAll()).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactly(account1);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'PL', 30, 5000, 20000, TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2018-12-12', '12:12')"
    })
    @Test
    public void shouldFindByCheckDigitAndAccountNumber() {
        //given
        String newAccountNumber = "222222222222222";
        //when
        List<Account> result = accountRepository.findByCheckDigitAndAccountNumber(account1.getCheckDigit(), newAccountNumber);
        //then
        Account expectedAccount = Account.builder()
                .accountNumber(newAccountNumber)
                .checkDigit(account1.getCheckDigit())
                .countryCode(account1.getCountryCode())
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2018, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        assertThat(result).hasSize(1);
        assertThat(result).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).contains(expectedAccount);
    }

    @Sql(statements = {
            "INSERT INTO accounts (id, account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( 1, '222222222222222', 'PL', 10, 1000,  20000.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldUpdateBalanceByAccountId() {
        //given
        Account account = Account.builder()
                .id(1)
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(1000.0)
                .build();
        Integer newAccountId = 1;
        BigDecimal newBalance = BigDecimal.valueOf(100);
        //
        // when
        int updatedRows = accountRepository.updateBalance(newAccountId, newBalance);
        //then
        assertThat(updatedRows).isEqualTo(1);
        assertThat(accountRepository.findById(newAccountId).get().getBalance()).isEqualByComparingTo(newBalance);
        assertThat(accountRepository.findById(newAccountId).get()).usingRecursiveComparison(recursiveComparisonConfig)
                .isEqualTo(account);
    }


    @Test
    public void shouldSave() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        Account savedAccount = accountRepository.save(account);
        //then
        assertThat(accountRepository.findAll())
                .hasSize(3);
        Account result = accountRepository.findById(savedAccount.getId()).get();
        assertThat(result).usingRecursiveComparison(recursiveComparisonConfig)
                .isEqualTo(account);
    }

    @Test
    public void shouldUpdate() {
        // given
        BigDecimal newBalance = BigDecimal.valueOf(90.0);
        Account account = accountRepository.findAll().get(0);
        account.setBalance(newBalance);
        account.setDebitLimit(6000.0);
        // when
        accountRepository.save(account);
        // then
        Account result = accountRepository.findById(account.getId()).get();
        assertThat(result.getBalance()).isEqualByComparingTo(newBalance);
        assertThat(result).usingRecursiveComparison(recursiveComparisonConfig)
                .isEqualTo(account);
    }


    @Test
    public void shouldFindallById() {
        // given
        Account account3 = accountRepository.findAll().get(0);
        Account account4 = accountRepository.findAll().get(1);
        Integer account3Id = account3.getId();
        Integer account4Id = account4.getId();
        List<Integer> accounts = Arrays.asList(account3Id, account4Id);
        // when
        List foundAccounts = accountRepository.findAllById(accounts);
        // then
        assertThat(foundAccounts).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).contains(account1, account2);
    }


    @Test
    public void shouldFindAllByExample() {
        // given
        Account accountExample = new Account();
        accountExample.setDebitLimit(1000.00);
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(accountExample, exampleMatcher);
        // when
        List<Account> foundAccounts = accountRepository.findAll(example);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(1);
        assertThat(foundAccounts).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).contains(account1);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'PL', 10, 5000,  20000.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldFindAllByExampleSorted() {
        // given
        Account accountExample = new Account();
        accountExample.setDebitLimit(5000.00);
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(accountExample, exampleMatcher);
        Sort sort = Sort.by("id").ascending();
        // when
        List<Account> foundAccounts = accountRepository.findAll(example, sort);
        // then
        Account expectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactly(account2, expectedAccount);
    }

    @Test
    public void shouldFindAll() {
        // given
        // when
        List<Account> foundAccounts = accountRepository.findAll();
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).containsExactlyInAnyOrder(account1, account2);

    }

    @Test
    public void shouldFindAllSorted() {
        // given
        Sort sort = Sort.by("id").ascending();
        // when
        List<Account> foundAccounts = accountRepository.findAll(sort);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig)
                .containsExactly(account1, account2);
    }

    @Test
    public void shouldSaveAll() {
        // given
        Account newAccount1 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        Account newAccount2 = Account.builder()
                .accountNumber("33333333333")
                .checkDigit(20)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        List<Account> accounts = Arrays.asList(newAccount1, newAccount2);
        // when
        accountRepository.saveAll(accounts);
        // then
        assertThat(accounts).contains(newAccount1, newAccount2);
    }

    @Sql(statements = {
            "INSERT INTO accounts (account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( '222222222222222', 'PL', 10, 5000,  20000.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldDeleteInBatch() {
        // given
        List<Account> accounts = Arrays.asList(accountRepository.findByAccountNumber(account1.getAccountNumber()), accountRepository.findByAccountNumber(account2.getAccountNumber()));
        // when
        accountRepository.deleteInBatch(accounts);
        // then
        Account expectedAccount = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        assertThat(accountRepository.findAll()).hasSize(1);
        assertThat(accountRepository.findAll()).usingRecursiveFieldByFieldElementComparator(recursiveComparisonConfig).contains(expectedAccount);
    }

    @Test
    public void shouldDeleteAllInBatch() {
        // given
        // when
        accountRepository.deleteAllInBatch();
        // then
        assertThat(accountRepository.findAll()).isEmpty();
    }

    @Sql(statements = {
            "INSERT INTO accounts (id, account_number, country_code, check_digit, debit_limit,  balance, update_timestamp, creation_date, creation_time) " +
                    "VALUES ( 1, '222222222222222', 'PL', 10, 5000,  20000.00 , TO_TIMESTAMP('2021-03-10 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12')"
    })
    @Test
    public void shouldGetOne() {
        // given
        Account expectedAccount = Account.builder()
                .id(1)
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        // when
        Account foundAccount = accountRepository.getOne(1);
        // then
        assertThat(foundAccount).usingRecursiveComparison(recursiveComparisonConfig)
                .isEqualTo(expectedAccount);
    }
}
