
INSERT INTO accounts (account_number, country_code, check_digit, balance, debit_limit,  update_timestamp, creation_date, creation_time)
VALUES ( '111111111111113', 'PL', 30, 20.0 , 1000.0, TO_TIMESTAMP('2021-03-02 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '10:12');

INSERT INTO accounts (account_number, country_code, check_digit, balance, debit_limit, update_timestamp, creation_date, creation_time)
VALUES ( '111111111111118', 'PL', 00, 20000.0 , 5000.0, TO_TIMESTAMP('2021-03-01 11:20:20',  'YYYY-MM-DD HH24:MI:SS' ), '2012-12-12', '12:12');