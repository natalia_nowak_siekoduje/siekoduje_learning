package pl.siekoduje.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.siekoduje.model.Account;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository  extends JpaRepository<Account, Integer> {

    Account findByAccountNumber(String accountNumber);

    List<Account> findByCheckDigitAndCountryCode(Integer checkDigit, String countryCode);

    List<Account> findByDebitLimitIsNotNull();

    List<Account> findByCountryCodeIsNot(String countryCode);

    List<Account> findByDebitLimitIs(Double debitLimit);

    List<Account> findByCreationDateAfter(LocalDate date);

    void deleteByAccountNumberEndingWith(String ending);

    @Query("SELECT a FROM Account a WHERE a.checkDigit = :checkDigit AND a.accountNumber = :accountNumber")
    List<Account> findByCheckDigitAndAccountNumber(@Param("checkDigit") Integer checkDigit, @Param("accountNumber") String accountNumber);


    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE Account a SET a.balance = :balance WHERE a.id = :id")
    int updateBalance(@Param("id") Integer id, @Param("balance") BigDecimal balance);

}
