package pl.siekoduje.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import pl.siekoduje.model.Account;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
public class AccountRepositoryIT {
    private final Account account1 = Account.builder()
            .accountNumber("111111111111113")
            .checkDigit(30)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(10, 12))
            .balance(BigDecimal.valueOf(20.00))
            .debitLimit(5000.0)
            .build();
    private final Account account2 = Account.builder()
            .accountNumber("111111111111118")
            .checkDigit(00)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(12, 12))
            .balance(BigDecimal.valueOf(20000.00))
            .debitLimit(5000.0)
            .build();
    @Autowired
    AccountRepository accountRepository;

    @BeforeEach
    public void beforeEach() {
        prepareInitialData();
    }

    private void prepareInitialData() {
        accountRepository.deleteAllInBatch();
        accountRepository.save(account1);
        accountRepository.save(account2);
    }


    @Test
    public void shouldSave() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        Account savedAccount = accountRepository.save(account);
        //then
        assertThat(accountRepository.findAll())
                .hasSize(3);
        Account result = accountRepository.findById(savedAccount.getId()).get();
        compareAccounts(result, account);
    }

    @Test
    public void shouldUpdate() {
        // given
        Account account = accountRepository.findAll().get(0);
        account.setBalance(BigDecimal.valueOf(90.0));
        account.setDebitLimit(6000.0);
        // when
        accountRepository.save(account);
        // then
        Account result = accountRepository.findById(account.getId()).get();
        compareAccounts(result, account);
    }


    @Test
    public void shouldFindById() {
        // given
        Account account3 = accountRepository.findAll().get(0);
        Account account4 = accountRepository.findAll().get(1);
        Integer account3Id = account3.getId();
        Integer account4Id = account4.getId();
        List<Integer> accounts = Arrays.asList(account3Id, account4Id);
        // when
        List foundAccounts = accountRepository.findAllById(accounts);
        // then
        assertThat(foundAccounts).contains(account1, account2);
    }


    @Test
    public void shouldFindAllByExample() {
        // given
        Account accountExample = new Account();
        accountExample.setDebitLimit(5000.00);
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(accountExample, exampleMatcher);

        // when
        List<Account> foundAccounts = accountRepository.findAll(example);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).contains(account1, account2);
    }

    @Test
    public void shouldFindAllByExampleSorted() {
        // given
        Account accountExample = new Account();
        accountExample.setDebitLimit(5000.00);
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(accountExample, exampleMatcher);
        Sort sort = Sort.by("id").ascending();
        // when
        List<Account> foundAccounts = accountRepository.findAll(example, sort);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).contains(account1, account2);
        compareAccounts(foundAccounts.get(0), account1);
        compareAccounts(foundAccounts.get(1), account2);
    }

    @Test
    public void shouldFindAll() {
        // given
        // when
        List foundAccounts = accountRepository.findAll();
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).contains(account1, account2);

    }

    @Test
    public void shouldFindAllSorted() {
        // given
        Sort sort = Sort.by("id").ascending();
        // when
        List<Account> foundAccounts = accountRepository.findAll(sort);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        compareAccounts(foundAccounts.get(0), account1);
        compareAccounts(foundAccounts.get(1), account2);

    }

    @Test
    public void shouldSaveAll() {
        // given
        Account account3 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        Account account4 = Account.builder()
                .accountNumber("33333333333")
                .checkDigit(20)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        List<Account> accounts = Arrays.asList(account3, account4);
        // when
        accountRepository.saveAll(accounts);
        // then
        assertThat(accounts).contains(account3, account4);
    }


    @Test
    public void shouldDeleteInBatch() {
        // given
        Account account3 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        accountRepository.save(account3);
        List<Account> accounts = Arrays.asList(account2, account1);
        // when
        accountRepository.deleteInBatch(accounts);
        // then

        assertThat(accountRepository.findAll()).hasSize(1);
        assertThat(accountRepository.findAll()).contains(account3);

    }

    @Test
    public void shouldDeleteAllInBatch() {
        // given
        Account account3 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        accountRepository.save(account3);
        List<Account> accounts = Arrays.asList(account2, account1);
        // when
        accountRepository.deleteAllInBatch();
        // then

        assertThat(accountRepository.findAll()).isEmpty();
    }

    @Test
    @Transactional
    public void shouldGetOne() {
        // given
        Account account = accountRepository.findAll().get(0);
        // when
        Account foundAccount = accountRepository.getOne(account.getId());
        // then
        compareAccounts(foundAccount, account1);
    }

    private void compareAccounts(Account a1, Account a2) {
        assertThat(a1.getAccountNumber()).isEqualTo(a2.getAccountNumber());
        assertThat(a1.getCheckDigit()).isEqualTo(a2.getCheckDigit());
        assertThat(a1.getCountryCode()).isEqualTo(a2.getCountryCode());
        assertThat(a1.getBalance()).isEqualByComparingTo(a2.getBalance());
        assertThat(a1.getDebitLimit()).isEqualTo(a2.getDebitLimit());
        assertThat(a1.getCreationTime()).isEqualTo(a2.getCreationTime());
        assertThat(a1.getCreationDate()).isEqualTo(a2.getCreationDate());
        assertThat(a1.getUpdateTimestamp()).isEqualTo(a2.getUpdateTimestamp());
    }

}
