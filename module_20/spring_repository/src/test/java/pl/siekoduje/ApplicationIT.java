package pl.siekoduje;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.CrudRepository;
import pl.siekoduje.repository.AccountRepository;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class ApplicationIT {
    @Autowired
    private ApplicationContext context;

    @Test
    public void shouldCreateAccountRepositoryBean() {
        // given
        // when
        AccountRepository accountRepository = context.getBean(AccountRepository.class);
        // then
        assertThat(accountRepository).isNotNull();
        assertThat(accountRepository).isInstanceOf(CrudRepository.class);
    }
}
