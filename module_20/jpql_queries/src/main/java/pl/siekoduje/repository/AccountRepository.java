package pl.siekoduje.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.siekoduje.model.Account;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    @Query("SELECT a FROM Account a WHERE a.checkDigit = :checkDigit AND a.countryCode = :countryCode")
    List<Account> findByCheckDigitAndCountryCode(@Param("checkDigit") Integer checkDigit, @Param("countryCode") String countryCode);


    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("UPDATE Account a SET a.balance = :balance WHERE a.id = :id")
    int updateBalance(@Param("id") Integer id, @Param("balance") BigDecimal balance);
}
