package pl.siekoduje.service;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import pl.siekoduje.model.Account;
import pl.siekoduje.repository.AccountRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class AccountServiceIT {
    private Account account1 = Account.builder()
            .accountNumber("111111111111113")
            .checkDigit(30)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(10, 12))
            .balance(BigDecimal.valueOf(20.00))
            .debitLimit(5000.0)
            .build();
    private Account account2 = Account.builder()
            .accountNumber("111111111111118")
            .checkDigit(00)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(12, 12))
            .balance(BigDecimal.valueOf(20000.00))
            .debitLimit(5000.0)
            .build();
    @Autowired
    private AccountService service;
    @Autowired
    private AccountRepository repository;

    @BeforeEach
    public void setUp() {
        repository.deleteAllInBatch();
        account1 = repository.save(account1);
        account2 = repository.save(account2);
    }


    @Test
    void shouldCreateAccount() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111118")
                .checkDigit(00)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(account, exampleMatcher);
        //when
        service.create(account);
        //then
        assertThat(repository.exists(example)).isTrue();
    }


    @Test
    void shouldGetAccount() {
        //given
        //when
        Account result = service.get(account1.getId());
        //then
        assertThat(account1).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(result);
    }

    @Test
    void shouldNotGetAccountWhenWrongId() {
        //given
        //when
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.get(-1));
        //then
        assertThat(thrown.getMessage()).isEqualTo("There is no account with id: " + -1);
    }

    @Test
    void shouldUpdateAccount() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111118")
                .checkDigit(00)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        Account updatedAccount = service.update(account1.getId(), account);
        account.setId(account1.getId());
        //then
        assertThat(repository.findById(account1.getId()).get()).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account);
    }

    @Test
    void shouldNotUpdateAccountWhenWrongId() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111118")
                .checkDigit(00)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () -> service.update(-1, account));
        //then
        assertThat(thrown.getMessage()).isEqualTo("There is no account with id: " + -1);
    }

    @Test
    void shouldDeleteAccount() {
        //given
        //when
        service.delete(account1);
        //then
        assertThat(repository.findById(account1.getId())).isEmpty();
    }

    @Test
    void shouldNotDeleteAccountWhenWrongAccountGiven() {
        //given
        Account account = Account.builder()
                .accountNumber("111111111111118")
                .checkDigit(00)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        service.delete(account);
        //then
        assertThat(repository.findAll()).hasSize(2);
        assertThat(repository.findAll()).contains(account1, account2);
    }
}