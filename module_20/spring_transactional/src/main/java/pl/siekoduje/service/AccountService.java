package pl.siekoduje.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;
import pl.siekoduje.model.Account;
import pl.siekoduje.repository.AccountRepository;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository repository;

    @Transactional
    public Account create(Account account) {
        return repository.save(account);
    }

    @Transactional(readOnly = true)
    public Account get(int id) {
        return repository.findById(id).orElseThrow(() -> new IllegalArgumentException("There is no account with id: " + id));
    }

    @Transactional
    public Account update(int id, Account updatedAccount) {
        Account account = repository.findById(id).orElseThrow(() -> new IllegalArgumentException("There is no account with id: " + id));
        account.setDebitLimit(updatedAccount.getDebitLimit());
        account.setBalance(updatedAccount.getBalance());
        account.setUpdateTimestamp(updatedAccount.getUpdateTimestamp());
        account.setCountryCode(updatedAccount.getCountryCode());
        account.setCheckDigit(updatedAccount.getCheckDigit());
        account.setAccountNumber(updatedAccount.getAccountNumber());
        account.setCreationDate(updatedAccount.getCreationDate());
        account.setCreationTime(updatedAccount.getCreationTime());
        return repository.save(account);
    }

    @Transactional
    public void delete(Account account) {
        repository.delete(account);
    }
}
