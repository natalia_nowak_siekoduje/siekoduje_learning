package pl.siekoduje.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Sort;
import pl.siekoduje.model.Account;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DataJpaTest
public class AccountRepositoryIT {
    private final Account account1 = Account.builder()
            .accountNumber("111111111111113")
            .checkDigit(30)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(10, 12))
            .balance(BigDecimal.valueOf(20.00))
            .debitLimit(1000.0)
            .build();
    private final Account account2 = Account.builder()
            .accountNumber("111111111111118")
            .checkDigit(00)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(12, 12))
            .balance(BigDecimal.valueOf(20000.00))
            .debitLimit(5000.0)
            .build();
    @Autowired
    AccountRepository accountRepository;

    @BeforeEach
    public void beforeEach() {
        prepareInitialData();
    }

    private void prepareInitialData() {
        accountRepository.deleteAllInBatch();
        accountRepository.save(account1);
        accountRepository.save(account2);
    }

    @Test
    public void shouldFindByAccountNumber() {
        //given

        //when
        Account result = accountRepository.findByAccountNumber(account1.getAccountNumber());
        //then
        assertThat(result).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account1);
    }

    @Test
    public void shouldReturnAccountsWithNotNullDebitLimit() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        accountRepository.save(account);
        //when
        List<Account> result = accountRepository.findByDebitLimitIsNotNull();
        //then
        assertThat(result).hasSize(2);
        assertThat(result).doesNotContain(account);
    }

    @Test
    public void shouldFindByCheckDigitAndCountryCode() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(account1.getCheckDigit())
                .countryCode(account1.getCountryCode())
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(5000.0)
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        accountRepository.save(account);
        //when
        List<Account> result = accountRepository.findByCheckDigitAndCountryCode(account.getCheckDigit(), account.getCountryCode());
        //then
        assertThat(result).hasSize(2);
        assertThat(result).containsExactlyInAnyOrder(account, account1);
    }

    @Test
    public void shouldFindByCountryCodeIsNot() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("DE")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(5000.0)
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        accountRepository.save(account);
        //when
        List<Account> result = accountRepository.findByCountryCodeIsNot(account.getCountryCode());
        //then
        assertThat(result).hasSize(2);
        assertThat(result).containsExactlyInAnyOrder(account2, account1);
    }

    @Test
    public void shouldFindByDebitLimit() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("DE")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(account1.getDebitLimit())
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        accountRepository.save(account);
        //when
        List<Account> result = accountRepository.findByDebitLimitIs(account.getDebitLimit());
        //then
        assertThat(result).hasSize(2);
        assertThat(result).containsExactlyInAnyOrder(account, account1);
    }


    @Test
    public void shouldFindByCreationDateAfter() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("DE")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2018, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(account1.getDebitLimit())
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        accountRepository.save(account);
        //when
        List<Account> result = accountRepository.findByCreationDateAfter(LocalDate.of(2018, 10, 10));
        //then
        assertThat(result).hasSize(1);
        assertThat(result).contains(account);
    }

    @Test
    public void shouldDeleteByAccountNumberEndingWith() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222218")
                .checkDigit(10)
                .countryCode("DE")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2018, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .debitLimit(account1.getDebitLimit())
                .balance(BigDecimal.valueOf(20000.00))
                .build();
        accountRepository.save(account);
        //when
        accountRepository.deleteByAccountNumberEndingWith("18");
        //then
        assertThat(accountRepository.findAll()).hasSize(1);
    }

    @Test
    public void shouldSave() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        Account savedAccount = accountRepository.save(account);
        //then
        assertThat(accountRepository.findAll())
                .hasSize(3);
        Account result = accountRepository.findById(savedAccount.getId()).get();
        assertThat(result).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account);
    }

    @Test
    public void shouldUpdate() {
        // given
        Account account = accountRepository.findAll().get(0);
        account.setBalance(BigDecimal.valueOf(90.0));
        account.setDebitLimit(6000.0);
        // when
        accountRepository.save(account);
        // then
        Account result = accountRepository.findById(account.getId()).get();
        assertThat(result).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account);
    }


    @Test
    public void shouldFindById() {
        // given
        Account account3 = accountRepository.findAll().get(0);
        Account account4 = accountRepository.findAll().get(1);
        Integer account3Id = account3.getId();
        Integer account4Id = account4.getId();
        List<Integer> accounts = Arrays.asList(account3Id, account4Id);
        // when
        List foundAccounts = accountRepository.findAllById(accounts);
        // then
        assertThat(foundAccounts).contains(account1, account2);
    }


    @Test
    public void shouldFindAllByExample() {
        // given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        accountRepository.save(account);
        Account accountExample = new Account();
        accountExample.setDebitLimit(5000.00);
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(accountExample, exampleMatcher);

        // when
        List<Account> foundAccounts = accountRepository.findAll(example);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).containsExactlyInAnyOrder(account2, account);
    }

    @Test
    public void shouldFindAllByExampleSorted() {
        // given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        accountRepository.save(account);
        Account accountExample = new Account();
        accountExample.setDebitLimit(5000.00);
        ExampleMatcher exampleMatcher = ExampleMatcher.matchingAny();
        Example<Account> example = Example.of(accountExample, exampleMatcher);
        Sort sort = Sort.by("id").ascending();
        // when
        List<Account> foundAccounts = accountRepository.findAll(example, sort);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).contains(account2, account);
        assertThat(foundAccounts.get(0)).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account2);
        assertThat(foundAccounts.get(1)).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account);
    }

    @Test
    public void shouldFindAll() {
        // given
        // when
        List foundAccounts = accountRepository.findAll();
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts).contains(account1, account2);

    }

    @Test
    public void shouldFindAllSorted() {
        // given
        Sort sort = Sort.by("id").ascending();
        // when
        List<Account> foundAccounts = accountRepository.findAll(sort);
        // then
        assertThat(foundAccounts).isNotEmpty();
        assertThat(foundAccounts).hasSize(2);
        assertThat(foundAccounts.get(0)).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account1);
        assertThat(foundAccounts.get(1)).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account2);
    }

    @Test
    public void shouldSaveAll() {
        // given
        Account account3 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        Account account4 = Account.builder()
                .accountNumber("33333333333")
                .checkDigit(20)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        List<Account> accounts = Arrays.asList(account3, account4);
        // when
        accountRepository.saveAll(accounts);
        // then
        assertThat(accounts).contains(account3, account4);
    }


    @Test
    public void shouldDeleteInBatch() {
        // given
        Account account3 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        accountRepository.save(account3);
        List<Account> accounts = Arrays.asList(account2, account1);
        // when
        accountRepository.deleteInBatch(accounts);
        // then
        assertThat(accountRepository.findAll()).hasSize(1);
        assertThat(accountRepository.findAll()).contains(account3);

    }

    @Test
    public void shouldDeleteAllInBatch() {
        // given
        Account account3 = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        accountRepository.save(account3);
        List<Account> accounts = Arrays.asList(account2, account1);
        // when
        accountRepository.deleteAllInBatch();
        // then
        assertThat(accountRepository.findAll()).isEmpty();
    }

    @Test
    public void shouldGetOne() {
        // given
        Account account = accountRepository.findAll().get(0);
        // when
        Account foundAccount = accountRepository.getOne(account.getId());
        // then
        assertThat(foundAccount).usingRecursiveComparison().ignoringFields("balance")
                .isEqualTo(account1);
    }
}
