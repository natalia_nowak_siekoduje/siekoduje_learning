package pl.siekoduje.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.siekoduje.model.Account;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository  extends JpaRepository<Account, Integer> {

    Account findByAccountNumber(String accountNumber);

    List<Account> findByCheckDigitAndCountryCode(Integer checkDigit, String countryCode);

    List<Account> findByDebitLimitIsNotNull();

    List<Account> findByCountryCodeIsNot(String countryCode);

    List<Account> findByDebitLimitIs(Double debitLimit);

    List<Account> findByCreationDateAfter(LocalDate date);

    void deleteByAccountNumberEndingWith(String ending);

}
