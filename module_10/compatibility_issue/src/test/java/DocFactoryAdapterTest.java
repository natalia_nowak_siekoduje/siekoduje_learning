import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DocFactoryAdapterTest {

    @Mock
    DocFactory docFactory;

    @InjectMocks
    DocFactoryAdapter docFactoryAdapter;

    @Test
    public void shouldConvertFile() {
        //given
        File inFile = mock(File.class);
        when(inFile.getAbsolutePath()).thenReturn("inPath");
        when(docFactory.fromPdfFile("inPath")).thenReturn("outPath");
        //when
        File outFile = docFactoryAdapter.convert(inFile);
        //tehn
        verify(docFactory).fromPdfFile("inPath");
        assertEquals(outFile.getName(), "outPath");
    }

    @Test
    public void docFactoryShouldThrowException() {
        //given
        File inFile = mock(File.class);
        when(inFile.getAbsolutePath()).thenReturn("bad string");
        when(docFactory.fromPdfFile("bad string")).thenThrow(new NullPointerException());
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            docFactoryAdapter.convert(inFile);
                        }
                );
        //then
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }

}