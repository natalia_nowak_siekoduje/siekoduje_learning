package superconverter;

import java.io.File;

public interface Converter {
    File convert(File pdfFile);
}