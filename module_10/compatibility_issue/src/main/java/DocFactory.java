interface DocFactory {
    String fromPdfFile(String pdfFile);
}
