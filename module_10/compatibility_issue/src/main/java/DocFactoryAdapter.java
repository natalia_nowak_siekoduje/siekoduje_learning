import superconverter.Converter;

import java.io.File;

public class DocFactoryAdapter implements Converter {
    private final DocFactory adaptee;

    public DocFactoryAdapter(DocFactory adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public File convert(File pdfFile) {

        return new File(adaptee.fromPdfFile(pdfFile.getAbsolutePath()));
    }
}
