public interface NotificationListener {
    public void onNotification(Notification notification);
}
