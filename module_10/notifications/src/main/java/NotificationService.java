import java.util.ArrayList;
import java.util.HashMap;

public class NotificationService {
    private final HashMap<NotificationType, NotificationListener> subscribers = new HashMap<>();

    public void receiveNotification(Notification notification) {
        subscribers.get(notification.getType()).onNotification(notification);
    }

    public void addListener(NotificationType type, NotificationListener notificationListener) {
        subscribers.put(type, notificationListener);
    }

    public void removeListener(NotificationType type, NotificationListener notificationListener) {
        subscribers.remove(type, notificationListener);
    }
}
