public enum NotificationType {
    EMAIL,
    SMS,
    SLACK
}
