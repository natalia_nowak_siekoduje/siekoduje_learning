import java.util.Date;
import java.util.Set;

public class Order {
    private Date date;
    private String status;
    private Set<Item> items;

    public Order(Date date, Set<Item> items) {
        this.date = date;
        this.items = items;
    }

    public Double calculateTotalCost() {
        return null;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }
}
