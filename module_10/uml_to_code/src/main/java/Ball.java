public class Ball  implements Item {
    private String type;

    public Ball(String type) {
        this.type = type;
    }
    public void openAirCup() {

    }

    @Override
    public Double getCost() {
        return null;
    }

    @Override
    public Double getWeight() {
        return null;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
