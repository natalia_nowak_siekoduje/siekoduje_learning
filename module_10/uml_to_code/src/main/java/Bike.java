public class Bike  implements Item {
    private String color;

    public Bike(String color) {
        this.color = color;
    }
    public void changeGear() {

    }

    public void turnWheel() {

    }

    @Override
    public Double getCost() {
        return null;
    }

    @Override
    public Double getWeight() {
        return null;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
