public interface Item {
    public Double getCost();
    public Double getWeight();
}
