import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class TaskBuilderTest {

    @Mock
    private List<Command> commands;

    @Test
    public void shouldCreateTask() {
        //given
        long id = 1111;
        String description = "qwerty";
        boolean done = true;
        LocalDate creationDate = LocalDate.of(1990, 11, 11);
        LocalDate expirydate = LocalDate.of(2020, 5, 20);
        //when
        TaskBuilder builder = new TaskBuilder();
        builder.setId(id);
        builder.setCommands(commands);
        builder.setCreationDate(creationDate);
        builder.setExpiryDate(expirydate);
        builder.setDescription(description);
        builder.setDone(done);
        Task givenTask = new Task(id, commands, description, done, creationDate, expirydate);
        Task result = builder.build();
        //then
        assertEquals(givenTask, result);
    }
}