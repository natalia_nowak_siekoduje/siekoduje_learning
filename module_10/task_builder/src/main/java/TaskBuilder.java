import java.time.LocalDate;
import java.util.List;

public class TaskBuilder {
    private  long id;
    private List<Command> commands;
    private String description;
    private boolean done;
    private LocalDate creationDate;
    private LocalDate expiryDate;

    public void setId(long id) {
        this.id = id;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Task build() {
        return new Task(id, commands, description, done, creationDate, expiryDate);
    }

    public long getId() {
        return id;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public String getDescription() {
        return description;
    }

    public boolean isDone() {
        return done;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }
}
