import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class Task {
        private final long id;
        private final List<Command> commands;
        private String description;
        private boolean done;
        private LocalDate creationDate;
        private LocalDate expiryDate;

    public Task(long id, List<Command> commands, String description, boolean done, LocalDate creationDate, LocalDate expiryDate) {
        this.id = id;
        this.commands = commands;
        this.description = description;
        this.done = done;
        this.creationDate = creationDate;
        this.expiryDate = expiryDate;
    }

    public long getId() {
        return id;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        Task task = (Task) o;
        return getId() == task.getId() &&
                isDone() == task.isDone() &&
                getCommands().equals(task.getCommands()) &&
                Objects.equals(getDescription(), task.getDescription()) &&
                getCreationDate().equals(task.getCreationDate()) &&
                Objects.equals(getExpiryDate(), task.getExpiryDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCommands(), getDescription(), isDone(), getCreationDate(), getExpiryDate());
    }

    //
//    @Override
//    public int hashCode() {
//        return super.hashCode();
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if(this == obj)
//            return true;
//        if(obj == null)
//            return false;
//        if(this.getClass() != obj.getClass())
//            return false;
//        Task other = (Task) obj;
//        return id == other.getId() &&
//                description.equals(other.getDescription()) &&
//                done == other.isDone() &&
//                creationDate.equals(other.getCreationDate()) &&
//                expiryDate.equals(other.getExpiryDate());
//        powinnam tutaj jeszcze chyba sprawdzać command, ale w sumie nie jstem pewna,
//        czy jak dam metodę equals, to mi sprawdzi, czy wszysctkie elementy z listy są równe
//        czy musze stworzyć swoją metodę
//    }
}
