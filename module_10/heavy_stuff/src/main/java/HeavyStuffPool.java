import java.net.ConnectException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.stream.IntStream;

public class HeavyStuffPool {
    private static HeavyStuffPool instance;
    private final Deque<HeavyStuff> pool = new ArrayDeque<>();
    private int poolCapacity;


    private HeavyStuffPool() {
        resize();
    }

    public static HeavyStuffPool getInstance() {
        if( instance == null)
            instance = new HeavyStuffPool();
        return instance;
    }

    public HeavyStuff acquire() {
        if(pool.isEmpty())
            resize();
        return pool.pollFirst(); 
    }

    public void release(HeavyStuff heavyStuff) {
        pool.push(heavyStuff);
    }

    private void resize() {
        IntStream.range(poolCapacity, poolCapacity + 5)
                .mapToObj(HeavyStuffFactory::createHeavyStuff)
                .peek(heavyStuff -> {
                    try {
                        heavyStuff.initialize();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .forEach(pool::addLast);

        poolCapacity += 5;
    }

    public int getPoolCapacity() {
        return poolCapacity;
    }
}
