public class HeavyStuffFactory {
    private static HeavyStuffFactory factory;
    private static final HeavyStuffPool pool = HeavyStuffPool.getInstance();
    public static HeavyStuffFactory getInstance() {
    if(factory == null)
        factory = new HeavyStuffFactory();
    return factory;
    }

    public static HeavyStuff createHeavyStuff(long id) {
        return new HeavyStuff( id);

    }
}