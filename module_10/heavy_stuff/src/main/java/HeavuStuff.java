import java.util.Objects;

class HeavyStuff {
    private long id;

    public HeavyStuff(long id) {
        this.id = id;
    }

    void initialize() throws Exception {
        Thread.sleep(1000);
    }

    public String process() {
        return "Processing finished by [" + toString() + "]";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HeavyStuff)) return false;
        HeavyStuff that = (HeavyStuff) o;
        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
