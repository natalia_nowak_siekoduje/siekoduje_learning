public class Application {
    public String processClientCall() {
        HeavyStuff heavyStuff = HeavyStuffPool.getInstance().acquire();
        return heavyStuff.process();
    }
}
