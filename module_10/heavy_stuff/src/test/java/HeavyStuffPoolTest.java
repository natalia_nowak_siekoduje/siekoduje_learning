import org.junit.Test;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;


public class HeavyStuffPoolTest {
    private final HeavyStuffPool pool = HeavyStuffPool.getInstance();
    private final HashSet<HeavyStuff> stuffSet = new HashSet<>();

    @Test
    public void shouldReturnHeavyStuff() {
        //given
        //when
        HeavyStuff result = pool.acquire();
        //then
        assertThat(result).isNotNull();
    }

    @Test
    public void shouldAcquireDifferentObjects() {
        //given
        int poolCapacity = 5;
        //when
        for (int i = 0; i < poolCapacity; i++) {
            stuffSet.add(pool.acquire());
        }
        //then
        assertThat(stuffSet.size()).isEqualTo(5);
    }

    @Test
    public void shouldResizePoolCapacity() {
        //given
        int poolCapacity = 5;
        //when
        for (int i = 0; i < poolCapacity + 1; i++) {
            stuffSet.add(pool.acquire());
        }
        //then
        assertThat(stuffSet.size()).isEqualTo(6);
    }


    @Test
    public void shouldReleaseHeavyStuff() {
        //given
        HeavyStuff expected = pool.acquire();
        //when
        pool.release(expected);
        HeavyStuff actual = pool.acquire();
        //then
        assertThat(expected).isEqualTo(actual);
    }
}