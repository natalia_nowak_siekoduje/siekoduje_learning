import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PDFDocumentProcessingServiceTest {
    private PDFDocumentProcessingService service = new PDFDocumentProcessingService();

    @Test
    public void shouldSetStrategy() {
    }

    @Test
    public void shouldSaveData() {
        //given
        PDFStrategy pdfStrategy = mock(PDFStrategy.class);
        service.setStrategy(pdfStrategy);
        //when
        service.saveData("data");
        //tehn
        verify(pdfStrategy).saveData("data");
    }
}