public interface SavingStrategy {
     public void saveData(String data);
}
