class PDFDocumentProcessingService {
    private SavingStrategy strategy;

    public void setStrategy(SavingStrategy strategy) {
        this.strategy = strategy;
    }

    public void saveData(String data) {
        strategy.saveData(data);
    }
}
