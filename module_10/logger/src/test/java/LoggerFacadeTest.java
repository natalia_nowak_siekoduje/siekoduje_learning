import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoggerFacadeTest {


    @Mock
    FileLogger fileLogger;
    @Mock
    StdOutLogger stdOutLogger;


    @Test
    public void shouldCallInfoMethods() {
        //given
        LoggerFacade facade = new LoggerFacade(fileLogger, stdOutLogger);
        //when
        String[] tab = {"1", "2"};
        facade.info("message", tab);
        //then
        verify(fileLogger).info("message", tab);
        verify(stdOutLogger).logInfo("message", tab);
    }

    @Test
    public void shouldCallInfoMethodsWithNullParams() {
        //given
        LoggerFacade facade = new LoggerFacade(fileLogger, stdOutLogger);
        //when
        facade.info("message", null);
        //then
        verify(fileLogger).info("message");
        verify(stdOutLogger).logInfo("message", (String) null);
    }

    @Test
    public void shouldCallErrorMethods() {
        //given
        LoggerFacade facade = new LoggerFacade(fileLogger, stdOutLogger);
        //when
        Exception exc = new Exception("error");
        facade.error("message", exc);
        //then
        verify(fileLogger).error("message", exc);
        verify(stdOutLogger).logError("message", exc);
    }


    @Test
    public void shouldCallErrorMethodsWithNullException() {
        //given
        LoggerFacade facade = new LoggerFacade(fileLogger, stdOutLogger);
        //when
        facade.error("message", null);
        //then
        verify(fileLogger).error("message", null);
        verify(stdOutLogger).logError("message", null);
    }

    @Test
    public void shouldCallErrorMethodsWithNewException() {
        //given
        LoggerFacade facade = new LoggerFacade(fileLogger, stdOutLogger);
        Throwable throwable = new Throwable("message");
        //when
        facade.error("message", throwable);
        ArgumentCaptor<Exception> exceptionArgumentCaptorForFileLogger = ArgumentCaptor.forClass(Exception.class);
        ArgumentCaptor<Exception> exceptionArgumentCaptorForStdOutLogger = ArgumentCaptor.forClass(Exception.class);
        //then

        verify(fileLogger).error(eq("message"), exceptionArgumentCaptorForFileLogger.capture());
        verify(stdOutLogger).logError(eq("message"), exceptionArgumentCaptorForStdOutLogger.capture());
        assertEquals(throwable.getMessage(), exceptionArgumentCaptorForFileLogger.getValue().getCause().getMessage());
        assertEquals(throwable.getMessage(), exceptionArgumentCaptorForStdOutLogger.getValue().getCause().getMessage());

    }
}