public interface Logger {

    void info(String message, Object... parms);
    void error(String message, Throwable throwable);
}
