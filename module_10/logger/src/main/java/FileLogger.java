interface FileLogger {
    void info(String message);

    void info(String message, Object[] params);

    void error(String message, Exception exc);
}
