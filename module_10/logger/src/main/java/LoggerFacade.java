import java.util.Arrays;
import java.util.stream.IntStream;

public class LoggerFacade implements Logger{
    private FileLogger fileLogger;
    private StdOutLogger stdOutLogger;

    public LoggerFacade(FileLogger fileLogger, StdOutLogger stdOutLogger) {
        this.fileLogger = fileLogger;
        this.stdOutLogger = stdOutLogger;
    }

    @Override
    public void info(String message, Object... params) {
        if(params == null) {
            fileLogger.info(message);
            stdOutLogger.logInfo(message, (String) null);
        }
        else {
            fileLogger.info(message, params);
            String[] paramsString = Arrays.stream(params)
                    .map(Object::toString)
                    .toArray(String[]::new);
            stdOutLogger.logInfo(message, paramsString);
        }
    }

    @Override
    public void error(String message, Throwable throwable) {
        Exception exc;
        if (throwable == null) {
            exc = (Exception) null;
        } else if (throwable instanceof Exception) {
            exc = (Exception) throwable;
        } else {
            exc = new Exception(throwable);
        }
        fileLogger.error(message, exc);
        stdOutLogger.logError(message, exc);
    }
}
