interface StdOutLogger {
    void logInfo(String message, String... params);
    void logError(String message, Exception exc);
}