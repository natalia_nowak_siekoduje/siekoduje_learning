public class Request {
    private final long userID;

    public Request(long userID) {
        this.userID = userID;
    }

    public long getUserID() {
        return userID;
    }
}
