import java.util.Optional;

abstract class  Validator {

    public void checkIfParameterIsNull(Object parameter, String name) {
        Optional.ofNullable(parameter).orElseThrow(() ->
                new IllegalArgumentException("Parameter " + name + " cannot be null") );
    }

    public void checkIfParameterIsBlank(String parameter, String name) {
        if (parameter.isBlank()) {
            throw new IllegalArgumentException("Parameter " + name + " cannot be blank");
        }
    }

    public void checkIfPositiveNumber(long number) {
        if (number < 0) {
            throw new IllegalArgumentException("Parameter 'id' cannot be negative");
        }
    }
}
