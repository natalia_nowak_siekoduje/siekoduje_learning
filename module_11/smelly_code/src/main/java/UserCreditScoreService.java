public class UserCreditScoreService {

    private final UserRepository userRepository;
    private final InternalScoreService internalScoreService;

    public UserCreditScoreService(UserRepository userRepository, InternalScoreService internalScoreService) {
        this.userRepository = userRepository;
        this.internalScoreService = internalScoreService;
    }


    public Response process(Request request) {
        try {
            User user = userRepository.find(request.getUserID());
            Score score = internalScoreService.getScore(user.getPesel());
                return mapScoreToResponse(score);
        } catch(Exception e) {
            return new Response(ResponseStatus.ERROR);
        }
    }

    private Response mapScoreToResponse(Score score) throws IllegalArgumentException {
        if(score == null )
            return new Response(ResponseStatus.ERROR);
//            throw new IllegalArgumentException("Parameter score cannot be null");
        if (score.getCalculatedScore() > 60) {
            return new Response(ResponseStatus.GRANTED);
        } else {
            return new Response(ResponseStatus.REJECTED);
        }
    }
}