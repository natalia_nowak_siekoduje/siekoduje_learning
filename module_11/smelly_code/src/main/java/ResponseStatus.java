public enum ResponseStatus {
    GRANTED(100) ,
    REJECTED(200),
    ERROR(200);
    private final int responseNumber;

    ResponseStatus(int responseNumber) {
        this.responseNumber = responseNumber;
    }

    public int getResponseNumber() {
        return responseNumber;
    }
}
