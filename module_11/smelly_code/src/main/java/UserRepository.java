import java.util.HashMap;
import java.util.Optional;

public class UserRepository {
    private HashMap<Long, User> users = new HashMap<>();

    public void add(Long userId, User user) {
        users.put(userId, user);
    }

    public User find(Long userId) throws  UserNotFoundException {
            return Optional.ofNullable(users.get(userId)).orElseThrow(() ->
                    new UserNotFoundException("User with id " + userId + " doesn't exist"));
    }

}
