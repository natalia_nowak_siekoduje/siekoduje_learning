import java.util.Optional;

public class LoanValidator extends Validator{
    public void validate(Loan loan) {
        checkIfParameterIsNull(loan, "loan");
        checkIfParameterIsNull(loan.getId(), "id");
        checkIfPositiveNumber(loan.getId());
    }


}
