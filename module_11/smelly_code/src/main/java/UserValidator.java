import java.util.Optional;

public class UserValidator extends Validator {
    public void checkIdUserIsConsistent(User user) {
        checkIfParameterIsNull(user, "user");
        checkIfParameterIsNull(user.getFirstName(), "first name");
        checkIfParameterIsBlank(user.getFirstName(), "first name");
        checkIfParameterIsNull(user.getLastName(), "last name");
        checkIfParameterIsBlank(user.getLastName(), "last name");
    }
}
