public class EmailService {
   private EmailSender emailSender;
   private EmailReceiver emailReceiver;

    public EmailService(EmailSender emailSender, EmailReceiver emailReceiver) {
        this.emailSender = emailSender;
        this.emailReceiver = emailReceiver;
    }

    public void receiveAndReply() {
       Email email =  emailReceiver.receiveEmail();
       email = replaceFromTo(email);
       email = changeContentAndSubject(email);
        emailSender.sendEmail(email);
    }

    private Email replaceFromTo(Email email) {
        String to = email.getFrom();
        String from = email.getTo();
        email.setFrom(from);
        email.setTo(to);
        return  email;
    }

    private Email changeContentAndSubject(Email email) {
        email.setSubject("odpowiedź");
        email.setContent("Dziękujemy. Odebraliśmy Twoją wiadomość. Wkrótce ktoś z naszego zespołu się z Tobą skontaktuje");
        return email;
    }


    // no i tutaj nie jestem pewna, czy dobrze obię ogólnie to zadanie, wiec nie wiem, czy dalej pisac, ale
//    zrobiłabym tak, ze wywołuję jakas tam metode, która wywołuje kolejno receiveEmail w emailReceiver i potem
//    sendEmail w emailSender
}
