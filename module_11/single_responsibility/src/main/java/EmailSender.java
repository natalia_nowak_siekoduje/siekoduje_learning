public interface EmailSender {
    void sendEmail(Email email);
}
