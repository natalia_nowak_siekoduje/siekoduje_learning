public interface EmailReceiver {
    Email receiveEmail();
}
