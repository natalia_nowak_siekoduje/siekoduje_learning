import org.junit.Test;
import org.junit.runner.Request;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoanServiceHappyTests {
    RequestValidator requestValidator = new RequestValidator();
    @Mock
    private UserRepository userRepository;
    @Mock
    private CreditScoreService creditScoreService;
    @Mock
    private AnalyticModuleService analyticModuleService;
    @InjectMocks
    LoanService loanService = new LoanService(creditScoreService, analyticModuleService,userRepository, requestValidator);
    @Test
    public void shouldReturnGrantedResponseForMortgage() throws UserNotFoundException {
        //given
        LoanRequest request = new LoanRequest("1234", LoanType.MORTGAGE);
        when(userRepository.findUserById(any(String.class))).thenReturn(new User("1234", "Andrzej", "Kowalski"));
        when(creditScoreService.getScore(any(String.class))).thenReturn(new Score(500));
        when(analyticModuleService.process(any(LoanRequest.class))).thenReturn(true);
        //when
        LoanResponse result = loanService.processLoanRequest(request);
        //then
        assertEquals(Status.GRANTED, result.getStatus());
    }

    @Test
    public void shouldReturnGrantedResponseForHomeLoan() throws UserNotFoundException {
        //given
        LoanRequest request = new LoanRequest("1234", LoanType.HOME_LOAN);
        when(userRepository.findUserById(any(String.class))).thenReturn(new User("1234", "Andrzej", "Kowalski"));
        when(creditScoreService.getScore(any(String.class))).thenReturn(new Score(500));
        when(analyticModuleService.process(any(LoanRequest.class))).thenReturn(true);
        //when
        LoanResponse result = loanService.processLoanRequest(request);
        //then
        assertEquals(Status.GRANTED, result.getStatus());
    }

    @Test
    public void shouldReturnGrantedResponseForShortTermLoan() throws UserNotFoundException {
        //given
        LoanRequest request = new LoanRequest("1234", LoanType.SHORT_TERM_LOAN);
        when(userRepository.findUserById(any(String.class))).thenReturn(new User("1234", "Andrzej", "Kowalski"));
        when(creditScoreService.getScore(any(String.class))).thenReturn(new Score(500));
        when(analyticModuleService.process(any(LoanRequest.class))).thenReturn(true);
        //when
        LoanResponse result = loanService.processLoanRequest(request);
        //then
        assertEquals(Status.GRANTED, result.getStatus());
    }

    @Test
    public void shouldReturnGrantedResponseForLongTermLoan() throws UserNotFoundException {
        //given
        LoanRequest request = new LoanRequest("1234", LoanType.LONG_TERM_LOAN);
        when(userRepository.findUserById(any(String.class))).thenReturn(new User("1234", "Andrzej", "Kowalski"));
        when(creditScoreService.getScore(any(String.class))).thenReturn(new Score(500));
        when(analyticModuleService.process(any(LoanRequest.class))).thenReturn(true);
        //when
        LoanResponse result = loanService.processLoanRequest(request);
        //then
        assertEquals(Status.GRANTED, result.getStatus());
    }

}
/*
1. testy dla czterech rodzajów LoanRequest
    1.1 gdy score >= 80
    1.3 score < 80
2. request == null
3. user id ==null
4. user == null
5. kiedy metoda isUserInternalCustomer zwarca false
 */