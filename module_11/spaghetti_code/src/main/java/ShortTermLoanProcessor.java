public class ShortTermLoanProcessor extends LoanTypeProcessor {
    public ShortTermLoanProcessor(UserRepository userRepository, CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService) {
        super(userRepository, creditScoreService, analyticModuleService);
    }

    @Override
    protected LoanResponse doProcess(User user, LoanRequest request) {
        Score score = getScore(user);
        if (score.getCalculatedScore() > 80)
            return new LoanResponse(Status.GRANTED);
        if(score.getCalculatedScore() > 0)
            try {
                if (analyzeRequest(request)) {
                    return new LoanResponse(Status.GRANTED);
                }
            } catch (Exception e) {
                System.err.println("Error " + e.getLocalizedMessage());
            }
        return new LoanResponse(Status.REJECTED);
    }
}
