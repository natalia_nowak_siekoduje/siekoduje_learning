public class Score {
    private final int calculatedScore;

    public Score(int calculatedScore) {
        this.calculatedScore = calculatedScore;
    }

    public int getCalculatedScore() {
        return calculatedScore;
    }
}
