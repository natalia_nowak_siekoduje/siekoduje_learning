public interface AnalyticModuleService {
    public boolean process(LoanRequest request);
}
