public class RequestValidator extends Validator {
    public void validateRequest(LoanRequest request) throws Exception {
        checkIfObjectIsNUll(request, new NullPointerException("Request cannot be null") );
        checkIfObjectIsNUll(request.getType(), new IllegalArgumentException("Missing type"));
        checkIfObjectIsNUll(request.getUserId(), new IllegalStateException("Unknown user id: " + request.getUserId()));
    }
}
