public abstract class LoanTypeProcessor {
    protected UserRepository userRepository;
    protected CreditScoreService creditScoreService;
    protected AnalyticModuleService analyticModuleService;
    protected LoanRequest request;
//    protected User user;
//    protected Score score;




    public LoanTypeProcessor(UserRepository userRepository, CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService) {
        this.userRepository = userRepository;
        this.creditScoreService = creditScoreService;
        this.analyticModuleService = analyticModuleService;
    }


    public final LoanResponse processRequest(LoanRequest request) {
        this.request = request;
         User user = userRepository.findUserById(request.getUserId());
        return doProcess(user, request);
    }

    protected abstract LoanResponse doProcess(User user, LoanRequest request);

    protected Score getScore(User user) {
        return creditScoreService.getScore(user.getUserId());
    }

    protected boolean analyzeRequest(LoanRequest request) {
        return analyticModuleService.process(request);
    }

//    protected LoanResponse getDecisionFromAnalyticModuleService() {
//        try {
//            if (analyticModuleService.process(request)) {
//                return new LoanResponse(Status.GRANTED);
//            }
//        } catch (Exception e) {
//            System.err.println("Error " + e.getLocalizedMessage());
//        }
//        return new LoanResponse(Status.REJECTED);
//    }

}
