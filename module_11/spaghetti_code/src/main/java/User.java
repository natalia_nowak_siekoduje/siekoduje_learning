public class User {
    private final String userId;

    private final String firstName;

    private final String lastName;


    public User(String id, String firstName, String lastName) {

        this.userId = id;

        this.firstName = firstName;

        this.lastName = lastName;

    }


    public String getUserId() {

        return userId;

    }


    public String getFirstName() {

        return firstName;

    }


    public String getLastName() {

        return lastName;

    }

    public boolean isUserInternalCustomer() {
        return true;
    }
}
