public class MortgageLoanProcessor extends LoanTypeProcessor {

    public MortgageLoanProcessor(UserRepository repository, CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService) {
        super(repository, creditScoreService, analyticModuleService);
    }

    @Override
    protected LoanResponse doProcess(User user, LoanRequest request) {
        Score score = creditScoreService.getScore(user.getUserId());
        if(score.getCalculatedScore() >= 80) {
            if (analyzeRequest(request)) {
                return new LoanResponse(Status.GRANTED);
            }
        }
        return new LoanResponse(Status.REJECTED);
    }
}
