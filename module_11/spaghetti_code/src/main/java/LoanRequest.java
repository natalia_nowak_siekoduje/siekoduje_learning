public class LoanRequest {
    private final String userId;
    private final LoanType loanType;


    public LoanRequest(String pesel, LoanType loanType) {
        this.userId = pesel;
        this.loanType = loanType;
    }


    public String getUserId() {
        return userId;
    }

    public LoanType getType() {
        return loanType;
    }
}
