public class LongTermRequestProcessor extends LoanTypeProcessor {
    public LongTermRequestProcessor(UserRepository userRepository, CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService) {
        super(userRepository, creditScoreService, analyticModuleService);
    }

    @Override
    protected LoanResponse doProcess(User user, LoanRequest request) {
        try {
            if (analyzeRequest(request)) {
                return new LoanResponse(Status.GRANTED);
            }
        } catch (Exception e) {
            System.err.println("Error " + e.getLocalizedMessage());
        }
        return new LoanResponse(Status.REJECTED);
    }
}
