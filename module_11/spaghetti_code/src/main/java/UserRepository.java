import java.util.HashMap;
import java.util.Optional;

public interface UserRepository {
    HashMap<String, User> users = new HashMap<>();

    public void add(String userId, User user);

    User findUserById(String userId);
}
