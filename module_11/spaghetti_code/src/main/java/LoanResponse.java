public class LoanResponse {
    private final Status status;


    public LoanResponse(Status status) {

        this.status = status;

    }


    public Status getStatus() {

        return status;

    }
}
