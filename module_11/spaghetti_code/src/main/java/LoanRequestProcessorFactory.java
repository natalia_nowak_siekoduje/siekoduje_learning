public class LoanRequestProcessorFactory {
    private UserRepository userRepository;
    private CreditScoreService creditScoreService;
    private AnalyticModuleService analyticModuleService;

    public LoanRequestProcessorFactory(UserRepository userRepository, CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService) {
        this.userRepository = userRepository;
        this.creditScoreService = creditScoreService;
        this.analyticModuleService = analyticModuleService;
    }

    public LoanTypeProcessor createLoanTypeProcessor(LoanType type) {
        switch (type) {
            case MORTGAGE:
                return new MortgageLoanProcessor(userRepository, creditScoreService, analyticModuleService);
            case HOME_LOAN:
                return new HomeLoanProcessor(userRepository, creditScoreService, analyticModuleService);
            case SHORT_TERM_LOAN:
                return new ShortTermLoanProcessor(userRepository, creditScoreService, analyticModuleService);
            case LONG_TERM_LOAN:
                return new LongTermRequestProcessor(userRepository, creditScoreService, analyticModuleService);
            default:
                throw new IllegalArgumentException("Unsupported type: " + type);
        }
    }
}
