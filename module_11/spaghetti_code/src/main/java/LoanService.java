public class LoanService {
    private UserRepository userRepository;
    private RequestValidator requestValidator;
    private LoanTypeProcessor requestProcessor;
    private CreditScoreService creditScoreService;
    private AnalyticModuleService analyticModuleService;
    private final LoanRequestProcessorFactory loanRequestProcessorFactory;

    public LoanService(CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService, UserRepository userRepository, RequestValidator requestValidator) {
        this.userRepository = userRepository;
        this.requestValidator = requestValidator;
        this.creditScoreService = creditScoreService;
        this.analyticModuleService = analyticModuleService;
        loanRequestProcessorFactory = new LoanRequestProcessorFactory(userRepository, creditScoreService, analyticModuleService);
    }

    public LoanResponse processLoanRequest(LoanRequest request) {
        try {
            requestValidator.validateRequest(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
       requestProcessor =  loanRequestProcessorFactory.createLoanTypeProcessor(request.getType());
        return requestProcessor.processRequest(request);
    }
}

//        if (request.getType() == null) {
//            throw new IllegalArgumentException("Missing type");
//        } else {
//            try {
//                if (request.getType() == LoanType.MORTGAGE) {
//                    if (request.getUserId() == null) {
//                        throw new IllegalArgumentException("Missing user id");
//                    } else {
//                        User user = userRepository.findUserById(request.getUserId());
//                        if (user == null) {
//                            throw new IllegalStateException("Unknown user id: " + request.getUserId());
//                        } else {
//                            Score score = creditScoreService.getScore(user.getUserId());
//                            if (score.getCalculatedScore() >= 80) {
//                                if (analyticModuleService.process(request)) {
//                                    return new LoanResponse(Status.GRANTED);
//                                }
//                            } else {
//                                return new LoanResponse(Status.REJECTED);
//                            }
//                        }
//                    }
//                } else if (request.getType() == LoanType.HOME_LOAN) {
//                    if (request.getUserId() == null) {
//                        throw new IllegalArgumentException("Missing user id");
//                    } else {
//                        User user = userRepository.findUserById(request.getUserId());
//                        if (user == null) {
//                            throw new IllegalStateException("Unknown user id: " + request.getUserId());
//                        } else {
//                            Score score = creditScoreService.getScore(user.getUserId());
//                            if (score.getCalculatedScore() > 0 && score.getCalculatedScore() <= 60) {
//                                if (user.isUserInternalCustomer()) {
//                                    try {
//                                        if (analyticModuleService.process(request)) {
//                                            return new LoanResponse(Status.GRANTED);
//                                        }
//                                    } catch (Exception e) {
//                                        System.err.println("Error " + e.getLocalizedMessage());
//                                    }
//                                } else {
//                                    return new LoanResponse(Status.REJECTED);
//                                }
//                            } else {
//                                try {
//                                    if (analyticModuleService.process(request)) {
//                                        return new LoanResponse(Status.GRANTED);
//                                    }
//                                } catch (Exception e) {
//                                    System.err.println("Error " + e.getLocalizedMessage());
//                                }
//                            }
//                        }
//                    }
//                } else if (request.getType() == LoanType.SHORT_TERM_LOAN) {
//                    if (request.getUserId() == null) {
//                        throw new IllegalArgumentException("Missing user id");
//                    } else {
//                        User user = userRepository.findUserById(request.getUserId());
//                        if (user == null) {
//                            throw new IllegalStateException("Unknown user id: " + request.getUserId());
//                        } else {
//                            Score score = creditScoreService.getScore(user.getUserId());
//                            if (score.getCalculatedScore() > 80) {
//                                return new LoanResponse(Status.GRANTED);
//                            } else if (score.getCalculatedScore() > 0) {
//                                try {
//                                    if (analyticModuleService.process(request)) {
//                                        return new LoanResponse(Status.GRANTED);
//                                    }
//                                } catch (Exception e) {
//                                    System.err.println("Error " + e.getLocalizedMessage());
//                                }
//                            } else {
//                                return new LoanResponse(Status.REJECTED);
//                            }
//                        }
//                    }
//                } else if (request.getType() == LoanType.LONG_TERM_LOAN) {
//                    if (request.getUserId() == null) {
//                        throw new IllegalArgumentException("Missing user id");
//                    } else {
//                        User user = userRepository.findUserById(request.getUserId());
//                        if (user == null) {
//                            throw new IllegalStateException("Unknown user id: " + request.getUserId());
//                        } else {
//                            try {
//                                try {
//                                    if (analyticModuleService.process(request)) {
//                                        return new LoanResponse(Status.GRANTED);
//                                    }
//                                } catch (Exception e) {
//                                    System.err.println("Error " + e.getLocalizedMessage());
//                                }
//                            } catch (Exception e) {
//                                System.err.println("Error " + e.getLocalizedMessage());
//                            }
//                        }
//                    }
//                } else {
//                    throw new IllegalArgumentException("Unsupported type: " + request.getType());
//                }
//            } catch (Exception e) {
//                System.err.println("Error " + e.getLocalizedMessage());
//            }
//        }
//        return new LoanResponse(Status.REJECTED);
//
//}