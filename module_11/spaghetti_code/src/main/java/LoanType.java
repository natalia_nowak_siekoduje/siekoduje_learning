public enum LoanType {
    MORTGAGE,
    HOME_LOAN,
    SHORT_TERM_LOAN,
    LONG_TERM_LOAN,
}
