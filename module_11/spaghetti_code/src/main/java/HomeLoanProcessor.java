public class HomeLoanProcessor extends LoanTypeProcessor {
    public HomeLoanProcessor(UserRepository userRepository, CreditScoreService creditScoreService, AnalyticModuleService analyticModuleService) {
        super(userRepository, creditScoreService, analyticModuleService);
    }

    @Override
    protected LoanResponse doProcess(User user, LoanRequest request) {
        Score score = getScore(user);
        if (score.getCalculatedScore() > 0 && score.getCalculatedScore() <= 60) {
            if (user.isUserInternalCustomer()) {
                try {
                    if (analyzeRequest(request)) {
                        return new LoanResponse(Status.GRANTED);
                    }
                } catch (Exception e) {
                    System.err.println("Error " + e.getLocalizedMessage());
                }
            }
        }
        return new LoanResponse(Status.REJECTED);
    }
}
