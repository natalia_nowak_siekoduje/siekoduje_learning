import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class DocumentProcessingServiceTest {
    @InjectMocks
    DocumentProcessingService documentProcessingService;
    @Mock
    private NotificationService notificationService;
    @Mock
    private SavingService savingService;

    @Test
    public void shouldProcessData() throws IOException {
        //given
        String data = "data";
        Notification notification = new Notification();
        notification.setType(NotificationType.EMAIL);
        notification.setDescription("description");
        //when
        documentProcessingService.processData(data, notification);
        //then
        verify(savingService).saveData(data);
        verify(notificationService).sendNotification(notification);

    }

    @Test
    public void shouldThrowException() throws IOException { 
        //given
        String data = "data";
        Notification notification = new Notification();
        notification.setType(NotificationType.EMAIL);
        notification.setDescription("description");
        Mockito.doThrow(new IOException()).when(savingService).saveData(data);
        Mockito.doNothing().when(notificationService).sendNotification(notification);
        //when
        documentProcessingService.processData(data, notification);
        //then
        ArgumentCaptor<Notification> notificationArgumentCaptor = ArgumentCaptor.forClass(Notification.class);
        verify(savingService).saveData(data);
        verify(notificationService).sendNotification(notificationArgumentCaptor.capture());
        Notification capturedNotification = notificationArgumentCaptor.getValue();
        assertThat(capturedNotification).isEqualTo(notification);

    }


}