import java.io.IOException;

public interface SavingService {

    void saveData(String data) throws IOException;
}
