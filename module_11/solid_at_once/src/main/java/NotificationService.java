public interface NotificationService {
    void sendNotification(Notification notification);
}
