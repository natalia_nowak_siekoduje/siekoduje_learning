import java.io.IOException;

public class DocumentProcessingService {
    private final NotificationService notificationService;
    private final SavingService savingService;

    protected DocumentProcessingService(NotificationService notificationService, SavingService savingService) {
        this.notificationService = notificationService;
        this.savingService = savingService;
    }

    public final void processData(String data, Notification notification){
        try {
            savingService.saveData(data);
            notificationService.sendNotification(notification);
        } catch (IOException e) {
            notificationService.sendNotification(notification);
        }
    };
}
