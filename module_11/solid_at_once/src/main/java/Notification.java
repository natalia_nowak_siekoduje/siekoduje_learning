import java.util.Objects;

public class Notification {
    private NotificationType type;

    private String description;

    public NotificationType getType() {
        return type;
    }


    public void setType(NotificationType type) {
        this.type = type;
    }


    public String getDescription() {

        return description;

    }


    public void setDescription(String description) {

        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Notification)) return false;
        Notification that = (Notification) o;
        return getType() == that.getType() &&
                getDescription().equals(that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getDescription());
    }
}
