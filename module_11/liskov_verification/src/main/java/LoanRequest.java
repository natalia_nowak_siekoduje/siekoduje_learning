public class LoanRequest {
    private final String pesel;

    public LoanRequest(String pesel) {
        this.pesel = pesel;
    }

    public String getPesel() {
        return pesel;
    }
}
