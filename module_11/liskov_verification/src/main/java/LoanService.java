public class LoanService {
    private final CreditScoreProvider creditScoreProvider;

    public LoanService(CreditScoreProvider creditScoreProvider) {
        this.creditScoreProvider = creditScoreProvider;
    }

    public LoanResponse processLoanRequest(LoanRequest loanRequest) {
        Score score = creditScoreProvider.getScore(loanRequest.getPesel());
        int calculatedScore = score.getCalculatedScore();
        // further processing
        return new LoanResponse(Status.GRANTED);
    }

    public static void main(String[] args) {
        LoanService loanService =
                new LoanService(new EnhancedCreditScoreService(new BIKScoreService()));
        loanService.processLoanRequest(new LoanRequest("90010746350"));
    }
}
