public class EnhancedCreditScoreService  implements CreditScoreProvider {
    private final BIKScoreService bikScoreService;
    public EnhancedCreditScoreService (BIKScoreService bikScoreService) {
        this.bikScoreService = bikScoreService;
    }
    @Override
    public EnhancedCreditScore getScore(String pesel) {
        BIKScore bikScore = bikScoreService.getScore(pesel);
        int cumulativeScore = bikScore.getCalculatedScore();
        // executing custom scoring logic
        if (isBankCustomer(pesel)) {
            cumulativeScore += 20;
        }
        return new EnhancedCreditScore(bikScore.getCalculatedScore(), cumulativeScore);
    }
    private boolean isBankCustomer(String pesel) {
        return true;
    }
}