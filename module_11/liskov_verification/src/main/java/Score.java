public interface Score {
    int getCalculatedScore();
}