public class EnhancedCreditScore implements Score{
    private final int originalBikScore;
    private final int cumulativeScore;

    public EnhancedCreditScore(int originalBikScore, int cumulativeScore) {
        this.originalBikScore = originalBikScore;
        this.cumulativeScore = cumulativeScore;
    }

    public int getOriginalBikScore() {
        return originalBikScore;
    }

    @Override
    public int getCalculatedScore() {
        return cumulativeScore;
    }
}