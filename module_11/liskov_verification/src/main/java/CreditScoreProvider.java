public interface CreditScoreProvider {
    Score getScore(String pesel);
}
