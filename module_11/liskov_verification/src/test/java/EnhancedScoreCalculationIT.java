import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class EnhancedScoreCalculationIT extends ScoreCalculationBase {

    @Test
    public void shouldCreateLoanServiceWithEnhancedCreditScoreService() {
        //given
        LoanService loanService = new LoanService(new EnhancedCreditScoreService(new BIKScoreService()));
        //when
        LoanResponse result = loanService.processLoanRequest(LOAN_REQUEST);
        //then
        assertThatResponseMatchesRequest(result);
    }
}