import org.junit.Test;

import static org.junit.Assert.*;

public class BasicScoreCalculationIT extends ScoreCalculationBase {
    @Test
    public void shouldCreateLoanService() {
        //given
        LoanService loanService = new LoanService(new BIKScoreService());
        //when
        LoanResponse result = loanService.processLoanRequest(LOAN_REQUEST);
        //then
        assertThatResponseMatchesRequest(result);
    }

}