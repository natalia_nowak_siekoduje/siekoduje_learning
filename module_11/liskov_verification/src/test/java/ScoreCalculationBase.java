import org.junit.Test;

import static org.junit.Assert.*;

public abstract class ScoreCalculationBase {
    public static final LoanRequest LOAN_REQUEST = new LoanRequest("3456789");

    public void assertThatResponseMatchesRequest(LoanResponse result) {
        assertEquals(Status.GRANTED, result.getStatus());
    }


}