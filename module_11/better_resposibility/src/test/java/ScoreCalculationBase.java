import static org.junit.Assert.*;

abstract class ScoreCalculationBase {
    public static final CreditCardRequest CARD_REQUEST = new CreditCardRequest("1234567890");

    public void assertThatResponseMatchesRequest(CreditCardResponse result) {
        assertEquals(Status.GRANTED, result.getStatus());
    }
}