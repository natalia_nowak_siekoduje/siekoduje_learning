import org.junit.Test;

import static org.junit.Assert.*;

public class EnhancedCreditScoreServiceTest extends ScoreCalculationBase {

        @Test
        public void shouldCalculateRequestWithEnhancedCreditScoreService() {
            //given
            CreditCardService creditCardService = new CreditCardService(new EnhancedCreditScoreService(new BIKScoreService(), new InternalBankScoreProvider()));
            //when
            CreditCardResponse result = creditCardService.processLoanRequest(CARD_REQUEST);
            //then
            assertThatResponseMatchesRequest(result);
        }

    }
