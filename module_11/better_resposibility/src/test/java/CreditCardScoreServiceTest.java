import org.junit.Test;

import static org.junit.Assert.*;

public class CreditCardScoreServiceTest extends ScoreCalculationBase {

    @Test
    public void shouldCalculateRequestWithCreditCardScoreService() {
        //given
        CreditCardService creditCardService = new CreditCardService(new CreditCardScoreService(new InternalBankScoreProvider()));
        //when
        CreditCardResponse result = creditCardService.processLoanRequest(CARD_REQUEST);
        //then
        assertThatResponseMatchesRequest(result);
    }

}