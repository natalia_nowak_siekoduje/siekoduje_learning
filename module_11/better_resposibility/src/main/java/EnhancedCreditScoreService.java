import score.EnhancedCreditScore;
import score.Score;

public class EnhancedCreditScoreService  implements CreditCardScoreProvider {

    private final BIKScoreService bikScoreService;
    private final CreditScoreProvider internalScoreProvider;

    public EnhancedCreditScoreService (BIKScoreService bikScoreService, CreditScoreProvider internalScoreProvider) {
        this.bikScoreService = bikScoreService;
        this.internalScoreProvider = internalScoreProvider;
    }

    @Override
    public Score getScore(String pesel) {
        Score bikScore = bikScoreService.getScore(pesel);

        int cumulativeScore = bikScore.getCalculatedScore();
        // executing custom scoring logic
        Score internalBankScore = internalScoreProvider.getScore(pesel);
        if(isBankCustomer(pesel))
            cumulativeScore = internalBankScore.getCalculatedScore() +bikScore.getCalculatedScore();
        return new EnhancedCreditScore(bikScore.getCalculatedScore(), cumulativeScore);
    }

    private boolean isBankCustomer(String pesel) {
        return true;
    }
}