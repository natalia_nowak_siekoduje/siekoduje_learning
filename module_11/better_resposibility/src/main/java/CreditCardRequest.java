public class CreditCardRequest {
    private final String pesel;

    public CreditCardRequest(String pesel) {
        this.pesel = pesel;
    }

    public String getPesel() {
        return pesel;
    }
}
