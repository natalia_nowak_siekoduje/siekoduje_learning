public class CreditCardResponse {
    private final Status status;

    public CreditCardResponse(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }
}
