import score.CreditCardScore;
import score.Score;

public class CreditCardScoreService implements CreditCardScoreProvider {
    private final CreditScoreProvider internalScoreProvider;

    public CreditCardScoreService(CreditScoreProvider internalScoreProvider) {
        this.internalScoreProvider = internalScoreProvider;
    }

    @Override
    public Score getScore(String pesel) {
        // logic calculating internal score
        int calculatedScore = internalScoreProvider.getScore(pesel).getCalculatedScore();

        return new CreditCardScore(calculatedScore);
    }
}
