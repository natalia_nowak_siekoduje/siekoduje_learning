import score.InternalBankScore;
import score.Score;

public class InternalBankScoreProvider implements CreditScoreProvider {

    @Override
    public Score getScore(String pesel) {
        return new InternalBankScore(20);
    }
}
