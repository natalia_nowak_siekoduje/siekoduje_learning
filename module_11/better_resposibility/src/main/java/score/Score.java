package score;

public interface Score {
    int getCalculatedScore();
}
