package score;

public class BIKScore implements Score {
    private final int calculatedScore;

    public BIKScore(int calculatedScore) {
        this.calculatedScore = calculatedScore;
    }

    @Override
    public int getCalculatedScore() {
        return calculatedScore;
    }
}