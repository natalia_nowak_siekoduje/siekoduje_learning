package score;

public class InternalBankScore implements Score{
    private final int calculatedScore;

    public InternalBankScore(int calculatedScore) {
        this.calculatedScore = calculatedScore;
    }

    @Override
    public int getCalculatedScore() {
        return calculatedScore;
    }
}
