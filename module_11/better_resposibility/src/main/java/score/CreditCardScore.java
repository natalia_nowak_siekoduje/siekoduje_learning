package score;

import score.Score;

public class CreditCardScore implements Score {
    private final int calculatedScore;

    public CreditCardScore(int calculatedScore) {
        this.calculatedScore = calculatedScore;
    }

    @Override
    public int getCalculatedScore() {
        return calculatedScore;
    }
}

