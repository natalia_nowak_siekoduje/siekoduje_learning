import score.Score;

public interface CreditCardScoreProvider {
    Score getScore(String pesel);
}
