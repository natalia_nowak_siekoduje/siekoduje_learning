import score.BIKScore;

public class BIKScoreService implements CreditCardScoreProvider {
    @Override
    public BIKScore getScore(String pesel) {
        // calculate result based on BIK data
        return new BIKScore(100);
    }
}