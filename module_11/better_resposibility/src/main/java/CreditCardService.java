import score.Score;

class CreditCardService {
    private final CreditCardScoreProvider creditCardScoreProvider;

    public CreditCardService(CreditCardScoreProvider creditCardScoreProvider) {
        this.creditCardScoreProvider = creditCardScoreProvider;
    }

    public CreditCardResponse processLoanRequest(CreditCardRequest creditCardRequest) {
        Score score = creditCardScoreProvider.getScore(creditCardRequest.getPesel());
        int calculatedScore = score.getCalculatedScore();
        // further processing
        return new CreditCardResponse(Status.GRANTED);
    }

    public static void main(String[] args) {
        CreditCardService loanService = new CreditCardService(new CreditCardScoreService(new InternalBankScoreProvider()));
        loanService.processLoanRequest(new CreditCardRequest("90010746350"));
    }
}