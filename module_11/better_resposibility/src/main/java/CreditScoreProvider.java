import score.Score;

public interface CreditScoreProvider {
    Score getScore(String pesel);
}