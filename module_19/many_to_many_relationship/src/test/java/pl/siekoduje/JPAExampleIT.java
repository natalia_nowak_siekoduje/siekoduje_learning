package pl.siekoduje;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Product;
import pl.siekoduje.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class JPAExampleIT {
    private EntityManagerFactory emf;
    private EntityManager em;

    @BeforeEach
    public void beforeEach() {
        emf = Persistence.createEntityManagerFactory("jpaTestExample");
        em = emf.createEntityManager();
    }

    @AfterEach
    public void afterEach() {
        em.close();
        emf.close();
    }

    @Test
    public void testCustomerAndAccountOneToManyBidirectionalRelationship() {
        em.getTransaction().begin();
        Product product1 = Product.builder()
                .referenceName("name1")
                .referenceId("111111111")
                .build();

        Product product2 = Product.builder()
                .referenceName("name2")
                .referenceId("2222222")
                .build();
        Product product3 = Product.builder()
                .referenceName("name3")
                .referenceId("333333")
                .build();
        Customer customer1 = Customer.builder()
                .firstName("Kasia")
                .lastName("Kowalska")
                .build();
        Customer customer2 = Customer.builder()
                .firstName("Kasia")
                .lastName("Kowalska")
                .build();

        customer1.addProduct(product1);
        customer1.addProduct(product2);
        customer2.addProduct(product1);
        customer2.addProduct(product3);
        em.persist(product1);
        em.persist(product2);
        em.persist(product3);
        em.persist(customer1);
        em.persist(customer2);
        em.getTransaction().commit();
        assertThat(em.find(Customer.class, 1).getProducts()).contains(product1, product2);
        assertThat(em.find(Product.class, 1).getCustomers()).contains(customer1);
        assertThat(em.find(Product.class, 1).getCustomers()).contains(customer1, customer2);
        assertThat(em.find(Product.class, 3).getCustomers()).contains(customer2);
    }

}