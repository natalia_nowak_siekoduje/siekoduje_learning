package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode(of = "id")
@Entity(name = "TransferDetails")
@Table(name = "transfers_details")
public class TransferDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "recipient_account")
    private String recipientAccount;

    @Column(name = "reference_code")
    private String referenceCode;

    @OneToOne
    @JoinColumn(name = "transfer_id")
    private Transfer transfer;

    public void setTransfer(Transfer transfer) {
        transfer.setTransferDetails(this);
        this.transfer = transfer;
    }
}
