package pl.siekoduje;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Transfer;
import pl.siekoduje.model.TransferDetails;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class JPAExampleIT {
    private EntityManagerFactory emf;
    private EntityManager em;

    @BeforeEach
    public void beforeEach() {
        emf = Persistence.createEntityManagerFactory("jpaTestExample");
        em = emf.createEntityManager();
    }

    @AfterEach
    public void afterEach() {
        em.close();
        emf.close();
    }

    @Test
    public void testTransferAndTransferDetailsOneToOneBidirectionalRelationship() {
        em.getTransaction().begin();
        Transfer transfer = Transfer.builder()
                .ammount(BigDecimal.valueOf(1000))
                .currency("PLN")
                .build();
        TransferDetails transferDetails = TransferDetails.builder()
                .recipientAccount("11111111111")
                .referenceCode("000")
                .build();
        transferDetails.setTransfer(transfer);
        em.persist(transfer);
        em.persist(transferDetails);
        em.getTransaction().commit();
        assertThat(em.find(TransferDetails.class, 1).getTransfer()).isEqualTo(transfer);
        assertThat(em.find(Transfer.class, 1).getTransferDetails()).isEqualTo(transferDetails);
    }

}