package repository;

import lombok.extern.slf4j.Slf4j;
import pl.siekoduje.model.Account;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Slf4j
public class JpaAccountRepository implements AccountRepository {
    private final EntityManager entityManager;

    public JpaAccountRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    @Override
    public Optional<Account> findById(Integer id) {
        Account account =  entityManager.find(Account.class, id);
        return Optional.ofNullable(account);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Account> findAll() {
        Query query = entityManager.createQuery("SELECT a FROM Account a");
        return query.getResultList();
    }

    @Override
    public Optional<Account> save(Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.persist(account);
            transaction.commit();
            return Optional.of(account);
        } catch (Exception e) {
            log.warn("Unable to save entity", e);
            transaction.rollback();
            return Optional.empty();
        }
    }

    @Override
    public void delete(Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.remove(account);
            transaction.commit();
        } catch (Exception e) {
            log.warn("Unable to delete entity", e);
            transaction.rollback();
        }
    }
}
