package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Account;
import repository.JpaAccountRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class JpaAccountRepositoryTest {
    private final Account account1 = Account.builder()
            .accountNumber("111111111111113")
            .checkDigit(30)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
            .creationDate(Date.valueOf("2012-12-12"))
            .creationTime(Time.valueOf(LocalTime.of(10, 12)))
            .balance(BigDecimal.valueOf(20L))
            .debitLimit(500.0)
            .build();
    private final Account account2 = Account.builder()
            .accountNumber("111111111111118")
            .checkDigit(00)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
            .creationDate(Date.valueOf("2012-12-12"))
            .creationTime(Time.valueOf(LocalTime.of(12, 12)))
            .balance(BigDecimal.valueOf(20000L))
            .debitLimit(5000.0)
            .build();

    private EntityManagerFactory emf;
    private EntityManager em;
    private JpaAccountRepository accountRepository;

    @BeforeEach
    public void beforeEach() {
        emf = Persistence.createEntityManagerFactory("jpaTestExample");
        em = emf.createEntityManager();
        accountRepository = new JpaAccountRepository(em);
        prepareInitialData();
    }

    private void prepareInitialData() {
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            em.persist(account1);
            em.persist(account2);
            em.flush();
            transaction.commit();
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    @AfterEach
    public void afterEach() {
        em.close();
        emf.close();
    }

    @Test
    public void shouldSave() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(Date.valueOf("2012-12-12"))
                .creationTime(Time.valueOf(LocalTime.of(12, 12)))
                .balance(BigDecimal.valueOf(20000L))
                .debitLimit(5000.0)
                .build();
        //when
        accountRepository.save(account);
        //then
        assertThat(accountRepository.findAll())
                .hasSize(3);
        Account result = em.find(Account.class, 3);
        assertThat(result.getAccountNumber()).isEqualTo(account.getAccountNumber());
        assertThat(result.getCheckDigit()).isEqualTo(account.getCheckDigit());
        assertThat(result.getCountryCode()).isEqualTo(account.getCountryCode());
        assertThat(result.getBalance()).isEqualTo(account.getBalance());
        assertThat(result.getDebitLimit()).isEqualTo(account.getDebitLimit());
        assertThat(result.getCreationTime()).isEqualTo(account.getCreationTime());
        assertThat(result.getCreationDate()).isEqualTo(account.getCreationDate());
        assertThat(result.getUpdateTimestamp()).isEqualTo(account.getUpdateTimestamp());
    }


    @Test
    public void shouldUpdate() {
        // given
        Account account = em.find(Account.class, 1);
        // when
        em.getTransaction().begin();
        account.setUpdateTimestamp(Timestamp.valueOf("2021-03-11 11:20:20"));
        em.getTransaction().commit();
        // then
        Account result = em.find(Account.class, 1);
        assertThat(result.getAccountNumber()).isEqualTo(account.getAccountNumber());
        assertThat(result.getCheckDigit()).isEqualTo(account.getCheckDigit());
        assertThat(result.getCountryCode()).isEqualTo(account.getCountryCode());
        assertThat(result.getBalance()).isEqualTo(account.getBalance());
        assertThat(result.getDebitLimit()).isEqualTo(account.getDebitLimit());
        assertThat(result.getCreationTime()).isEqualTo(account.getCreationTime());
        assertThat(result.getCreationDate()).isEqualTo(account.getCreationDate());
        assertThat(result.getUpdateTimestamp()).isEqualTo(account.getUpdateTimestamp());
    }

    @Test
    public void shouldDelete() {
        // given
        Account account = em.find(Account.class, 1);
        // when
        accountRepository.delete(account);
        // then
        assertThat(em.find(Account.class, 1)).isNull();
    }

    @Test
    public void shouldFindAll() {
        // given
        // when
        List<Account> allAccounts = accountRepository.findAll();
        // then
        assertThat(allAccounts).hasSize(2);
        Account result1 = em.find(Account.class, 1);
        Account result2 = em.find(Account.class, 2);
        assertThat(result1.getAccountNumber()).isEqualTo(account1.getAccountNumber());
        assertThat(result1.getCheckDigit()).isEqualTo(account1.getCheckDigit());
        assertThat(result1.getCountryCode()).isEqualTo(account1.getCountryCode());
        assertThat(result1.getBalance()).isEqualTo(account1.getBalance());
        assertThat(result1.getDebitLimit()).isEqualTo(account1.getDebitLimit());
        assertThat(result1.getCreationTime()).isEqualTo(account1.getCreationTime());
        assertThat(result1.getCreationDate()).isEqualTo(account1.getCreationDate());
        assertThat(result1.getUpdateTimestamp()).isEqualTo(account1.getUpdateTimestamp());
        assertThat(result2.getAccountNumber()).isEqualTo(account2.getAccountNumber());
        assertThat(result2.getCheckDigit()).isEqualTo(account2.getCheckDigit());
        assertThat(result2.getCountryCode()).isEqualTo(account2.getCountryCode());
        assertThat(result2.getBalance()).isEqualTo(account2.getBalance());
        assertThat(result2.getDebitLimit()).isEqualTo(account2.getDebitLimit());
        assertThat(result2.getCreationTime()).isEqualTo(account2.getCreationTime());
        assertThat(result2.getCreationDate()).isEqualTo(account2.getCreationDate());
        assertThat(result2.getUpdateTimestamp()).isEqualTo(account2.getUpdateTimestamp());
    }

    @Test
    public void shouldFindById() {
        // given
        // when
        Optional<Account> accountOptional = accountRepository.findById(1);
        // then
        assertThat(accountOptional).isNotEmpty();
        Account account = accountOptional.get();
        assertThat(account.getAccountNumber()).isEqualTo(account1.getAccountNumber());
        assertThat(account.getCheckDigit()).isEqualTo(account1.getCheckDigit());
        assertThat(account.getCountryCode()).isEqualTo(account1.getCountryCode());
        assertThat(account.getBalance()).isEqualTo(account1.getBalance());
        assertThat(account.getDebitLimit()).isEqualTo(account1.getDebitLimit());
        assertThat(account.getCreationTime()).isEqualTo(account1.getCreationTime());
        assertThat(account.getCreationDate()).isEqualTo(account1.getCreationDate());
        assertThat(account.getUpdateTimestamp()).isEqualTo(account1.getUpdateTimestamp());

    }

}
