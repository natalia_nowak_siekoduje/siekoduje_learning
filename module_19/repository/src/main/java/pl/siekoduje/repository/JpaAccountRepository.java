package pl.siekoduje.repository;

import lombok.extern.slf4j.Slf4j;
import pl.siekoduje.model.Account;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Slf4j
public class JpaAccountRepository implements AccountRepository {
    private final EntityManager entityManager;

    public JpaAccountRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Optional<Account> findById(Integer id) {
        Account account = entityManager.find(Account.class, id);
        return Optional.ofNullable(account);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Account> findAll() {
        Query query = entityManager.createQuery("SELECT a FROM Account a");
        return query.getResultList();
    }

    @Override
    public Optional<Account> save(Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.persist(account);
            transaction.commit();
            return Optional.of(account);
        } catch (Exception e) {
            log.warn("Unable to save entity", e);
            transaction.rollback();
            throw new IllegalArgumentException("Unable to save account: " + account);

        }
    }

    public Optional<Account> update(int id, Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        Optional<Account> foundAccount = this.findById(id);
        try {
            transaction.begin();
            foundAccount.ifPresentOrElse(a -> {
                updateAccount(a, account);
                    }, () -> {throw new IllegalArgumentException();});
            transaction.commit();
        } catch (Exception e) {
            log.warn("Unable to update entity.", e);
            transaction.rollback();
            throw new IllegalArgumentException("Unable to update account with id: " + id);
        }
        return foundAccount;
    }

    @Override
    public void delete(Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.remove(account);
            transaction.commit();
        } catch (Exception e) {
            log.warn("Unable to delete entity", e);
            transaction.rollback();
            throw new IllegalArgumentException("There is no account: " + account);
        }
    }

    @Override
    public void deleteAllAccounts() {
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Query query = entityManager.createQuery("DELETE FROM Account");
            query.executeUpdate();
            transaction.commit();
        } catch (Exception e) {
            log.warn("Unable to delete entity", e);
            transaction.rollback();
            throw new IllegalArgumentException("Unable to delete all accounts");
        }
    }

    private void updateAccount(Account originalAccount, Account referenceAccount) {
        originalAccount.setAccountNumber(referenceAccount.getAccountNumber());
        originalAccount.setCheckDigit(referenceAccount.getCheckDigit());
        originalAccount.setCountryCode(referenceAccount.getCountryCode());
        originalAccount.setBalance(referenceAccount.getBalance());
        originalAccount.setDebitLimit(referenceAccount.getDebitLimit());
        originalAccount.setUpdateTimestamp(referenceAccount.getUpdateTimestamp());
        originalAccount.setCreationDate(referenceAccount.getCreationDate());
        originalAccount.setCreationTime(referenceAccount.getCreationTime());
    }
}
