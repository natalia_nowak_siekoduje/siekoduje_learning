package pl.siekoduje.repository;

import pl.siekoduje.model.Account;

import java.util.List;
import java.util.Optional;

public interface AccountRepository {
    Optional<Account> findById(Integer id);

    List<Account> findAll();

    Optional<Account> save(Account account);

    Optional<Account> update(int id, Account account);

    void delete(Account account);

    void deleteAllAccounts();
}
