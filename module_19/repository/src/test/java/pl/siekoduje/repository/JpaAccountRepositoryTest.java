package pl.siekoduje.repository;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Account;
import pl.siekoduje.repository.JpaAccountRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Slf4j
public class JpaAccountRepositoryTest {
    private final Account account1 = Account.builder()
            .accountNumber("111111111111113")
            .checkDigit(30)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-02 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(10, 12))
            .balance(BigDecimal.valueOf(20.00))
            .debitLimit(500.0)
            .build();
    private final Account account2 = Account.builder()
            .accountNumber("111111111111118")
            .checkDigit(00)
            .countryCode("PL")
            .updateTimestamp(Timestamp.valueOf("2021-03-01 11:20:20"))
            .creationDate(LocalDate.of(2012, 12, 12))
            .creationTime(LocalTime.of(12, 12))
            .balance(BigDecimal.valueOf(20000.00))
            .debitLimit(5000.0)
            .build();

    private EntityManagerFactory emf;
    private EntityManager em;
    private JpaAccountRepository accountRepository;

    @BeforeEach
    public void beforeEach() {
        emf = Persistence.createEntityManagerFactory("jpaTestExample");
        em = emf.createEntityManager();
        accountRepository = new JpaAccountRepository(em);
        prepareInitialData();
    }

    private void prepareInitialData() {
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            em.persist(account1);
            em.persist(account2);
            em.flush();
            transaction.commit();
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    @AfterEach
    public void afterEach() {
        em.close();
        emf.close();
    }

    @Test
    public void shouldSave() {
        //given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        //when
        accountRepository.save(account);
        //then
        assertThat(accountRepository.findAll())
                .hasSize(3);
        Account result = em.find(Account.class, 3);
        compareAccount(result, account);
    }

    @Test
    public void shouldUpdate() {
        // given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        // when
        accountRepository.update(1, account);
        // then
        Account result = em.find(Account.class, 1);
        compareAccount(result, account);
    }

    @Test
    public void shouldNotUpdateWhenWrongId() {
        // given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000.00))
                .debitLimit(5000.0)
                .build();
        // when
        IllegalArgumentException thrown = assertThrows( IllegalArgumentException.class, () ->{
                accountRepository.update(-1, account);});

        // then
        assertThat(thrown.getMessage()).isEqualTo("Unable to update account with id: " + -1L );
        Account resultAccount1 = em.find(Account.class, 1);
        Account resultAccount2 = em.find(Account.class, 2);
        compareAccount(resultAccount1, account1);
        compareAccount(resultAccount2, account2);
    }

    @Test
    public void shouldFindAll() {
        // given
        // when
        List<Account> allAccounts = accountRepository.findAll();
        // then
        assertThat(allAccounts).hasSize(2);
        Account result1 = em.find(Account.class, 1);
        Account result2 = em.find(Account.class, 2);
        compareAccount(result1, account1);
        compareAccount(result2, account2);
    }

    @Test
    public void shouldFindById() {
        // given
        // when
        Optional<Account> accountOptional = accountRepository.findById(1);
        // then
        assertThat(accountOptional).isNotEmpty();
        Account account = accountOptional.get();
        compareAccount(account, account1);
    }

    @Test
    public void shouldNotFindByIdWhenWrongId() {
        // given
        // when
        Optional<Account> accountOptional = accountRepository.findById(-1);
        // then
        assertThat(accountOptional).isEmpty();
    }

    @Test
    public void shouldDeleteAccount() {
        // given
        Account account = em.find(Account.class, 1);
        // when
        accountRepository.delete(account);
        // then
        assertThat(em.find(Account.class, 1)).isNull();
    }

    @Test
    public void shouldNotDeleteAccountWhenWrongId() {
        // given
        Account account = Account.builder()
                .accountNumber("222222222222222")
                .checkDigit(10)
                .countryCode("PL")
                .updateTimestamp(Timestamp.valueOf("2021-03-10 11:20:20"))
                .creationDate(LocalDate.of(2012, 12, 12))
                .creationTime(LocalTime.of(12, 12))
                .balance(BigDecimal.valueOf(20000))
                .debitLimit(5000.0)
                .build();
        account.setId(-1);
        // when
             IllegalArgumentException thrown = assertThrows(IllegalArgumentException.class, () ->
                        accountRepository.delete(account));
        // then
        assertThat(thrown.getMessage()).isEqualTo("There is no account: " + account);
        compareAccount(em.find(Account.class, 1),account1);
        compareAccount(em.find(Account.class, 2), account2);
    }

    @Test
    public void shouldDeleteAllAccounts() {
        // given
        // when
        accountRepository.deleteAllAccounts();
        // then
        Query query = em.createQuery("SELECT a FROM Account a");
        List resultList = query.getResultList();
        assertThat(resultList).isEmpty();
    }

    private void compareAccount(Account a1, Account a2) {
        assertThat(a1.getAccountNumber()).isEqualTo(a2.getAccountNumber());
        assertThat(a1.getCheckDigit()).isEqualTo(a2.getCheckDigit());
        assertThat(a1.getCountryCode()).isEqualTo(a2.getCountryCode());
        assertThat(a1.getBalance()).isEqualByComparingTo(a2.getBalance());
        assertThat(a1.getDebitLimit()).isEqualTo(a2.getDebitLimit());
        assertThat(a1.getCreationTime()).isEqualTo(a2.getCreationTime());
        assertThat(a1.getCreationDate()).isEqualTo(a2.getCreationDate());
        assertThat(a1.getUpdateTimestamp()).isEqualTo(a2.getUpdateTimestamp());

    }

}
