package pl.siekoduje;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Account;
import pl.siekoduje.model.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class JPAExampleIT {
    private EntityManagerFactory emf;
    private EntityManager em;

    @BeforeEach
    public void beforeEach() {
        emf = Persistence.createEntityManagerFactory("jpaTestExample");
        em = emf.createEntityManager();
    }

    @AfterEach
    public void afterEach() {
        em.close();
        emf.close();
    }

    @Test
    public void testCustomerAndAccountOneToManyBidirectionalRelationship() {
        em.getTransaction().begin();
        Account account1 = Account.builder()
                .ammount(BigDecimal.valueOf(1000))
                .currency("PLN")
                .build();

        Account account2 = Account.builder()
                .ammount(BigDecimal.valueOf(555))
                .currency("PLN")
                .build();
        Customer customer = Customer.builder()
                .firstName("Kasia")
                .lastName("Kowalska")
                .build();
        customer.addAccount(account1);
        customer.addAccount(account2);
        em.persist(account1);
        em.persist(account2);
        em.persist(customer);
        em.getTransaction().commit();
        assertThat(em.find(Customer.class, 1).getAccounts()).contains(account1, account2);
        assertThat(em.find(Account.class, 1).getCustomer()).isEqualTo(customer);
        assertThat(em.find(Account.class, 2).getCustomer()).isEqualTo(customer);
    }

}