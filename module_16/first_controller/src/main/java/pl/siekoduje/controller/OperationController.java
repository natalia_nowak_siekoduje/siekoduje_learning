package pl.siekoduje.controller;

import lombok.RequiredArgsConstructor;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.config.OperationProperties;

@RestController
@RequiredArgsConstructor
public class OperationController {
    private final OperationProperties operationProperties;

    @GetMapping("api/get")
    public String getStringBody() {
        return operationProperties.getGet().getReturnValue();
    }

    @PostMapping("api/post")
    public String postStringBody() {
        return operationProperties.getPost().getReturnValue();
    }

    @PutMapping("api/put")
    public String putStringBody() {
        return operationProperties.getPut().getReturnValue();
    }

    @DeleteMapping("api/delete")
    public String deleteStringBody() {
        return operationProperties.getDelete().getReturnValue();
    }

}
