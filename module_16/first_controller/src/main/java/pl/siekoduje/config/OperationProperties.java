package pl.siekoduje.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "operation")
@Getter
@Setter
public class OperationProperties {

    private Operation get;
    private Operation put;
    private Operation post;
    private Operation delete;

    @Getter
    @Setter
    public static class Operation {
        private String returnValue;
    }


}
