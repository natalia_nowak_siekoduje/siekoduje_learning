package pl.siekoduje.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Appointment;
import pl.siekoduje.model.TimeSlot;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class AppointmentServiceTest {
    private final TimeSlotService timeSlotService = new TimeSlotService();
    private final AppointmentService appointmentService = new AppointmentService(timeSlotService);
    private String doctorId;
    private int slotId;
    private LocalDate date;
    private LocalTime timeSlotStart;
    private LocalTime timeSlotEnd;
    private TimeSlot timeSlot;
    private String patientId;


    @BeforeEach
    public void setUp() {
        doctorId = "JohnSmith";
        slotId = 1000;
        date = LocalDate.of(2020, 12, 1);
        timeSlotStart = LocalTime.of(10, 0, 0);
        timeSlotEnd = LocalTime.of(10, 30, 0);
        timeSlot = TimeSlot.builder()
                .available(true)
                .id(slotId)
                .doctorId(doctorId)
                .start(timeSlotStart)
                .end(timeSlotEnd)
                .build();
        patientId = "oliviaJones";

    }


    @Test
    public void shouldBookAppointment() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);

        //when
        Optional<Appointment> appointmentOptional = appointmentService.bookAppointment(doctorId, slotId, patientId);
        //then
        assertThat(appointmentOptional.isPresent()).isTrue();
        assertThat(timeSlot.isAvailable()).isFalse();
    }

    @Test
    public void shouldNotBookAppointmentWhenWrongSlotId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);

        //when
        Optional<Appointment> appointmentOptional = appointmentService.bookAppointment(doctorId, slotId + 1, patientId);
        //then
        assertThat(appointmentOptional.isEmpty()).isTrue();
        assertThat(timeSlot.isAvailable()).isTrue();
    }


    @Test
    public void shouldNotBookAppointmentWhenWrongDoctorId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);

        //when
        Optional<Appointment> appointmentOptional = appointmentService.bookAppointment("WrongId", slotId, patientId);
        //then
        assertThat(appointmentOptional.isEmpty()).isTrue();
        assertThat(timeSlot.isAvailable()).isTrue();
    }

    @Test
    public void shouldCancelAppointment() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        appointmentService.bookAppointment(doctorId, slotId, patientId);
        //when
       
        boolean result = appointmentService.cancelAppointment(doctorId, slotId);
        //then
        assertThat(result).isTrue();
        assertThat(timeSlot.isAvailable()).isTrue();
    }

    @Test
    public void shouldNotCancelAppointmentWhenWrongDoctorId() {
        //given
        //when
        boolean result = appointmentService.cancelAppointment("wrongDoctorId", slotId);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldNotCancelAppointmentWhenWrongSlotId() {
        //given
        //when
        boolean result = appointmentService.cancelAppointment(doctorId, -1);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldGetAppointment() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        appointmentService.bookAppointment(doctorId, slotId, patientId);
        //when
        Optional<Appointment> optionalAppointment = appointmentService.getAppointment(doctorId, slotId);
        //then
        assertThat(optionalAppointment.isPresent()).isTrue();
        assertThat(optionalAppointment.get().getPatientId()).isEqualTo(patientId);
        assertThat(optionalAppointment.get().getTimeSlot()).isEqualTo(timeSlot);
    }

    @Test
    public void shouldNotGetAppointmentWhenWrongSlotId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        appointmentService.bookAppointment(doctorId, slotId, patientId);
        //when
        Optional<Appointment> optionalAppointment = appointmentService.getAppointment(doctorId, slotId + 1);
        //then

        assertThat(optionalAppointment.isEmpty()).isTrue();
    }

    @Test
    public void shouldNotGetAppointmentWhenWrongDoctorId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        appointmentService.bookAppointment(doctorId, slotId, patientId);
        //when
        Optional<Appointment> optionalAppointment2 = appointmentService.getAppointment("wrongId", slotId);
        //then
        assertThat(optionalAppointment2.isEmpty()).isTrue();
    }
}
