package pl.siekoduje.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.TimeSlot;
import pl.siekoduje.model.TimeSlotList;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class TimeSlotServiceTest {

    private final TimeSlotService timeSlotService = new TimeSlotService();
    private String doctorId;
    private int slotId;
    private LocalDate date;
    private LocalTime timeSlotStart;
    private LocalTime timeSlotEnd;
    private TimeSlot timeSlot;

    @BeforeEach
    public void setUp() {
        doctorId = "JohnSmith";
        slotId = 1000;
        date = LocalDate.of(2020, 12, 1);
        timeSlotStart = LocalTime.of(10, 0, 0);
        timeSlotEnd = LocalTime.of(10, 30, 0);
        timeSlot = TimeSlot.builder()
                .available(true)
                .id(slotId)
                .doctorId(doctorId)
                .start(timeSlotStart)
                .end(timeSlotEnd)
                .build();
    }

    @Test
    public void shouldAddNewTimeSlot() {
        // given

        // when
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);

        //then
        List<TimeSlot> result = timeSlotService.getSlots().get(doctorId).get(date).getSlots();
        assertThat(result.contains(timeSlot)).isTrue();
    }

    @Test
    public void shouldGetTimeSlot() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when
        Optional<TimeSlot> result = timeSlotService.getSlot(doctorId, timeSlot.getId());
        //then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(timeSlot);
    }

    @Test
    public void shouldNotReturnAppointmentWhenWrongSlotIdl() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when
        Optional<TimeSlot> resultWithWrongSlotId = timeSlotService.getSlot(doctorId, timeSlot.getId() + 1);
        //then
        assertThat(resultWithWrongSlotId.isEmpty()).isTrue();
    }

    @Test
    public void shouldNotReturnAppointmentWhenWrongDoctorId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when

        Optional<TimeSlot> resultWithWrongDoctorId = timeSlotService.getSlot("aaa", timeSlot.getId());
        //then
        assertThat(resultWithWrongDoctorId.isEmpty()).isTrue();
    }

    @Test
    public void shouldBookTimeSlot() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when
        boolean result = timeSlotService.bookTimeSlot(doctorId, timeSlot.getId());
        //then
        assertThat(result).isTrue();
        assertThat(timeSlot.isAvailable()).isFalse();
    }

    @Test
    public void shouldNotBookTimeSlotWhenWrongDoctorId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        timeSlotService.bookTimeSlot(doctorId, timeSlot.getId());
        //when
        boolean result = timeSlotService.bookTimeSlot("wrongDoctorId", timeSlot.getId());

        //then
        assertThat(result).isFalse();

    }

    @Test
    public void shouldNotBookTimeSlotWhenWrongSlotId() {
        //given
        int wrongSlotId = 10;
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        timeSlotService.bookTimeSlot(doctorId, timeSlot.getId());
        //when
        boolean result = timeSlotService.bookTimeSlot(doctorId, wrongSlotId);
        //then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReleaseTimeSLot() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        timeSlotService.bookTimeSlot(doctorId, timeSlot.getId());
        //when
        timeSlotService.releaseTimeSlot(doctorId, timeSlot.getId());
        //then
        assertThat(timeSlot.isAvailable()).isTrue();
    }

    @Test
    public void shouldGetSlots() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when
        Optional<TimeSlotList> result = timeSlotService.getSlots(doctorId, date);
        //then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get().getSlots().contains(timeSlot)).isTrue();
    }

    @Test
    public void shouldNotReturnTimeSlotWhenWrongDate() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when
        Optional<TimeSlotList> result = timeSlotService.getSlots(doctorId, date.plusDays(1));
        //then
        assertThat(result.isPresent()).isFalse();
    }

    @Test
    public void shouldNotReturnTimeSlotWhenWrongDoctorId() {
        //given
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
        //when
        Optional<TimeSlotList> result = timeSlotService.getSlots("wrongId", date);
        //then
        assertThat(result.isPresent()).isFalse();
    }


}
