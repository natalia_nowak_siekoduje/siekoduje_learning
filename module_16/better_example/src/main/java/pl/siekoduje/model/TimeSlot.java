package pl.siekoduje.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalTime;

@Getter
@Setter
@Builder
public class TimeSlot {
    private int id;
    private String doctorId;
    private LocalTime start;
    private LocalTime end;
    private boolean available;

}
