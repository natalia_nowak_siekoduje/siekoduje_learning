package pl.siekoduje.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Appointment {
    private String patientId;
    private TimeSlot timeSlot;
}
