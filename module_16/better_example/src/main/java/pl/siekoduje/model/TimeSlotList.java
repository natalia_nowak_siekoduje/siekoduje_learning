package pl.siekoduje.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Getter
@Setter

public class TimeSlotList {
    private List<TimeSlot> slots = new ArrayList<>();

    public void addTimeSlot(TimeSlot slot) { slots.add(slot);
    }

}
