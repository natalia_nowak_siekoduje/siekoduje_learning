package pl.siekoduje.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.model.Appointment;
import pl.siekoduje.model.AppointmentRequest;
import pl.siekoduje.model.TimeSlot;
import pl.siekoduje.model.TimeSlotList;
import pl.siekoduje.service.AppointmentService;
import pl.siekoduje.service.TimeSlotService;

import java.time.LocalDate;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/doctors/{doctorId}/slots")
public class AppointmentController {
    private final AppointmentService appointmentService;
    private final TimeSlotService timeSlotService;


    @GetMapping
    public ResponseEntity<TimeSlotList> getTimeSlots(
            @PathVariable String doctorId,
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate localDate) {
        Optional<TimeSlotList> timeSlotList = timeSlotService.getSlots(doctorId, localDate);
        if (timeSlotList.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(timeSlotList.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    //    curl -s -X GET 'http://localhost:8080/api/doctors/JimJones/slots/1002/appointment' | json_pp
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{slotId}/appointment")
    public ResponseEntity<Appointment> getAppointment(
            @PathVariable String doctorId,
            @PathVariable int slotId) {
        Optional<Appointment> appointment = appointmentService.getAppointment(doctorId, slotId);
        if (appointment.isPresent())
            return ResponseEntity.status(HttpStatus.OK)
                    .body(appointment.get());
        else
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .build();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public void addTimeSlot(
            @PathVariable String doctorId,
            @RequestBody TimeSlot timeSlot,
            @RequestBody LocalDate date) {
        timeSlotService.addNewTimeSlot(timeSlot, doctorId, date);
    }

    //  curl -D - -s -X POST -H "Content-Type: application/json" -d
// '{"timeSlot":{"id":1001,"doctorId":"johnsmith","start":"14:30:00","end":"15:00:00"}}' 'http://localhost:8080/api/doctors/jonSmith/slots'
    @PostMapping("{slotId}")
    public ResponseEntity<Appointment> bookAppointment(
            @PathVariable String doctorId,
            @PathVariable int slotId,
            @RequestBody AppointmentRequest appointmentRequest) {
        Optional<Appointment> appointment = appointmentService.bookAppointment(doctorId, slotId, appointmentRequest.getPatientId());
        if (appointment.isPresent()) {
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(appointment.get());
        }

        return ResponseEntity.status(HttpStatus.CONFLICT)
                .build();

    }

    @DeleteMapping("{slotId}/appointment")
    public ResponseEntity<Void> deleteAppointment(
            @PathVariable String doctorId,
            @PathVariable int slotId
    ) {
        if (appointmentService.cancelAppointment(doctorId, slotId)) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }


}
