package pl.siekoduje.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.siekoduje.model.Appointment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Getter
@Setter
@Slf4j
public class AppointmentService {
    private final TimeSlotService timeSlotService;
    private final Map<String, List<Appointment>> appointments = new HashMap<>(); // mamy slotId i wizyte

    public Optional<Appointment> bookAppointment(String doctorId, int slotId, String patientId) {
        if (timeSlotService.getSlot(doctorId, slotId).isPresent() && timeSlotService.bookTimeSlot(doctorId, slotId)) {
            Appointment appointment = buildNewAppointment(doctorId, slotId, patientId);
            List<Appointment> doctorAppointments = appointments.getOrDefault(doctorId, new ArrayList<>());
            doctorAppointments.add(appointment);
            appointments.put(doctorId, doctorAppointments);
            return Optional.of(appointment);
        }
        return Optional.empty();
    }

    private Appointment buildNewAppointment(String doctorId, int slotId, String patientId) {

        return Appointment.builder()
                .patientId(patientId)
                .timeSlot(timeSlotService.getSlot(doctorId, slotId).get())
                .build();
    }

    public boolean cancelAppointment(String doctorId, int slotId) {
        List<Appointment> doctorAppointments = this.appointments.getOrDefault(doctorId, new ArrayList<>());
        return doctorAppointments
                .stream()
                .filter(a -> isMatchingTimeSlotId(slotId, a))
                .findFirst()
                .map(a -> {
                    timeSlotService.releaseTimeSlot(doctorId, slotId);
                    appointments.remove(a);
                    return true;
                }).orElse(false);

    }

    private boolean isMatchingTimeSlotId(int slotId, Appointment a) {
        return a.getTimeSlot().getId() == slotId;
    }


    public Optional<Appointment> getAppointment(String doctorId, int slotId) {
        List<Appointment> doctorAppointments = appointments.getOrDefault(doctorId, new ArrayList<>());
        return doctorAppointments.stream()
                .filter(a -> isMatchingTimeSlotId(slotId, a))
                .findFirst();
    }


}
