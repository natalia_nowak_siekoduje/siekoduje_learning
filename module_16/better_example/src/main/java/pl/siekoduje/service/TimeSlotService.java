package pl.siekoduje.service;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.siekoduje.model.TimeSlot;
import pl.siekoduje.model.TimeSlotList;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Getter
@Setter
@Builder
@Slf4j
public class TimeSlotService {

    private final Map<String, Map<LocalDate, TimeSlotList>> slots = new HashMap<>();


    public void addNewTimeSlot(TimeSlot slot, String doctorId, LocalDate date) {
        Map<LocalDate, TimeSlotList> timeSlots = slots.getOrDefault(doctorId, getOrCreateDoctorSchedule(doctorId));
        TimeSlotList l = timeSlots.getOrDefault(date, new TimeSlotList());
        l.addTimeSlot(slot);
        timeSlots.put(date, l);
    }

    private Map<LocalDate, TimeSlotList> getOrCreateDoctorSchedule(String doctorId) {
        Map<LocalDate, TimeSlotList> doctorMap = slots.get(doctorId);
        if (doctorMap == null) {
            doctorMap = new HashMap<>();
            slots.put(doctorId, doctorMap);
        }
        return doctorMap;
    }

    public boolean bookTimeSlot(String doctorId, int slotId) {
        Optional<TimeSlot> slotOptional = getSlot(doctorId, slotId);
        if (slotOptional.isPresent()) {
            TimeSlot slot = slotOptional.get();
            if (slot.isAvailable()) {
                getSlot(doctorId, slotId).get().setAvailable(false);
                return true;
            }
        }
        return false;
    }

    public void releaseTimeSlot(String doctorId, int slotId) {
        slots.getOrDefault(doctorId, new HashMap<>())
                .values()
                .stream()
                .forEach(list -> list.getSlots()
                        .stream()
                        .filter(slot -> slot.getId() == slotId && !slot.isAvailable())
                        .findFirst()
                        .ifPresent(slot -> slot.setAvailable(true))
                );
    }

    public Optional<TimeSlot> getSlot(String doctorId, int slotId) {
        Map<LocalDate, TimeSlotList> doctorSlots = slots.getOrDefault(doctorId, new HashMap<>());
        return slots.getOrDefault(doctorId, new HashMap<>())
                .values()
                .stream()
                .map(TimeSlotList::getSlots)
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                .stream()
                .filter(slot -> slot.getId() == slotId)
                .findFirst();
    }

    public Optional<TimeSlotList> getSlots(String doctorId, LocalDate date) {
        if (slots.containsKey(doctorId)) {
            return Optional.ofNullable(slots.get(doctorId).get(date));
        }
        return Optional.empty();

    }
}
