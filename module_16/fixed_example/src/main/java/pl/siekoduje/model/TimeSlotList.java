package pl.siekoduje.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@Builder
public class TimeSlotList {
    private List<TimeSlot> slots;
}
