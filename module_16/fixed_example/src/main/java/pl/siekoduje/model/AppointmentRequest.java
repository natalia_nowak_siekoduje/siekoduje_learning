package pl.siekoduje.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AppointmentRequest {
    private String patientId;
}
