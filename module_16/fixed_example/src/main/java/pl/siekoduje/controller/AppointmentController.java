package pl.siekoduje.controller;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.model.Appointment;
import pl.siekoduje.model.AppointmentRequest;
import pl.siekoduje.model.TimeSlot;
import pl.siekoduje.model.TimeSlotList;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/doctors/{doctorId}/slots")
public class AppointmentController {
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public TimeSlotList getTimeSlots(
            @PathVariable String doctorId,
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate localDate) {
        List<TimeSlot> slots = new ArrayList<>();
        slots.add(
                TimeSlot.builder()
                        .id(1000)
                        .doctorId(doctorId)
                        .start(LocalTime.of(14, 0, 0))
                        .end(LocalTime.of(14, 30, 0))
                        .build());
        slots.add(
                TimeSlot.builder()
                        .id(1001)
                        .doctorId(doctorId)
                        .start(LocalTime.of(14, 30, 0))
                        .end(LocalTime.of(15, 0, 0))
                        .build());
        slots.add(
                TimeSlot.builder()
                        .id(1002)
                        .doctorId(doctorId)
                        .start(LocalTime.of(16, 30, 0))
                        .end(LocalTime.of(17, 0, 0))
                        .build());
        return TimeSlotList.builder().slots(slots).build();
    }

//    curl -s -X GET 'http://localhost:8080/api/doctors/JimJones/slots/1002/appointment' | json_pp
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("{slotId}/appointment")
    public Appointment getAppointment(
            @PathVariable String doctorId,
            @PathVariable int slotId) {
        Appointment appointment =
                Appointment.builder()
                        .patientId("oliviajones")
                        .timeSlot(
                                TimeSlot.builder()
                                        .id(slotId)
                                        .doctorId(doctorId)
                                        .start(LocalTime.of(14, 30, 0))
                                        .end(LocalTime.of(15, 0, 0))
                                        .build())
                        .build();
        return appointment;
    }


    @PostMapping("{slotId}")
    public ResponseEntity<Appointment> bookAppointment(
            @PathVariable String doctorId,
            @PathVariable Long slotId,
            @RequestBody AppointmentRequest appointmentRequest) {

        if(slotId == 1000){
            return ResponseEntity.status(HttpStatus.CONFLICT)
                .build();
        }
        Appointment appointment =
                Appointment.builder()
                        .patientId(appointmentRequest.getPatientId())
                        .timeSlot(
                                TimeSlot.builder()
                                        .id(1001)
                                        .doctorId(doctorId)
                                        .start(LocalTime.of(14, 30, 0))
                                        .end(LocalTime.of(15, 0, 0))
                                        .build())
                        .build();
        return ResponseEntity.status(HttpStatus.CREATED)
                .header(
                        HttpHeaders.LOCATION,
                        "api/doctors/" + doctorId + "/slots/" + slotId + "/appointment")
                .body(appointment);
    }


}
