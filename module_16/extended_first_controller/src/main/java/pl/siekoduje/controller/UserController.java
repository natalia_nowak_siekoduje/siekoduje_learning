package pl.siekoduje.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;

@Slf4j
@RestController
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("api/users/{userId}")
    public User getUser(@RequestParam Long userId) {
        return userRepository.getUser(userId);
    }

    @PostMapping("api/users")
    public User createUser(RequestEntity<User> userRequestEntity) {
        return userRepository.addUser(userRequestEntity.getBody());
    }

    @PutMapping("api/users/{userId}")
    public void updateUser(@PathVariable Long userId, RequestEntity<User> requestEntity) {
        log.info(requestEntity.getBody().toString());
    }

    @DeleteMapping("api/users/{userId}")
    public User deleteUser(@PathVariable Long userId) {
        return userRepository.deleteUser(userId);
    }

}
