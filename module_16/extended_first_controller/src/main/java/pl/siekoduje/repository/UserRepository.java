package pl.siekoduje.repository;

import org.springframework.stereotype.Component;
import pl.siekoduje.model.User;

import java.util.HashMap;
import java.util.NoSuchElementException;


@Component
public class UserRepository {

    private final HashMap<Long, User> users = new HashMap<>();

    public User getUser(Long userId) {
        if (users.containsKey(userId))
            return users.get(userId);
        throw new NoSuchElementException("User with id:" + userId + " doesn't exist");
    }

    public User deleteUser(Long userId) {
        if (users.containsKey(userId)) {
            User deletedUser = users.get(userId);
            users.remove(userId);
            return deletedUser;
        } else {
            throw new NoSuchElementException("User with id:" + userId + " doesn't exist");
        }
    }

    public User addUser(User user ) {
        long userId = users.size() > 0 ? users.size() : 1;
        users.put(userId, user);
        return user;
    }

//    public User addUser(long userId, String firstName, String lastName) {
//        User user = User.builder()
//                .firstName(firstName)
//                .lastName(lastName)
//                .build();
//        users.put(userId, user);
//        return user;
//    }
}
