package service;

import controller.AccountController;
import model.Account;
import model.Currency;
import repository.AccountRepository;

import java.util.Collection;
import java.util.Optional;

public class AccountService {

    private  AccountRepository accountRepository;

    public AccountService() {
        accountRepository = new AccountRepository();
    }

    public Account createAccount(String number, Double value) {
        accountRepository.create(new Account(number, value));
        return accountRepository.findAccount(number).get();
    }

    public void deleteAccount(String number) {
        accountRepository.deleteAccount(number);
    }

    public Collection< Account> getAll(){
        return accountRepository.getAll();
    }

    public Optional<Account> findAccount(String number) {
        return accountRepository.findAccount(number);
    }
}
