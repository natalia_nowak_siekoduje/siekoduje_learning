import controller.AccountController;
import model.Currency;
import repository.AccountRepository;
import service.AccountService;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Application {



    public static void main(String[] args) {

        AccountController accountController = new AccountController();
        Application app = new Application();

        try {
            URI uri = Thread.currentThread().getContextClassLoader()
                    .getResource("accounts.csv")
                    .toURI();
            Path path = Paths.get(uri);
            app.readFile(path, accountController);
        } catch (Exception e) {
            e.printStackTrace();
        }
        accountController.updateAccountBalance("55 5555 5555 5555 5555 5555 5555", 9080.0);
        System.out.println(accountController.getAccountBalance("11 1111 1111 1111 1111 1111 1111"));
        System.out.println(accountController.getAccountBalance("55 5555 5555 5555 5555 5555 5555"));
    }


    public  void readFile(Path file, AccountController accountController) throws IOException {
        Files.lines(file)
                .filter(s -> !s.contains("number"))
                .forEach(s -> {
            String[] data = s.split(",");
            accountController.createAccount(data[0], new Double(data[1]));
        });
    }

}
