package repository;

import model.Account;
import service.AccountService;

import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class AccountRepository {

    private ConcurrentHashMap<String, Account> accounts = new ConcurrentHashMap<>();

    public void create(Account account) {
        accounts.put(account.getNumber(), account);
    }

    public Optional<Account> read(String number) {
       return findAccount(number);
    }

    public void update(String number) {
        // jak to ma działać ?
    }

    public void deleteAccount(String number) {
        accounts.remove(number);
    }

    public Collection< Account> getAll(){
        return accounts.values();
    }

    public Optional<Account> findAccount(String number) {
        return Optional.ofNullable(accounts.get(number));
    }
}
