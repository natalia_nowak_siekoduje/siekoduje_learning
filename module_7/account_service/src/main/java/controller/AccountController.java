package controller;

import model.Currency;
import service.AccountService;

public class AccountController {

    private AccountService accountService;

    public AccountController() {
        this.accountService = new AccountService();
    }

    public void createAccount(String number, Double value) {
        accountService.createAccount(number, value);
    }

    public boolean updateAccountBalance(String number, Double value) {
        if(accountService.findAccount(number).isPresent()) {
            accountService.findAccount(number).get().setBalance(value);
            return true;
        }
        return false;
    }
// zastanawiam się co mam tutaj zwracać, jesli podam zly numer ??
    public double getAccountBalance(String number) {
        return accountService.findAccount(number).get().getBalance();
    }

}
