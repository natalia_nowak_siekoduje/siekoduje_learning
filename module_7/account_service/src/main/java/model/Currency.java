package model;

public enum Currency {
    PLN,
    USD,
    GPB,
    EUR,
    CHF,
    IDR
}
