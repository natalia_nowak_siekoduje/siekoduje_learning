package model;

import java.util.*;


public class Account {

    private final String number;
    private Currency currency;
    private Double balance;

    public Account(String number, Currency currency) {
        this.number = number;
        this.currency = currency;
        this.balance = 0.0;
    }

    public Account(String number, Double balance) {
        this.number = number;
        this.currency = currency;
        this.balance = balance;
    }
    
    public String getNumber() {
        return number;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
