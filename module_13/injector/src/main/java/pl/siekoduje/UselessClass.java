package pl.siekoduje;

public class UselessClass {
    private DummyClass dummyClass;
    private StrangeClass strangeClass;

    public DummyClass getDummyClass() {
        return dummyClass;
    }

    public void setDummyClass(DummyClass dummyClass) {
        this.dummyClass = dummyClass;
    }

    public StrangeClass getStrangeClass() {
        return strangeClass;
    }

    public void setStrangeClass(StrangeClass strangeClass) {
        this.strangeClass = strangeClass;
    }
}
