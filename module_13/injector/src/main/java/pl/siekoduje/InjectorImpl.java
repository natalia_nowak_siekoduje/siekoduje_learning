package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class InjectorImpl implements Injector {
    private final Map<String, Object > objects = new HashMap<>();
    @Override
    public <T> T createWithDependencies(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if(objects.containsValue(clazz))
            return (T) objects.get(clazz.getName());
        T object = createObject(clazz);
        Arrays.stream(object.getClass().getDeclaredFields()).forEach(f -> {
            try {
               initializeField(object, f);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                log.error("Unable to initialize field ", e);
                throw new InitializingFieldException();
            }
        });
        objects.put(clazz.getName(), object);

        return object;
    }

    private void initializeField(Object target, Field fieldOfNewObject) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        fieldOfNewObject.setAccessible(true);
        log.info("szukam obiektu :  {} ",  fieldOfNewObject.getGenericType().getTypeName());
        if(!objects.containsKey(fieldOfNewObject.getType().getName())) {
            log.info("nie znaleziono obiektu klasy :  {} ",  fieldOfNewObject.getGenericType().getTypeName());
            Object newInstance = createObject(fieldOfNewObject.getType());
            objects.put(newInstance.getClass().getName(), newInstance);
            log.info("dodaje do puli obiektow pare: ( {} , {} )",  newInstance.getClass().getName(), newInstance);
            fieldOfNewObject.set(target, newInstance);
        } else {
            fieldOfNewObject.set(target, objects.get(fieldOfNewObject.getClass().getTypeName()));
        }
    }

    private <T> T createObject(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<T> declaredConstructor = clazz.getDeclaredConstructor();
        log.info("Tworzę obiekt klasy " + declaredConstructor.newInstance().getClass().getName());
        return declaredConstructor.newInstance();
    }
    
    public Map<String, Object> getObjects() {
        return objects;
    }

}
