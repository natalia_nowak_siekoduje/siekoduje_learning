package pl.siekoduje;

import java.lang.reflect.InvocationTargetException;

public interface Injector {
    <T> T createWithDependencies(Class<T> clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
}
