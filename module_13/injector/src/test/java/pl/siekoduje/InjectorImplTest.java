package pl.siekoduje;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;


public class InjectorImplTest {
    private InjectorImpl injector = new InjectorImpl();

    @Test
    public void shouldCreateWithDependencies() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //when
        UselessClass result = injector.createWithDependencies(UselessClass.class);
        //then
        assertThat(result).isNotNull();
        assertThat(result.getDummyClass()).isNotNull();
        assertThat(result.getStrangeClass()).isNotNull();
    }

    @Test
    public void shouldThrowInitializingFieldException() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        //when
       Throwable thrown  = catchThrowable(() -> {
           injector.createWithDependencies(ClassWithError.class);
       });
       //then
        assertThat(thrown).isInstanceOf(InitializingFieldException.class);
    }

}