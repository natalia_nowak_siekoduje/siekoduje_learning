import com.icegreen.greenmail.configuration.GreenMailConfiguration;
import com.icegreen.greenmail.junit4.GreenMailRule;
import com.icegreen.greenmail.pop3.commands.PassCommand;
import com.icegreen.greenmail.util.ServerSetupTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.siekoduje.config.TestConfig;
import pl.siekoduje.copy.CopyTaskExecutor;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@Slf4j
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class ApplicationIT {
    @ClassRule
    public static TemporaryFolder temporaryFolder = new TemporaryFolder();
    private static File folder;
    private static final String LOGIN = "kkowalski11214@gmail.com";
    private static final String PASSWORD = "Qwerty1234!";

    @Autowired
    private ApplicationContext context;
    @Rule
    public GreenMailRule greenMailRule = new GreenMailRule(ServerSetupTest.SMTP).withConfiguration(GreenMailConfiguration.aConfig().withUser(LOGIN, PASSWORD));
    
    @BeforeClass
    public static void setUp() throws IOException {
        folder = temporaryFolder.getRoot();
        when(TestConfig.applicationProperties.getInputDirectory()).thenReturn(createNewFolder("from-dir"));
        when(TestConfig.applicationProperties.getSmtpServer()).thenReturn("127.0.0.1");
        when(TestConfig.applicationProperties.getSmtpPort()).thenReturn("3025");
        when(TestConfig.applicationProperties.getSenderLogin()).thenReturn("kkowalski11214@gmail.com");
        when(TestConfig.applicationProperties.getSenderPassword()).thenReturn("Qwerty1234!");
        when(TestConfig.applicationProperties.getReceiverLogin()).thenReturn("llachows@gmail.com");
        when(TestConfig.applicationProperties.getTargetDirectory()).thenReturn(createNewFolder("to-dir"));
        System.out.println(folder);
    }

    private static File createNewFolder(String s) throws IOException {
        File newFile = new File(folder, s);
        newFile.mkdir();
        return newFile;
    }


    @Test
    public void shouldCallMethodsInRunMethod() throws Exception {
        //given
        CopyTaskExecutor copyTask = context.getBean(CopyTaskExecutor.class);
        Path testPath = Files.writeString(folder.toPath().resolve("from-dir\\test.txt"), "test");
        File targetDirectory = new File(folder, "to-dir");
        //when
        copyTask.start();
        //then
        waitUntilFileAppears(targetDirectory);
        MimeMessage[] receivedMessages = greenMailRule.getReceivedMessages();
        MimeMessage receivedMessage = receivedMessages[0];

        assertThat(receivedMessages.length).isEqualTo(1);

        String sender = receivedMessage.getFrom()[0].toString();
        String receiver = receivedMessage.getAllRecipients()[0].toString();
        assertThat(sender).isEqualTo(TestConfig.applicationProperties.getSenderLogin());
        assertThat(receiver).isEqualTo(TestConfig.applicationProperties.getReceiverLogin());

    }

    private void waitUntilFileAppears(File targetDirectory) throws InterruptedException {
        int counter = 0;
        //when

        while (!ifZipExsist(targetDirectory)) {
            Thread.sleep(1000*1);
            counter++;
            log.info("Checking if file exsists {} time", counter);
            if(counter>6)
                throw new AssertionError();

        };
    }

    private boolean ifZipExsist(File directory) {
        return Arrays.stream(directory.listFiles()).anyMatch(f -> {
            log.info(f.getName());
            return f.getName().equals("test.zip");
        });
    }

//    @Test
//    public void shouldReturnPropertyApplicationName() {
//        assertThat(applicationPropertiesMock.getApplicationName()).isEqualTo("PuzzlesInSpring");
//    }
}
