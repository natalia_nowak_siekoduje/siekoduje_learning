package pl.siekoduje.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import pl.siekoduje.email.EmailSenderService;

import static org.mockito.Mockito.mock;


@Configuration
@Import(AppConfig.class)
public class TestConfig {

    public static ApplicationProperties applicationProperties = mock(ApplicationProperties.class);;


    @Primary
    @Bean
    public ApplicationProperties applicationPropertiesMock() {
        return applicationProperties;
    }

}
