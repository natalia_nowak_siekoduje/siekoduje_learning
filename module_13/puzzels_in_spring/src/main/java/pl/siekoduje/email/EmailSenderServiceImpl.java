package pl.siekoduje.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.siekoduje.config.ApplicationProperties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

@Component
public class EmailSenderServiceImpl implements EmailSenderService {
    private final String username;
    private final String password;
    private final String smtpServer;
    private final String smtpPort;
    private final Session session;
    private final boolean smtpAuthEnabled;
    private final boolean smtpSslEnabled;


    @Autowired
    public EmailSenderServiceImpl(ApplicationProperties applicationProperties) {
        this.username = applicationProperties.getSenderLogin();
        this.password = applicationProperties.getSenderPassword();
        this.smtpServer = applicationProperties.getSmtpServer();
        this.smtpPort = applicationProperties.getSmtpPort();
        this.smtpAuthEnabled = applicationProperties.isSmtpAuthEnabled();
        this.smtpSslEnabled = applicationProperties.isSmtpSslEnabled();
        Properties properties = new Properties();
        properties.put("mail.smtp.host", smtpServer);
        properties.put("mail.smtp.port", smtpPort);
        properties.put("mail.smtp.auth", smtpAuthEnabled);
        properties.put("mail.smtp.ssl.enable", smtpSslEnabled);
        session = Session.getInstance(properties, null);
        session.setDebug(true); // log additional information
    }

    @Override
    public void sendEmail(String from, String to, String subject, String content) {

        try {
            MimeMessage message = createMessage(from, to, subject, content);
            Transport transport = createTransport();
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private MimeMessage createMessage(String from, String to, String subject, String content) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        message.setText(content);
        message.setSentDate(new Date());
        return message;
    }

    private Transport createTransport() throws MessagingException {
        Transport transport = session.getTransport("smtp");
        transport.connect(smtpServer, username, password);
        return transport;
    }
}
