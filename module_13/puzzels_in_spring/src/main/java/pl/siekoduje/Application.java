package pl.siekoduje;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.siekoduje.config.ApplicationProperties;
import pl.siekoduje.copy.CopyTaskExecutor;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationProperties.class.getPackageName());
        CopyTaskExecutor copyTaskExecutor = context.getBean(CopyTaskExecutor.class);
        copyTaskExecutor.start();
    }
}
