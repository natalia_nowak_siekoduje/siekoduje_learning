package pl.siekoduje.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.File;

@Getter
@Component
@PropertySource("classpath:application.properties")
public class ApplicationProperties {
//    ------------poprzednie zadanie-----------
//    private final Resource resource = new ClassPathResource("application.properties");
//    private final Properties properties = new Properties();
//    private final File target;
//    private final String senderLogin;
//    private final String senderPassword;
//    private final String receiverLogin;
//
//
//    public ApplicationProperties() throws IOException {
//        properties.load(resource.getInputStream());
//        target = new File(System.getProperty("user.dir"), (String) properties.get);
//        senderLogin = (String) properties.get("senderLogin");
//        senderPassword = (String) properties.get("senderPassword");
//        receiverLogin = (String) properties.get("receiverLogin");
//    }

    @Value("${application.name}")
    private String applicationName;

    @Value("${mail.sender.login}")
    private String senderLogin;

    @Value("${mail.sender.password}")
    private String senderPassword;

    @Value("${mail.receiver.login}")
    private String receiverLogin;

    @Value("${application.backup.source}")
    private File inputDirectory;

    @Value("${application.backup.destination}")
    private File targetDirectory;

    @Value("${mail.smtp.server}")
    private String smtpServer;

    @Value("${mail.smtp.port}")
    private String smtpPort;

    @Value("${mail.smtp.auth}")
    private boolean isSmtpAuthEnabled;

    @Value("${mail.smtp.ssl.enable}")
    private boolean isSmtpSslEnabled;


}
