package pl.siekoduje.config;

import lombok.Getter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import pl.siekoduje.copy.CopyTask;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Getter
@Configuration
@ComponentScan(basePackages = "pl.siekoduje")
public class AppConfig {


    @Bean("executor")
    public ScheduledExecutorService ScheduledExecutorService(CopyTask copyTask) {
        return Executors.newSingleThreadScheduledExecutor();

    }
}
