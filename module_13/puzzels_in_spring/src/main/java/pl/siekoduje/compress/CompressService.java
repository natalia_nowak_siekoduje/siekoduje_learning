package pl.siekoduje.compress;

import java.io.File;

public interface CompressService {
    File compress(File directory) throws Exception;
}
