package pl.siekoduje.copy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.siekoduje.compress.CompressService;
import pl.siekoduje.config.ApplicationProperties;
import pl.siekoduje.email.EmailSenderService;
import pl.siekoduje.service.MoveFileService;

import java.io.File;

@Component("copyTask")
@Slf4j
public class CopyTaskImpl implements CopyTask {

    private final EmailSenderService emailSenderService;
    private final MoveFileService moveFileService;
    private final CompressService compressService;
    private final File inputDirectory;
    private final String senderLogin;
    private final String receiverLogin;

    public CopyTaskImpl(EmailSenderService emailSenderService, MoveFileService moveFileService, CompressService compressService, ApplicationProperties applicationProperties) {
        this.emailSenderService = emailSenderService;
        this.moveFileService = moveFileService;
        this.compressService = compressService;
        this.inputDirectory = applicationProperties.getInputDirectory();
        this.receiverLogin = applicationProperties.getReceiverLogin();
        this.senderLogin = applicationProperties.getSenderLogin();
    }

    @Override
    public void run() {
        try {
            File compressedFile = compressService.compress(inputDirectory);
            File newLocation = moveFileService.move(compressedFile);
            emailSenderService.sendEmail(senderLogin, receiverLogin, "Data backed up", "content");
        } catch (Exception e) {
            log.error("Couldn't compress file ", e);
        }

    }
}
