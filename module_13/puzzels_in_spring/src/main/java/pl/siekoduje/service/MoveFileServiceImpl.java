package pl.siekoduje.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.siekoduje.config.ApplicationProperties;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Component
public class MoveFileServiceImpl implements MoveFileService {
    private final File target;
    private final ApplicationProperties applicationProperties;

    @Autowired
    public MoveFileServiceImpl(ApplicationProperties applicationProperties) throws IOException {
        this.applicationProperties = applicationProperties;
        this.target = applicationProperties.getTargetDirectory();
    }

    @Override
    public File move(File file) throws IOException, URISyntaxException {
        Path targetPath = Paths.get(target.getAbsolutePath(), file.getName());
        Files.move(file.toPath(), targetPath, StandardCopyOption.REPLACE_EXISTING);
        return targetPath.toFile();
    }
}
