package pl.siekoduje;

import org.springframework.beans.factory.annotation.Autowired;

public class HelloWorldService {
    @Autowired private HelloWorldSentence helloWorldSentence;

    public String helloWorld() {
        return helloWorldSentence.getHelloWorld();
    }
}
