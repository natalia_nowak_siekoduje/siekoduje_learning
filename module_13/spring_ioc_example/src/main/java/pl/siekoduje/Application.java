package pl.siekoduje;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
@Slf4j
public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class.getPackageName());
        HelloWorldService helloWorldService = context.getBean(HelloWorldService.class);
        log.info(helloWorldService.helloWorld());
    }
}
