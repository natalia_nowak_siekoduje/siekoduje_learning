package pl.siekoduje;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public HelloWorldService helloWorldService(){
        return new HelloWorldService();
    }

    @Bean
    public HelloWorldSentence helloWorldSentence() {
        return new HelloWorldSentence();
    }
}
