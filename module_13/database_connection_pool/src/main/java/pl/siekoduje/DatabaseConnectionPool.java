package pl.siekoduje;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


import java.util.ArrayDeque;
import java.util.Deque;
import java.util.stream.IntStream;

@Getter
@Component
public class DatabaseConnectionPool {
    private final Deque<Connection> pool = new ArrayDeque<>();
    private int poolCapacity;
    private final ApplicationContext context;


    @Autowired
    public DatabaseConnectionPool(ApplicationContext context) {
        this.context = context;
    }

    @PostConstruct
    public void init() {
        resize();
    }

    @PreDestroy
    public void close() {
        pool.stream()
                .forEach(connection -> {
                    connection.close();
                });
    }

    private void resize() {
        IntStream.range(poolCapacity, poolCapacity +5)
                .forEach(i  -> {
                    Connection connection = context.getBean(Connection.class);
                   pool.addLast(connection);
                });
        poolCapacity +=5;
    }

    public Connection acquire() {
        if(pool.isEmpty())
            resize();
        return pool.pollFirst();
    }

    public void release(Connection connection) {
        pool.push(connection);
    }

}
