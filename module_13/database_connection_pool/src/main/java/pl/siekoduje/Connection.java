package pl.siekoduje;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Slf4j
@Setter
@Getter
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Connection {

    private boolean isOpen = false;

    public void open(){
        isOpen = true;
        log.info("pl.siekoduje.Connection is open");
    }

    public void close() {
        isOpen = false;
        log.info("pl.siekoduje.Connection is closed");
    }
}
