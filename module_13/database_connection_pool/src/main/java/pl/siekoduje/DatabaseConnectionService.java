package pl.siekoduje;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.siekoduje.DatabaseConnectionPool;

@RequiredArgsConstructor
@Component
public class DatabaseConnectionService {
    private final DatabaseConnectionPool pool;

    public Connection execute() {
        return pool.acquire();
    }

}
