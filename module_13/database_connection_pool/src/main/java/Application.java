import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.siekoduje.Connection;
import pl.siekoduje.DatabaseConnectionService;

public class Application {

    public static void main(String[] args) {
         ApplicationContext context = new AnnotationConfigApplicationContext(Application.class.getPackageName());
         DatabaseConnectionService service = context.getBean(DatabaseConnectionService.class);
        Connection connection = service.execute();

    }
}
