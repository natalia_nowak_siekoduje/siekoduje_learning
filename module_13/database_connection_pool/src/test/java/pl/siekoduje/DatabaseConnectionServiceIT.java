package pl.siekoduje;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;


public class DatabaseConnectionServiceIT {

    private ApplicationContext context;
    private DatabaseConnectionService service;

    @Before
    public void setUp() {
        context = new AnnotationConfigApplicationContext(DatabaseConnectionService.class.getPackageName());
        service = context.getBean(DatabaseConnectionService.class);
    }

    @Test
    public void shouldReturnConnection() {
        //when
        Connection result = service.execute();
        //then
        assertThat(result).isInstanceOf(Connection.class);
    }


}