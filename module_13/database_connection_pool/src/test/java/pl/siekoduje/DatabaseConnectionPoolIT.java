package pl.siekoduje;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

public class DatabaseConnectionPoolIT {

    private HashSet<Connection> stuffSet;
    private ApplicationContext context;
    private DatabaseConnectionPool pool;

    @Before
    public void setUp() {
        context = new AnnotationConfigApplicationContext(DatabaseConnectionService.class.getPackageName());
        pool = context.getBean(DatabaseConnectionPool.class);
        stuffSet = new HashSet<>();
    }

    @Test
    public void shouldReturnConnection() {
        //given
        //when
        Connection result = pool.acquire();
        //then
        assertThat(result).isNotNull();
    }

    @Test
    public void shouldAcquireDifferentObjects() {
        //given
        int poolCapacity = 5;
        //when
        for (int i = 0; i < poolCapacity; i++) {
            stuffSet.add(pool.acquire());
        }
        //then
        assertThat(stuffSet.size()).isEqualTo(5);
    }

    @Test
    public void shouldResizePoolCapacity() {
        //given
        int poolCapacity = 5;
        //when
        for (int i = 0; i < poolCapacity + 1; i++) {
            stuffSet.add(pool.acquire());
        }
        //then
        assertThat(stuffSet.size()).isEqualTo(6);
    }

    @Test
    public void shouldReleaseConnection() {
        //given
        Connection expected = pool.acquire();
        //when
        pool.release(expected);
        Connection actual = pool.acquire();
        //then
        assertThat(expected).isEqualTo(actual);
    }

    @Test
    public void shouldCloseAllConnections() {
        //when
        pool.close();
        //then
        pool.getPool().stream()
                .forEach(connection -> {
                    assertThat(connection.isOpen()).isEqualTo(false);
                });
    }
}