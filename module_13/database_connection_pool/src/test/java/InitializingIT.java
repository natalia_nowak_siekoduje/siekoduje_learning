import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.siekoduje.Connection;
import pl.siekoduje.DatabaseConnectionPool;
import pl.siekoduje.DatabaseConnectionService;

import static org.assertj.core.api.Assertions.assertThat;


public class InitializingIT {
    private ApplicationContext context;

    @Before
    public void setUp() {
        context = new AnnotationConfigApplicationContext(DatabaseConnectionService.class.getPackageName());
    }

    @Test
    public void shouldCreateDatabaseConnectionService() {
        //when
        DatabaseConnectionService service = context.getBean(DatabaseConnectionService.class);
        //then
        assertThat(service).isInstanceOf(DatabaseConnectionService.class);
    }

    @Test
    public void shouldCreateDatabaseConnectionPool() {
        //when
        DatabaseConnectionPool pool = context.getBean(DatabaseConnectionPool.class);
        //then
        assertThat(pool).isInstanceOf(DatabaseConnectionPool.class);
    }

    @Test
    public void shouldCreateConnection() {
        //when
        Connection connection = context.getBean(Connection.class);
        //then
        assertThat(connection).isInstanceOf(Connection.class);
    }

    @Test
    public void shouldExistOnlyOneBeanOfTypeDatabaseConnectionService() {
        //when
        DatabaseConnectionService service1 = context.getBean(DatabaseConnectionService.class);
        DatabaseConnectionService service2 = context.getBean(DatabaseConnectionService.class);
        //then
        assertThat(service1).isEqualTo(service2);

    }

    @Test
    public void shouldExistOnlyOneBeanOfTypeDatabaseConnectionPool() {
        //when
        DatabaseConnectionPool pool1 = context.getBean(DatabaseConnectionPool.class);
        DatabaseConnectionPool pool2 = context.getBean(DatabaseConnectionPool.class);
        //then
        assertThat(pool1).isEqualTo(pool2);

    }

    @Test
    public void shouldExistManyBeansOfTypeConnection() {
        //when
        Connection connection1 = context.getBean(Connection.class);
        Connection connection2 = context.getBean(Connection.class);
        //then
        assertThat(connection1).isNotEqualTo(connection2);

    }


}