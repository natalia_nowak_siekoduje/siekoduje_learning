import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        MoveFileServiceImpl moveFileServiceImpl = new MoveFileServiceImpl(new File(System.getProperty("user.dir") ,"to-dir"));
        try {
            String module8dir = new File(System.getProperty("user.dir")).getAbsolutePath();
            Path file = Paths.get(module8dir, "from-dir","file.txt");
            moveFileServiceImpl.move(file.toFile());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
