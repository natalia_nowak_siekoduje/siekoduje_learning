import java.io.File;
import java.io.FileNotFoundException;

public interface CompressService {
    File compress(File directory) throws  Exception;
}
