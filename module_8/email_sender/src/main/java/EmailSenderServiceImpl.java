import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

public class EmailSenderServiceImpl implements EmailSenderService{
    private String username;
    private String password;
    private String smtpServer = "smtp.gmail.com";
    private String smtpPort = "465";
    private Properties properties = new Properties();
    private Session session;

    public EmailSenderServiceImpl(String username, String password) {
        this.username = username;
        this.password = password;
        properties.put("mail.smtp.host", smtpServer);
        properties.put("mail.smtp.port", smtpPort);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.enable", "true");
        session = Session.getInstance(properties, null);
        session.setDebug(true); // log additional information
    }

    @Override
    public void sendEmail(String from, String to, String subject, String content) {

        try {
            MimeMessage message = createMessage(from, to, subject, content);
            Transport transport = createTransport();
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private MimeMessage createMessage(String from, String to, String subject, String content) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSubject(subject);
        message.setText(content);
        message.setSentDate(new Date());
        return message;
    }

    private Transport createTransport() throws MessagingException {
        Transport transport = session.getTransport("smtp");
        transport.connect(smtpServer, username, password);
        return transport;
    }
}
