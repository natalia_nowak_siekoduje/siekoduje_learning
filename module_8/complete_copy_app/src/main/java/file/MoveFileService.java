package file;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;

public interface MoveFileService {
    File move(File file) throws URISyntaxException, IOException;
}
