import compress.CompressService;
import email.EmailSenderService;
import file.MoveFileService;

import java.io.File;

public class CopyTaskImpl implements CopyTask {

    private EmailSenderService emailSenderService;
    private MoveFileService moveFileService;
    private CompressService compressService;
    private File inputDirectory;
    private String emailSender;
    private String emailReceiver;

    public CopyTaskImpl(EmailSenderService emailSenderService, MoveFileService moveFileService, CompressService compressService, File inputDirectory, String emailSender, String emailReceiver) {
        this.emailSenderService = emailSenderService;
        this.moveFileService = moveFileService;
        this.compressService = compressService;
        this.inputDirectory = inputDirectory;
        this.emailReceiver = emailReceiver;
        this.emailSender = emailSender;
    }

    @Override
    public void run() {
        try {
            File compressedFile = compressService.compress(inputDirectory);
            File newLocation = moveFileService.move(compressedFile);
            emailSenderService.sendEmail(emailSender, emailReceiver, "Data backed up", "content");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
