import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class MoveFileServiceImpl implements MoveFileService {
    private File target ;

    public MoveFileServiceImpl(File target) {
        this.target = target;
    }

    @Override
    public File move(File file) throws IOException, URISyntaxException {

            Path targetPath = Paths.get(target.getAbsolutePath(), file.getName());

            Files.move(file.toPath(), targetPath, StandardCopyOption.REPLACE_EXISTING);

        return targetPath.toFile();
    }
}
