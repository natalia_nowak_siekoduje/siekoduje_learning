import compress.CompressService;
import compress.CompressServiceImpl;
import email.EmailSenderService;
import email.EmailSenderServiceImpl;
import file.MoveFileService;
import file.MoveFileServiceImpl;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Timer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Application {
    /*
    jak przeniosę mój plik, to nie mam już co przenosić. Co z tym zorbić ????
     */

    public static void main(String[] args) {
        CompressServiceImpl compressService = new CompressServiceImpl();
        EmailSenderService emailSenderService = new EmailSenderServiceImpl("kkowalski11214@gmail.com", "Qwerty1234!");
        MoveFileService moveFileService = new MoveFileServiceImpl(new File(System.getProperty("user.dir") ,"to-dir"));

        String module8dir = new File(System.getProperty("user.dir")).getAbsolutePath();
        Path file = Paths.get(module8dir, "from-dir","file.txt");
        File inputDirectory = file.toFile();
        String emailSender = "kkowalski11214@gmail.com";
        String emailReceiver = "llachows@gmail.com";

        CopyTaskImpl copyTask = new CopyTaskImpl(emailSenderService, moveFileService, compressService, inputDirectory, emailSender, emailReceiver);
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.scheduleAtFixedRate(copyTask,0, 1, TimeUnit.HOURS );

    }
}