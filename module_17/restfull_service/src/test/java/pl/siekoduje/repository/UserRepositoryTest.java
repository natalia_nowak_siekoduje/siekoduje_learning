package pl.siekoduje.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.User;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserRepositoryTest {
    private UserRepository userRepository;
    private User user;

    @BeforeEach
    public void setUp() {
        userRepository = new UserRepository();
        user = User.builder()
                .firstName("Janek")
                .lastName("Jankowski")
                .build();
    }

    @Test
    public void shouldAddUser() {
        //given
        //when
        User result = userRepository.addUser(user);
        //then
        assertThat(result).isEqualTo(user);
        assertThat(userRepository.getUsers().containsValue(user)).isTrue();
        assertThat(userRepository.getUsers().containsKey(user.getId())).isTrue();
    }

    @Test
    public void shouldReturnUser() {
        //given
        userRepository.addUser(user);
        //when
        User result = userRepository.getUser(user.getId());
        //then
        assertThat(result).isEqualTo(user);
    }

    @Test
    public void shouldNotReturnUserWhenWrongId() {
        //given
        userRepository.addUser(user);
        //when
        RuntimeException thrown = assertThrows(RuntimeException.class, () ->
                userRepository.getUser(Long.valueOf(0)));
        //then
        assertThat(thrown).isInstanceOf(RuntimeException.class);
        assertThat(thrown.getMessage()).isEqualTo("Unknown user with id: " + 0);
    }

    @Test
    public void shouldDeleteUser() {
        //given
        userRepository.addUser(user);
        //when
        Optional<User> result = userRepository.deleteUser(user.getId());
        //then
        assertThat(result.isPresent()).isTrue();
        assertThat(result.get()).isEqualTo(user);
        assertThat(userRepository.getUsers().containsKey(user.getId())).isFalse();
    }

    @Test
    public void shouldNotDeleteUserWhenWrongId() {
        //given
        userRepository.addUser(user);
        //when
        Optional<User> result = userRepository.deleteUser(Long.valueOf(0));
        //then
        assertThat(result.isEmpty()).isTrue();
        assertThat(userRepository.getUsers().containsKey(user.getId())).isTrue();
    }

}