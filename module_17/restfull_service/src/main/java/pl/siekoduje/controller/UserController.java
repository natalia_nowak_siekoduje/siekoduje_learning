package pl.siekoduje.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;

import java.net.URI;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/users")
public class UserController {

    private final UserRepository userRepository;

    //    curl -s -X GET 'http://localhost:8080/api/users/1' | json_pp
    @GetMapping("{userId}")
    public ResponseEntity<User> getUser(@PathVariable Long userId) {
        User user = userRepository.getUser(userId);
        updateWithSelfLink(user);
        return ResponseEntity.ok(user);
    }

    //    curl -D - -s -X POST -H "Content-Type: application/json" -d '{"firstName":"John","lastName":"Smith"}' 'http://localhost:8080/api/users' | json_pp
    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody User user) {
        User addedUser = userRepository.addUser(user);
        updateWithSelfLink(addedUser);
        return ResponseEntity.created(URI.create("api/users/" + addedUser.getId())).body(addedUser);

    }

    //    curl -s -X DELETE 'http://localhost:8080/api/users/1'
    @DeleteMapping("{userId}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId) {
        if (userRepository.deleteUser(userId).isPresent()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    private User updateWithSelfLink(User user) {
        Link link = linkTo(methodOn(UserController.class).getUser(user.getId())).withSelfRel();
        user.add(link);
        return user;
    }

}
