package pl.siekoduje.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.model.Order;
import pl.siekoduje.service.OrderService;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RequiredArgsConstructor
@RestController
@RequestMapping("api/orders")
public class OrderController {
    private final OrderService orderService;

    //    $ curl -s -X GET 'http://localhost:8080/api/orders/1'| json_pp
    @GetMapping("{orderId}")
    @ResponseStatus(HttpStatus.OK)
    public Order getOrder(@PathVariable Long orderId) {
        Order order = orderService.getOrder(orderId);
        updateWithSelfLink(order);
        return order;
    }

    //    $ curl -s -X GET 'http://localhost:8080/api/orders'| json_pp
    @GetMapping
    public CollectionModel<Order> getOrders() {
        List<Order> orders = orderService.getOrders();
        for (Order order : orders) {
            Link selfLink = linkTo(methodOn(OrderController.class).getOrder(order.getId())).withSelfRel();
            order.add(selfLink);
        }
        Link link = linkTo(methodOn(OrderController.class).getOrders()).withSelfRel();

        return CollectionModel.of(orders, link);
    }
//    curl -s -X GET 'http://localhost:8080/api/orders/users?userId=1'| json_pp
    @GetMapping("users")
    public CollectionModel<Order> getOrders(@RequestParam Long userId) {
            List<Order> orders = orderService.getOrders(userId);
            for (Order order : orders) {
                Link selfLink = linkTo(methodOn(OrderController.class).getOrder(order.getId())).withSelfRel();
                order.add(selfLink);
            }
            Link link = linkTo(methodOn(OrderController.class).getOrders(userId)).withSelfRel();
            return CollectionModel.of(orders, link);
    }

    //    curl -s -X GET 'http://localhost:8080/api/orders/1/items'| json_pp
    @GetMapping("{orderId}/items")
    public CollectionModel<String> getItems(@PathVariable Long orderId) {
        Order order = orderService.getOrder(orderId);
        List<String> items = order.getItems();
        Link link = linkTo(methodOn(OrderController.class).getItems(orderId)).withSelfRel();
        return CollectionModel.of(items, link);
    }

//    curl -D - -s -X POST -H "Content-Type: application/json" -d '{"items":["item1", "item2"], "orderAmount":10, "state":"UNPAID"}' 'http://localhost:8080/api/orders/users/2'
 @PostMapping("/users/{userId}")
    public ResponseEntity<Order> addOrder(@PathVariable Long userId,
                                          @RequestBody Order order) {
        Optional<Order> optionalOrder = orderService.addOrder(order, userId);
        if (optionalOrder.isPresent()) {
            Order createdOrder = optionalOrder.get();
            updateWithSelfLink(createdOrder);
            return ResponseEntity.created(URI.create("api/orders/" + createdOrder.getId() + "/users/" + userId)).body(updateWithSelfLink(order));
        }
        return ResponseEntity.notFound().build();
    }

    //    $ curl -s -X PUT -H "Content-Type: application/json" -d '{"items":["item10", "item20"], "orderAmount":10, "state":"UNPAID", "id":"1"}' 'http://localhost:8080/api/orders'| json_pp
    @PutMapping
    public ResponseEntity<Order> updateOrder(@RequestBody Order updatedOrder) {
            orderService.updateOrder(updatedOrder);
            return ResponseEntity.status(HttpStatus.CREATED).body(updateWithSelfLink(orderService.getOrder(updatedOrder.getId())));
    }

    //    curl -s -X DELETE -H "Content-Type: application/json" -d '{"items":["item10", "item20"], "orderAmount":10, "state":"UNPAID", "id":"1"}' 'http://localhost:8080/api/orders'| json_pp
    @DeleteMapping
    public ResponseEntity<Void> deleteOrder(@RequestBody Order order) {
        if (orderService.deleteOrder(order)) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    private Order updateWithSelfLink(Order order) {
        Link link = linkTo(methodOn(OrderController.class).getOrder(order.getId())).withSelfRel();
        order.add(link);
        return order;
    }
}
