package pl.siekoduje.model;

public enum OrderState {
    UNPAID,
    DELIVERED,
    PAID
}
