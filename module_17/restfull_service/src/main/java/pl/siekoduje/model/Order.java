package pl.siekoduje.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;


@Getter
@Setter
@Builder
@EqualsAndHashCode(callSuper=false)
public class Order extends RepresentationModel<Order> {
    private Long id;
    private List<String> items;
    private double orderAmount;
    private OrderState state;
}
