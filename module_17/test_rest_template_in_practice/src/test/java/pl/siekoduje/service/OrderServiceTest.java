package pl.siekoduje.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.OrderState;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OrderServiceTest {

    private UserRepository repositoryMock;
    private OrderService service;
    private Order order;
    private User user;

    @BeforeEach
    public void setUp() {
        repositoryMock = mock(UserRepository.class);
        service = new OrderService(repositoryMock);
        order = Order.builder()
                .orderAmount(20.20)
                .state(OrderState.UNPAID)
                .items(new ArrayList<>())
                .build();
        user = User.builder()
                .firstName("Janek")
                .lastName("Jankowski")
                .build();

        when(repositoryMock.getUser((Long.valueOf(1)))).thenReturn(user);
    }

    @Test
    public void shouldAddOrder() {
        //given
        //when
        Optional<Order> result = service.addOrder(order, user.getId());
        //then
        assertThat(result).isPresent();
        assertThat(service.getOrders()).contains(order);
    }

    @Test
    public void shouldNotAddOrderWhenWrongUserId() {
        //given
        //when
        when(repositoryMock.getUser(Long.valueOf(-1))).thenThrow(new RuntimeException("Unknown user with id: -1"));
        Optional<Order> result = service.addOrder(order, Long.valueOf(-1));
        //then
        assertThat(result).isEmpty();
        assertThat(service.getOrders().contains(order)).isFalse();
    }

    @Test
    public void shouldReturnEmptyListWhenThereAreNoOrders() {
        //given
        //when
        //then
        assertThat(service.getOrders()).isEmpty();
    }

    @Test
    public void shouldReturnAllOrders() {
        //given

        service.addOrder(order, user.getId());
        //when
        List<Order> result = service.getOrders();
        //then
        assertThat(result).contains(order);
    }

    @Test
    public void shouldReturnUserOrders() {
        //given
        Order secondOrder = Order.builder()
                .orderAmount(30.30)
                .state(OrderState.UNPAID)
                .build();
        service.addOrder(order, user.getId());
        service.addOrder(secondOrder, user.getId());
        //when
        List<Order> result = service.getOrders(user.getId());
        //then
        assertThat(result.size()).isEqualTo(2);
        assertThat(result).contains(order, secondOrder);
    }

    @Test
    public void shouldNotReturnUserOrdersWhenWrongUserId() {
        //given
        service.addOrder(order, user.getId());
        //when
        RuntimeException thrown =
                assertThrows(RuntimeException.class, () ->
                        service.getOrders(Long.valueOf(-1)));
        //then
        assertThat(thrown.getMessage()).isEqualTo("Unknown user with id: -1");
    }

    @Test
    public void shouldReturnOrder() {
        //given
        service.addOrder(order, user.getId());
        //when
        Order result = service.getOrder(this.order.getId());
        //then
        assertThat(result).isEqualTo(order);
    }

    @Test
    public void shouldNotReturnOrderWhenWrongOrderId() {
        //given
        service.addOrder(order, user.getId());
        //when
        RuntimeException thrown =
                assertThrows(RuntimeException.class, () -> {
                    service.getOrder(Long.valueOf(-1));
                });

        //then
        assertThat(thrown).isInstanceOf(RuntimeException.class);
    }

    @Test
    public void shouldUpdateOrder() {
        //given
        service.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order updatedOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.UNPAID)
                .items(newItems)
                .build();
        //when
        service.updateOrder(updatedOrder);
        //then
        assertThat(service.getOrder(updatedOrder.getId()).getOrderAmount()).isEqualTo(newAmount);
        assertThat(service.getOrder(updatedOrder.getId()).getItems()).isEqualTo(newItems);
        assertThat(service.getOrder(updatedOrder.getId()).getState()).isEqualTo(updatedOrder.getState());
    }

    @Test
    public void shouldNotUpdateOrderWhenWrongOrderId() {
        //given
        service.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order updatedOrder = Order.builder()
                .id(Long.valueOf(-1))
                .orderAmount(newAmount)
                .state(OrderState.UNPAID)
                .items(newItems)
                .build();
        //when
        RuntimeException thrown =
                assertThrows(RuntimeException.class, () -> {
                    service.updateOrder(updatedOrder);
                });

        //then
        assertThat(thrown.getMessage()).isEqualTo("Unknown order with id: -1");
        Order unchangedOrder = service.getOrder(order.getId());
        assertThat(unchangedOrder.getOrderAmount()).isEqualTo(order.getOrderAmount());
        assertThat(unchangedOrder.getItems()).isEqualTo(order.getItems());
        assertThat(unchangedOrder.getState()).isEqualTo(OrderState.UNPAID);
    }

    @Test
    public void shouldDeleteOrder() {
        //given
        service.addOrder(order, user.getId());
        //when
        boolean result = service.deleteOrder(order);
        //then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldNotDeleteOrderWhenWrongOrderId() {
        //given
        service.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order wrongOrder = Order.builder()
                .id(order.getId() + 1)
                .orderAmount(newAmount)
                .state(OrderState.UNPAID)
                .items(newItems)
                .build();
        //when
        boolean result = service.deleteOrder(wrongOrder);
        //then
        assertThat(result).isFalse();
        assertThat(service.getOrder(order.getId())).isEqualTo(order);
    }

}