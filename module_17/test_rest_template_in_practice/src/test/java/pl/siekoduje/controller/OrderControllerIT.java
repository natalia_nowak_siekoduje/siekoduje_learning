package pl.siekoduje.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.OrderState;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;
import pl.siekoduje.service.OrderService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderControllerIT {
    @Autowired
    OrderService orderService;
    @Autowired
    UserRepository userRepository;
    private User user;
    private Order order;
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;
    private HttpHeaders headers;

    @BeforeEach
    public void setUp() {
        headers = new HttpHeaders();
        String firstName = "Jan";
        String lastName = "Kowalski";
        user = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build();
        order = Order.builder()
                .orderAmount(20.20)
                .state(OrderState.UNPAID)
                .items(Arrays.asList("q", "1"))
                .build();
        userRepository.addUser(user);
        headers.setContentType(MediaType.APPLICATION_JSON);
    }

    @DirtiesContext
    @Test
    public void shouldReturnOrder() {
        //given
        orderService.addOrder(order, user.getId());
        //when
        ResponseEntity<Order> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/" + order.getId(), Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getBody())
                .isEqualTo(order);
    }

    @DirtiesContext
    @Test
    public void shouldNotReturnOrderWhenWrongOrderId() {
        //given
        orderService.addOrder(order, user.getId());
        //when
        ResponseEntity<Order> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/" + Long.valueOf(-1), Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldAddOrder() {
        //given
        HttpEntity<Order> request = new HttpEntity<>(order, headers);
        //when
        ResponseEntity<Order> response = this.restTemplate.postForEntity("http://localhost:" + port + "/api/orders/users/" + user.getId(), request, Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody())
                .isNotNull();
        Order createdOrder = response.getBody();
        assertThat(createdOrder.getState()).isEqualTo(OrderState.UNPAID);
        assertThat(createdOrder.getOrderAmount()).isEqualTo(20.20);
    }

    @DirtiesContext
    @Test
    public void shouldUpdateOrder() {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order updatedOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Order> request = new HttpEntity<>(updatedOrder, headers);
        //when
        ResponseEntity<Order> response = this.restTemplate.exchange("http://localhost:" + port + "/api/orders", HttpMethod.PUT, request, Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getBody())
                .isNotNull();
        Order returnedOrder = response.getBody();
        assertThat(returnedOrder.getState()).isEqualTo(OrderState.PAID);
        assertThat(returnedOrder.getOrderAmount()).isEqualTo(100);
    }

    @DirtiesContext
    @Test
    public void shouldNotUpdateOrderWhenWrongOrderId() {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order updatedOrder = Order.builder()
                .id(Long.valueOf(-1))
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Order> request = new HttpEntity<>(updatedOrder, headers);
        //when
        ResponseEntity<Order> response = this.restTemplate.exchange("http://localhost:" + port + "/api/orders", HttpMethod.PUT, request, Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @DirtiesContext
    @Test
    public void shouldDeleteOrder() {
        //given
        orderService.addOrder(order, user.getId());
        HttpEntity<Order> request = new HttpEntity<>(order, headers);
        //when
        ResponseEntity<Order> response = this.restTemplate.exchange("http://localhost:" + port + "/api/orders", HttpMethod.DELETE, request, Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isNull();
    }

    @DirtiesContext
    @Test
    public void shouldNotDeleteOrderWhenWrongOrderId() {
        //given
        orderService.addOrder(order, user.getId());
        Order wrongOrder = Order.builder()
                .id(Long.valueOf(-1))
                .orderAmount(20.2)
                .state(OrderState.PAID)
                .items(Arrays.asList("a", "b"))
                .build();
        HttpEntity<Order> request = new HttpEntity<>(wrongOrder, headers);
        //when
        ResponseEntity<Order> response = this.restTemplate.exchange("http://localhost:" + port + "/api/orders", HttpMethod.DELETE, request, Order.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @DirtiesContext
    @Test
    public void shouldReturnAllOrders() {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order secondOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        orderService.addOrder(secondOrder, user.getId());
        //when
        ResponseEntity<LinkedHashMap> response = this.restTemplate.exchange("http://localhost:" + port + "/api/orders",
                HttpMethod.GET, null,
                LinkedHashMap.class);
        //then
        assertThat(response).isNotNull();

        LinkedHashMap _embedded = (LinkedHashMap) response.getBody().get("_embedded");
        ArrayList<LinkedHashMap> orderList = (ArrayList<LinkedHashMap>) _embedded.get("orderList");
        assertThat(orderList.size()).isEqualTo(2);
        LinkedHashMap<String, Object> order1 = orderList.get(0);
        LinkedHashMap<String, Object> order2 = orderList.get(1);
        Double orderAmount1 = (Double) order1.get("orderAmount");
        Double orderAmount2 = (Double) order2.get("orderAmount");
        assertThat(orderAmount1).isEqualTo(20.20);
        assertThat(orderAmount2).isEqualTo(100);
    }

    @DirtiesContext
    @Test
    public void shouldReturnUserOrders() {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order secondOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        orderService.addOrder(secondOrder, user.getId());

        String firstName = "Kasia";
        String lastName = "Kowalska";
        User secondUser = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build();
        userRepository.addUser(secondUser);
        orderService.addOrder(secondOrder, secondUser.getId());
        //when
        ResponseEntity<LinkedHashMap> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/users?userId=" + user.getId(), LinkedHashMap.class);
        //then
        assertThat(response).isNotNull();
        LinkedHashMap _embedded = (LinkedHashMap) response.getBody().get("_embedded");
        ArrayList<LinkedHashMap> orderList = (ArrayList<LinkedHashMap>) _embedded.get("orderList");

        assertThat(orderList.size()).isEqualTo(2);
        LinkedHashMap<String, Object> order1 = orderList.get(0);
        LinkedHashMap<String, Object> order2 = orderList.get(1);
        Double orderAmount1 = (Double) order1.get("orderAmount");
        Double orderAmount2 = (Double) order2.get("orderAmount");
        assertThat(orderAmount1).isEqualTo(20.20);
        assertThat(orderAmount2).isEqualTo(100);
    }

    @DirtiesContext
    @Test
    public void shouldNotReturnUserOrdersWhenWrongUserId() {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order secondOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        orderService.addOrder(secondOrder, user.getId());

        //when
        ResponseEntity<LinkedHashMap> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/users?userId=" + Long.valueOf(-1), LinkedHashMap.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @DirtiesContext
    @Test
    public void shouldReturnOrderItems() {
        //given
        orderService.addOrder(order, user.getId());
        //when
        ResponseEntity<LinkedHashMap> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/" + order.getId() + "/items", LinkedHashMap.class);
        //then
        assertThat(response).isNotNull();
        LinkedHashMap _embedded = (LinkedHashMap) response.getBody().get("_embedded");
        ArrayList<String> orderList = (ArrayList<String>) _embedded.get("stringList");
        assertThat(orderList.size()).isEqualTo(2);
        String item1 = orderList.get(0);
        String item2 = orderList.get(1);
        assertThat(item1).isEqualTo("q");
        assertThat(item2).isEqualTo("1");

    }

    @DirtiesContext
    @Test
    public void shouldNotReturnOrderItemsWhenWrongOrderId() {
        //given
        orderService.addOrder(order, user.getId());
        //when
        ResponseEntity<LinkedHashMap> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/orders/" + Long.valueOf(-1) + "/items", LinkedHashMap.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);


    }

}