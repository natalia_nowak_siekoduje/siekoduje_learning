package pl.siekoduje.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerIT {
    private static User user;
    @Autowired
    UserRepository userRepository;
    @LocalServerPort
    private int port;
    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeAll
    public static void setUp() {
        String firstName = "Jan";
        String lastName = "Kowalski";
        user = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }


    @Test
    public void shouldCreateUser() {
        //given
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<User> request = new HttpEntity<>(user, headers);
        // when
        ResponseEntity<User> response = this.restTemplate.postForEntity("http://localhost:" + port + "/api/users", request, User.class);
        // then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody())
                .isNotNull()
                .isEqualTo(user);
    }

    @Test
    public void shouldReturnUser() {
        //given
        userRepository.addUser(user);
        Long userId = user.getId();
        //when
        ResponseEntity<User> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/users/" + userId, User.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody())
                .isNotNull()
                .isEqualTo(user);
    }

    @Test
    public void shouldNotReturnUserWhenWrongUserId() {
        //given
        //when
        ResponseEntity<User> response = this.restTemplate.getForEntity("http://localhost:" + port + "/api/users/" + Long.valueOf(-1), User.class);
        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void shouldDeleteUser() {
        //given
        Long userId = user.getId();
        //when
        ResponseEntity<User> response = this.restTemplate.exchange("http://localhost:" + port + "/api/users/" + userId, HttpMethod.DELETE, null, User.class);
        //then
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(response.getBody()).isNull();
    }

    @Test
    public void shouldNotDeleteUserWhenWrongUserId() {
        //given
        //when
        ResponseEntity<User> response = this.restTemplate.exchange("http://localhost:" + port + "/api/users/" + Long.valueOf(-1), HttpMethod.DELETE, null, User.class);
        //then
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

}