package pl.siekoduje.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;


@Getter
@Builder
@Setter
public class User extends RepresentationModel<User> {
    private final String firstName;
    private final String lastName;
    private Long id;
}
