package pl.siekoduje.repository;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.siekoduje.model.User;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@RequiredArgsConstructor
@Getter
@Repository
public class UserRepository {
    private final ConcurrentHashMap<Long, User> users = new ConcurrentHashMap<>();
    private final AtomicLong userIdCounter = new AtomicLong();

    public User addUser(User user) {
        if (users.contains(user))
            return user;
        user.setId(userIdCounter.incrementAndGet());
        users.put(user.getId(), user);
        return users.get(user.getId());
    }

    public User getUser(Long userId) {
        return findUser(userId)
                .map(this::createCopy)
                .orElseThrow(
                        () -> new RuntimeException("Unknown user with id: " + userId)
                );
    }

    public Optional<User> deleteUser(Long userId) {
        return Optional.ofNullable(users.remove(userId));
    }

    private Optional<User> findUser(Long userId) {
        return Optional.ofNullable(users.get(userId));
    }

    private User createCopy(User user) {
        return User.builder()
                .id(user.getId())
                .lastName(user.getLastName())
                .firstName(user.getFirstName())
                .build();
    }
}
