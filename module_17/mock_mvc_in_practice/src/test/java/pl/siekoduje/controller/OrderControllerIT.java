package pl.siekoduje.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.OrderState;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;
import pl.siekoduje.service.OrderService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class OrderControllerIT {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserRepository userRepository;
    private User user;
    private Order order;


    @BeforeEach
    public void setUp() {
        String firstName = "Jan";
        String lastName = "Kowalski";
        user = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build();
        order = Order.builder()
                .orderAmount(20.20)
                .state(OrderState.UNPAID)
                .items(Arrays.asList("q", "1"))
                .build();
        userRepository.addUser(user);
    }

    @DirtiesContext
    @Test
    public void shouldReturnOrder() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        //when
        this.mockMvc.perform(get("/api/orders/" + order.getId()))
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().json(
                        "{" +
                                "\"id\":1, " +
                                "\"items\":[\"q\",\"1\"], " +
                                "\"orderAmount\": 20.2, " +
                                "\"state\":\"UNPAID\"}" +
                                "}"
                ));
    }

    @DirtiesContext
    @Test
    public void shouldNotReturnOrderWhenWrongOrderId() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        //when
        this.mockMvc.perform(get("/api/orders/" + -1L))
                //then
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @DirtiesContext
    @Test
    public void shouldAddOrder() throws Exception {

        //given
        String request = "{" +
                "\"id\":1, " +
                "\"items\":[\"q\",\"1\"], " +
                "\"orderAmount\": 20.2, " +
                "\"state\":\"UNPAID\"}" +
                "}";
        this.mockMvc.perform(post("/api/orders/users/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))

                //then
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().json(
                        "{" +
                                "\"id\":1, " +
                                "\"items\":[\"q\",\"1\"], " +
                                "\"orderAmount\": 20.2, " +
                                "\"state\":\"UNPAID\"}" +
                                "}"
                ));
    }

    @DirtiesContext
    @Test
    public void shouldUpdateOrder() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        String request = "{" +
                "\"id\":1, " +
                "\"items\":[\"a\",\"b\"], " +
                "\"orderAmount\": 100, " +
                "\"state\":\"PAID\"}" +
                "}";
        this.mockMvc.perform(put("/api/orders/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                //then
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().json(
                        "{" +
                                "\"id\":1, " +
                                "\"items\":[\"a\",\"b\"], " +
                                "\"orderAmount\": 100, " +
                                "\"state\":\"PAID\"}" +
                                "}"
                ));
    }

    @DirtiesContext
    @Test
    public void shouldNotUpdateOrderWhenWrongOrderId() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        String request = "{" +
                "\"id\":-1, " +
                "\"items\":[\"a\",\"b\"], " +
                "\"orderAmount\": 100, " +
                "\"state\":\"PAID\"}" +
                "}";
        //when
        this.mockMvc.perform(put("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                //then
                .andDo(print())
                .andExpect(status().isNotFound());
    }


    @DirtiesContext
    @Test
    public void shouldDeleteOrder() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        String request = "{" +
                "\"id\":1, " +
                "\"items\":[\"q\",\"1\"], " +
                "\"orderAmount\": 20.2, " +
                "\"state\":\"UNPAID\"}" +
                "}";
        //when
        this.mockMvc.perform(delete("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                //then
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @DirtiesContext
    @Test
    public void shouldNotDeleteOrderWhenWrongOrderId() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        String request = "{" +
                "\"id\":-1, " +
                "\"items\":[\"q\",\"1\"], " +
                "\"orderAmount\": 20.2, " +
                "\"state\":\"UNPAID\"}" +
                "}";
        //when
        this.mockMvc.perform(delete("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                //then
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @DirtiesContext
    @Test
    public void shouldReturnAllOrders() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order secondOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        orderService.addOrder(secondOrder, user.getId());
        //when
        this.mockMvc.perform(get("/api/orders"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.orderList[0].id", is(1)))
                .andExpect(jsonPath("$._embedded.orderList[0].orderAmount", is(20.2)))
                .andExpect(jsonPath("$._embedded.orderList[0].state", is("UNPAID")))
                .andExpect(jsonPath("$._embedded.orderList[0].items[0]", is("q")))
                .andExpect(jsonPath("$._embedded.orderList[0].items[1]", is("1")))
                .andExpect(jsonPath("$._embedded.orderList[1].id", is(2)))
                .andExpect(jsonPath("$._embedded.orderList[1].orderAmount", is(100.0)))
                .andExpect(jsonPath("$._embedded.orderList[1].state", is("PAID")))
                .andExpect(jsonPath("$._embedded.orderList[1].items[0]", is("a")))
                .andExpect(jsonPath("$._embedded.orderList[1].items[1]", is("b")));
    }


    @DirtiesContext
    @Test
    public void shouldReturnUserOrders() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order secondOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        orderService.addOrder(secondOrder, user.getId());

        String firstName = "Kasia";
        String lastName = "Kowalska";
        User secondUser = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build();
        userRepository.addUser(secondUser);
        orderService.addOrder(secondOrder, secondUser.getId());
        //when
        this.mockMvc.perform(get("/api/orders/users?userId=" + user.getId()))
                //then
                .andDo(print())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.orderList[0].id", is(1)))
                .andExpect(jsonPath("$._embedded.orderList[0].orderAmount", is(20.2)));
    }

    @DirtiesContext
    @Test
    public void shouldNotReturnUserOrdersWhenWrongUserId() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        double newAmount = 100;
        List<String> newItems = Arrays.asList("a", "b");
        Order secondOrder = Order.builder()
                .id(order.getId())
                .orderAmount(newAmount)
                .state(OrderState.PAID)
                .items(newItems)
                .build();
        orderService.addOrder(secondOrder, user.getId());
        //when
        this.mockMvc.perform(get("/api/orders/users?userId=" + -1L))
                //then
                .andDo(print())
                .andExpect(status().isNotFound());

    }

    @DirtiesContext
    @Test
    public void shouldReturnOrderItems() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        //when
        this.mockMvc.perform(get("/api/orders/" + order.getId() + "/items"))
                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(jsonPath("$._embedded.stringList[0]", is("q")))
                .andExpect(jsonPath("$._embedded.stringList[1]", is("1")));

    }

    @DirtiesContext
    @Test
    public void shouldNotReturnOrderItemsWhenWrongOrderId() throws Exception {
        //given
        orderService.addOrder(order, user.getId());
        //when
        this.mockMvc.perform(get("/api/orders/" + -1L + "/items"))
                //then
                .andDo(print())
                .andExpect(status().isNotFound());
    }

}