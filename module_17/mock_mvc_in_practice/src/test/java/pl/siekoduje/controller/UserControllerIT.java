package pl.siekoduje.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIT {
    private static User user;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MockMvc mockMvc;

    @BeforeAll
    public static void setUp() {
        String firstName = "Jan";
        String lastName = "Kowalski";
        user = User.builder()
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }

    @DirtiesContext
    @Test
    public void shouldCreateUser() throws Exception {
        //given
        String request = "{" +
                "  \"firstName\": \"Jan\"," +
                "  \"lastName\": \"Kowalski\"" +
                "}";
        // when
        this.mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(request))
                // then
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().json(
                        "{" +
                                "\"firstName\":\"Jan\"," +
                                "\"lastName\":\"Kowalski\"," +
                                "\"id\":1" +
                                "}"
                ));
    }

    @DirtiesContext
    @Test
    public void shouldReturnUser() throws Exception {
        //given
        userRepository.addUser(user);
        Long userId = user.getId();
        //when
        this.mockMvc.perform(get("/api/users/" + userId))
                // then
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/hal+json"))
                .andExpect(content().json(
                        "{" +
                                "   \"firstName\": \"Jan\", " +
                                "   \"lastName\": \"Kowalski\", " +
                                "   \"id\": 1" +
                                "}"
                ));
    }

    @DirtiesContext
    @Test
    public void shouldNotReturnUserWhenWrongUserId() throws Exception {
        //given
        userRepository.addUser(user);
        //when
        this.mockMvc.perform(get("/api/users/" + Long.valueOf(-1)))
                // then
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @DirtiesContext
    @Test
    public void shouldDeleteUser() throws Exception {
        //given
        userRepository.addUser(user);
        //when
        this.mockMvc.perform(delete("/api/users/" + user.getId()))
                // then
                .andDo(print())
                .andExpect(status().isNoContent());

    }

    @DirtiesContext
    @Test
    public void shouldNotDeleteUserWhenWrongUserId() throws Exception {
        //given
        userRepository.addUser(user);
        //when
        this.mockMvc.perform(delete("/api/users/" + Long.valueOf(-1)))
                // then
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}