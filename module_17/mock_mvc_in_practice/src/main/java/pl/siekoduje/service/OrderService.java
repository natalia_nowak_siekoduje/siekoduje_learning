package pl.siekoduje.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.siekoduje.exceptions.OrderNotFoundException;
import pl.siekoduje.exceptions.UserNotFoundException;
import pl.siekoduje.model.Order;
import pl.siekoduje.repository.UserRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Getter
@Service
public class OrderService {
    private final UserRepository userRepository;
    private final Map<Long, List<Order>> orders = new HashMap<>();
    private final AtomicLong orderIdCounter = new AtomicLong();

    public Optional<Order> addOrder(Order order, Long userId) {
        try {
            userRepository.getUser(userId);
            order.setId(orderIdCounter.incrementAndGet());
            List<Order> list = orders.getOrDefault(userId, new ArrayList<>());
            list.add(order);
            orders.put(userId, list);
            return Optional.ofNullable(order);
        } catch (RuntimeException e) {
            return Optional.empty();
        }
    }

    public List<Order> getOrders() {
        return orders.values()
                .stream()
                .flatMap(Collection::stream)
                .map(this::createCopy)
                .collect(Collectors.toList());
    }

    public List<Order> getOrders(Long userId) {
        Optional<List<Order>> orders = findOrders(userId);
        if (orders.isPresent()) {
            return orders.get()
                    .stream()
                    .map(this::createCopy)
                    .collect(Collectors.toList());
        }
        throw new UserNotFoundException("Unknown user with id: " + userId);

    }

    public Order getOrder(Long orderId) {
        return findOrder(orderId)
                .map(this::createCopy)
                .orElseThrow(
                        () ->
                                new OrderNotFoundException("Unknown order with id: " + orderId)
                );

    }

    private Optional<List<Order>> findOrders(Long userId) {
        return Optional.ofNullable(orders.get(userId));

    }

    private Optional<Order> findOrder(Long orderId) {
        return orders.values()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
                .stream()
                .filter(order -> order.getId() == orderId)
                .findFirst();

    }

    public void updateOrder(Order order) {
        findOrder(order.getId())
                .map(o ->
                {
                    o.setItems(order.getItems());
                    o.setOrderAmount(order.getOrderAmount());
                    o.setState(order.getState());
                    return o;
                })
                .map(this::createCopy)
                .orElseThrow(
                        () ->
                                new OrderNotFoundException("Unknown order with id: " + order.getId())
                );
    }

    public boolean deleteOrder(Order order) {
        return orders.values()
                .stream()
                .map(l -> l.remove(order))
                .reduce((b1, b2) -> b1 || b2)
                .orElse(false);
    }

    private Order createCopy(Order order) {
        return Order.builder()
                .state(order.getState())
                .items(order.getItems())
                .id(order.getId())
                .orderAmount(order.getOrderAmount())
                .build();
    }
}
