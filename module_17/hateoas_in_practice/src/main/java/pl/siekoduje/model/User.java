package pl.siekoduje.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.hateoas.RepresentationModel;


@Getter
@Setter
@ToString
@EqualsAndHashCode
@Builder
public class User extends RepresentationModel<User> {
    private String firstName;
    private String lastName;
    private  Long id;
}
