package pl.siekoduje.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.siekoduje.model.User;
import pl.siekoduje.repository.UserRepository;

import java.util.Collection;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Slf4j
@RestController
@RequestMapping("api/users")
public class UserController {

    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    public CollectionModel<User> getUsers() {
        Collection<User> users = userRepository.getUsers().values();
        for (User user : users) {
            Link selfLink =
                    linkTo(
                            methodOn(UserController.class)
                                    .getUser(user.getId())).withSelfRel();
            user.add(selfLink);
        }
        Link selfLink = linkTo(methodOn(UserController.class).getUsers()).withSelfRel();
        return CollectionModel.of(users, selfLink);
    }


    @GetMapping("{userId}")
    public User getUser(@RequestParam Long userId) {
        User user = userRepository.getUser(userId);
        Link selfLink = linkTo(methodOn(UserController.class).getUser(userId)).withSelfRel();
        user.add(selfLink);
        return user;
    }

    @PostMapping
    public User createUser(RequestEntity<User> userRequestEntity) {
        User newUser = userRepository.addUser(userRequestEntity.getBody());
        Link selfLink = linkTo(methodOn(UserController.class).createUser(userRequestEntity)).withSelfRel();
        newUser.add(selfLink);
        return newUser;
    }

    @PutMapping("{userId}")
    public void updateUser(@PathVariable Long userId, RequestEntity<User> requestEntity) {
        log.info(requestEntity.getBody().toString());
    }

    @DeleteMapping("{userId}")
    public void deleteUser(@PathVariable Long userId) {
        User deletedUser = userRepository.deleteUser(userId);
    }

}
