package pl.siekoduje.repository;

import lombok.Getter;
import org.springframework.stereotype.Component;
import pl.siekoduje.model.User;

import java.util.HashMap;
import java.util.NoSuchElementException;


@Component
@Getter
public class UserRepository {

    private final HashMap<Long, User> users = new HashMap<>();

    public User getUser(Long userId) {
        if (users.containsKey(userId))
            return users.get(userId);
        throw new NoSuchElementException("User with id:" + userId + " doesn't exist");
    }

    public User deleteUser(Long userId) {
        if (users.containsKey(userId)) {
            User deletedUser = users.get(userId);
            users.remove(userId);
            return deletedUser;
        } else {
            throw new NoSuchElementException("User with id:" + userId + " doesn't exist");
        }
    }

    public User addUser(User user ) {
        long userId = users.size() > 0 ? users.size() : 1;
        user.setId(userId);
        users.put(userId, user);
        return user;
    }
}
