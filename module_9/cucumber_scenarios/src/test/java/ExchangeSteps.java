


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.catchThrowable;
public class ExchangeSteps {

    private InMemoryCurrencyProvider provider =  new InMemoryCurrencyProvider();
    private Currency from;
    private Currency to;
    private Double result;
    private Throwable thrown;

    @Given("^I have (\\w{3}) and I want (\\w{3})$")
        public void initializeCurrencyProvider(String fromCurrency, String toCurrency) {
            from = Currency.valueOf(fromCurrency);
            to = Currency.valueOf(toCurrency);

    }

    @Given("^I have null and I want (\\w{3})$")
    public void initializeCurrencyProviderWithNullFromCurrency( String toCurrency) {
        from = null;
        to = Currency.valueOf(toCurrency);

    }

    @Given("^I have (\\w{3}) and I want null$")
    public void initializeCurrencyProviderWithNullToCurrency(String fromCurrency) {
        from = Currency.valueOf(fromCurrency);
        to = null;

    }

    @When("^I exchange them$")
    public void exchangeMoney() {
        result = provider.getExchangeRate(from, to);
    }

    @When("^I try to exchange them$")
    public void exchangeMoneyExceptions() {
        thrown =
                catchThrowable(
                        () -> {
                            provider.getExchangeRate(from, to);
                        }
                );
    }

    @Then("^Exchange rate is ([0-9]*\\.?[0-9]*)$")
    public void validateResult(String exchangingRate) {
        Double rate = Double.valueOf(exchangingRate);
        assertEquals(rate, result);
    }

    @Then("^I get message: (.+)$")
    public void validateResultForExceptions(String messageException) {
        assertEquals(messageException, thrown.getMessage());
    }
}
