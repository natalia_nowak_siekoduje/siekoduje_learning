
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features/moneyExchangeExceptionsTests.feature",
                "classpath:features/moneyExchangeTests.feature"},
        glue = {""}
)
public class ExchangeTest {
}
