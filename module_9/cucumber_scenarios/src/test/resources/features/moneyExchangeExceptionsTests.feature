Feature: money conversion exceptions

  Scenario: As a user I want to exchange HUF to PLN
    Given I have HUF and I want PLN
    When I try to exchange them
    Then I get message: Unsuported source currency: HUF

  Scenario: As a user I want to exchange PLN to HUF
    Given I have PLN and I want HUF
    When I try to exchange them
    Then I get message: Unsuported destination currency: HUF

  Scenario: As a user I want to exchange null to HUF
    Given I have null and I want PLN
    When I try to exchange them
    Then I get message: Parameter "from" cannot be null

  Scenario: As a user I want to exchange PLN to null
    Given I have PLN and I want null
    When I try to exchange them
    Then I get message: Parameter "to" cannot be null