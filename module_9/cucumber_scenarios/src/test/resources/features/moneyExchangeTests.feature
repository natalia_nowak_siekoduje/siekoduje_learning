Feature: money conversion
  Scenario: As a user I want to exchange USD to PLN
    Given I have USD and I want PLN
    When I exchange them
    Then Exchange rate is 4.22379


  Scenario: As a user I want to exchange EUR to PLN
    Given I have EUR and I want PLN
    When I exchange them
    Then Exchange rate is 4.56749

  Scenario: As a user I want to exchange PLN to EUR
    Given I have PLN and I want EUR
    When I exchange them
    Then Exchange rate is 0.218939

  Scenario: As a user I want to exchange PLN to USD
    Given I have PLN and I want USD
    When I exchange them
    Then Exchange rate is 0.236754

  Scenario: As a user I want to exchange USD to EUR
    Given I have USD and I want EUR
    When I exchange them
    Then Exchange rate is 0.924776

  Scenario: As a user I want to exchange EUR to USD
    Given I have EUR and I want USD
    When I exchange them
    Then Exchange rate is 1.08134