import java.util.HashMap;
import java.util.Map;

public class ExchangingRatesBiMap {

    private  final Map<CurrencyPair, ExchangeRate> exchangingRates = new HashMap<>();
    private  final Map<ExchangeRate, CurrencyPair > currencyPairs = new HashMap<>();

    public void put(CurrencyPair currencyPair, ExchangeRate exchangeRate) {
        exchangingRates.put(currencyPair, exchangeRate);
        currencyPairs.put(exchangeRate, currencyPair);
    }

    public CurrencyPair findCurrencyPair(ExchangeRate exchangeRate) {
        return currencyPairs.get(exchangeRate);
    }

    public ExchangeRate findExchangingRate(CurrencyPair currencyPair) {
        return exchangingRates.get(currencyPair);
    }
}
