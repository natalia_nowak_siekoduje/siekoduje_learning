import java.util.Objects;

public class CurrencyPair {
    private Currency from;
    private Currency to;

    public CurrencyPair(Currency from, Currency to) {
        this.from = from;
        this.to = to;
    }

    public Currency getFrom() {
        return from;
    }

    public void setFrom(Currency from) {
        this.from = from;
    }

    public Currency getTo() {
        return to;
    }

    public void setTo(Currency to) {
        this.to = to;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(from) + Objects.hashCode(to);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(this.getClass() != obj.getClass())
            return false;
        CurrencyPair other = (CurrencyPair) obj;
        return (this.getFrom() == other.getFrom() && this.getTo() == other.getTo()) || (this.getTo() == other.getFrom() && this.getFrom() == other.getTo()); // czy tutaj mogę wstawiac znaki równości, czy powinnam mieć metodę equal dla currency  ?
    }
}
