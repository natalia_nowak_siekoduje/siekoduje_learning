interface CurrencyProvider {
    Double getExchangeRate(Currency from, Currency to) throws ExchangeRateUnavailableException, IllegalAccessException;
}