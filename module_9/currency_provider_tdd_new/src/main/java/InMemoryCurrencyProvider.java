import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class InMemoryCurrencyProvider implements CurrencyProvider {
   private final ExchangingRatesBiMap exchangingRatesBiMap = new ExchangingRatesBiMap();
   private final Set<Currency> validateCurrenncies = new HashSet();

//    zrobić swoj obiekt np. BiMap, gdzie mam metody pobierania elemntów po kluczu lub po wartosci

    public InMemoryCurrencyProvider() {
        exchangingRatesBiMap.put(new CurrencyPair(Currency.EUR, Currency.PLN), new ExchangeRate(4.56749,0.218939));
        exchangingRatesBiMap.put(new CurrencyPair(Currency.USD, Currency.PLN), new ExchangeRate(4.22379, 0.236754));
        exchangingRatesBiMap.put(new CurrencyPair(Currency.USD, Currency.EUR), new ExchangeRate(0.924776, 1.08134));
        addNewCurrency(Currency.PLN);
        addNewCurrency(Currency.EUR);
        addNewCurrency(Currency.USD);

    }

    @Override
    public Double getExchangeRate(Currency from, Currency to) throws ExchangeRateUnavailableException {
        Objects.requireNonNull(from, "Parameter \"from\" cannot be null");
        Objects.requireNonNull(to, "Parameter \"to\" cannot be null");
        CurrencyPair exchangingPair = new CurrencyPair(from, to);
        if(!isCurrencyValidate(from)) {
            throw new IllegalArgumentException("Unsuported source currency: " + from);
        }
        if(!isCurrencyValidate(to)) {
            throw new IllegalArgumentException("Unsuported destination currency: " + to);
        }
//        if(from != Currency.PLN && to != Currency.PLN){
//            return (1/exchangingRates.get(to)) * exchangingRates.get(from);
//        }
//        if(from == Currency.PLN){
//            return 1/exchangingRates.get(to);
//        }
        ExchangeRate rate = exchangingRatesBiMap.findExchangingRate(exchangingPair);
        if(exchangingPair.getFrom() == exchangingRatesBiMap.findCurrencyPair(rate).getFrom()) // problem w tym, ze to zawsze bedzie prawda
            return rate.getDirect();
        return rate.getInverse();
    }

    private void addNewCurrency(Currency currency) {
        validateCurrenncies.add(currency);
    }

    public boolean isCurrencyValidate(Currency currency) {
        return validateCurrenncies.contains(currency);
    }


}

