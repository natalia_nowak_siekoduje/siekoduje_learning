public class ExchangeRate {
    private Double direct;
    private Double inverse;

    public ExchangeRate(Double direct, Double inverse) {
        this.direct = direct;
        this.inverse = inverse;
    }

    public Double getDirect() {
        return direct;
    }

    public void setDirect(Double direct) {
        this.direct = direct;
    }

    public Double getInverse() {
        return inverse;
    }

    public void setInverse(Double inverse) {
        this.inverse = inverse;
    }
}
