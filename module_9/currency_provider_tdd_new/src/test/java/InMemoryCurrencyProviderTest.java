import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryCurrencyProviderTest {
    private InMemoryCurrencyProvider provider = new InMemoryCurrencyProvider();

    @Test
    public void shouldCalculateExchangeRateForUsdPln() {
        //given
        Currency from = Currency.USD;
        Currency to = Currency.PLN;
        //when
        Double result = provider.getExchangeRate(from, to);
        //then
        assertThat(result).isEqualTo(4.22379);
    }

    @Test
    public void shouldCalculateExchangeRateForEurPln() {
        //given
        Currency from = Currency.EUR;
        Currency to = Currency.PLN;
        //when
        Double result = provider.getExchangeRate(from, to);
        //then
        assertThat(result).isEqualTo(4.56749);
    }

    @Test
    public void shouldCalculateExchangeRateForUsdEur() {
        //given
        Currency from = Currency.USD;
        Currency to = Currency.EUR;
        //when
        Double result = provider.getExchangeRate(from, to);
        //then
        assertThat(result).isEqualTo(0.924776);
    }

    @Test
    public void shouldCalculateExchangeRateForEurUsd()  {
        //given
        Currency from = Currency.EUR;
        Currency to = Currency.USD;
        //when
        Double result = provider.getExchangeRate(from, to);
        //then
        assertThat(result).isEqualTo(1.08134);
    }

    @Test
    public void shouldCalculateExchangeRateForPlnUsd()  {
        //given
        Currency from = Currency.PLN;
        Currency to = Currency.USD;
        //when
        Double result = provider.getExchangeRate(from, to);
        //then
        assertThat(result).isEqualTo(0.236754);
    }

    @Test
    public void shouldCalculateExchangeRateForPlnEur() {
        //given
        Currency from = Currency.PLN;
        Currency to = Currency.EUR;
        //when
        Double result = provider.getExchangeRate(from, to);
        //then
        assertThat(result).isEqualTo(0.218939);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency() {
        //given
        Currency from = Currency.HUF;
        Currency to = Currency.PLN;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            provider.getExchangeRate(from, to);
                        }
                );
        //then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class).hasMessage("Unsuported source currency: " + from);
        }

    @Test
    public void shouldThrowIllegalArgumentExceptionForUnsupportedDestinationCurrency() {
        //given
        Currency from = Currency.USD;
        Currency to = Currency.HUF;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            provider.getExchangeRate(from, to);
                        }
                );
        //then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class).hasMessage("Unsuported destination currency: " + to);
    }

    @Test
    public void shouldThrowNullPointerExceptionForSourceCurrency() {
        //given
        Currency from = null;
        Currency to = Currency.PLN;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            provider.getExchangeRate(from, to);
                        }
                );
        //then
        assertThat(thrown).isInstanceOf(NullPointerException.class).hasMessage("Parameter \"from\" cannot be null");
    }

    @Test
    public void shouldThrowNullPointerExceptionForDestinationCurrency() {
        //given
        Currency from = Currency.USD;
        Currency to = null;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            provider.getExchangeRate(from, to);
                        }
                );
        //then
        assertThat(thrown).isInstanceOf(NullPointerException.class).hasMessage("Parameter \"to\" cannot be null");
    }
}


/*
Myślę, ze klasa powinna mieć pola z aktualnym kursem walut w stosunku do jakiejs jednej waluty i w wtedy
możemy robić konwersjęna przeróżne waluty
no i oczywisćie powinna miec możliwość ustawiania, czyli w testach musialabym sprawdzić:
1. czy wykonuje konwersje
2. czy rzuca wyjątek, w przypadku, gdy podana jest zła waluta from
3. 2 tylko z walutą 'to'
4. NPE dla oydwu przypadkow
 */
