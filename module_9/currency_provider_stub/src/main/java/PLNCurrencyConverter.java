import java.util.Objects;

public class PLNCurrencyConverter {
    private final CurrencyProvider currencyProvider;

    public PLNCurrencyConverter(CurrencyProvider currencyProvider) {
        this.currencyProvider = currencyProvider;
    }

    public Double convert(Double amount, Currency currency) {
        Objects.requireNonNull(amount, "Parameter amount cannot be null");
        Objects.requireNonNull(currency, "Parameter currency cannot be null");
        if (amount < 0) {
            throw new IllegalArgumentException("Parameter amount cannot be a negative number");
        }
        if (currency == Currency.PLN) {
            throw new IllegalArgumentException("PLN currency is not supported in this context");
        }
        return currencyProvider.getExchangeRate(currency, Currency.PLN);
    }
}
