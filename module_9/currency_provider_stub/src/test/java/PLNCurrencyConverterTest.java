import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PLNCurrencyConverterTest {
    private PLNCurrencyConverter converter;
    private CurrencyProvider providerStub;

    @Before
    public void setUp() {
        providerStub = new CurrencyProvider() {
            @Override
            public Double getExchangeRate(Currency from, Currency to) throws ExchangeRateUnavailableException {
                return 10.0;
            }
        };
        converter = new PLNCurrencyConverter(providerStub);
    }

    @Test
    public void shouldConvertMoney() {
        //given
        double amount = 10;
        Currency currency = Currency.EUR;
        //when
        double result = converter.convert(amount, currency);
        //then
        assertEquals( result, 10.0, 0);

    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForNegativeAmount() {
        //given
        double amount = -2;
        Currency currency = Currency.EUR;
        //when
        double result = converter.convert(amount, currency);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency() {
        //given
        double amount = 2;
        Currency currency = Currency.PLN;
        //when
        double result = converter.convert(amount, currency);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullCurrency() {
        //given
        double amount = 22;
        //when
        double result = converter.convert(amount, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullAmount() {
        //given
        Currency currency = Currency.EUR;
        //when
        double result = converter.convert(null, currency);
    }

}