import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PLNCurrencyConverterTest {
    private PLNCurrencyConverter plnCurrencyConverter = new PLNCurrencyConverter();

    @Test
    public void shouldConvertUsdToPln() {
        //given
        double amount = 20;
        Currency currency = Currency.USD;
        //when
        Double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertEquals(result,  78.5910202);
    }

    @Test
    public void shouldConvertEuroPln() {
        //given
        double amount = 20;
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertEquals(result,  87.23524660000001);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForNegativeAmount(){
        //given
        double amount = -2;
        Currency currency = Currency.EUR;
        //when
        IllegalArgumentException thrown =
                assertThrows(
                        IllegalArgumentException.class,
                        () -> {
                            plnCurrencyConverter.convert(amount, currency);
                        }
                );
        assertEquals("Parameter amount cannot be a negative number", thrown.getMessage());

        //then
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency(){
        //given
        double amount = 2;
        Currency currency = Currency.PLN;
        //when
        IllegalArgumentException thrown =
                assertThrows(
                        IllegalArgumentException.class,
                        () -> {
                            plnCurrencyConverter.convert(amount, currency);
                        }
                );
        //then
        assertEquals("Unsupported currency: " + currency, thrown.getMessage());
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenNullCurrency(){
        //given
        double amount = 22;
        //when
        NullPointerException thrown =
                assertThrows(
                        NullPointerException.class,
                        () -> {
                            plnCurrencyConverter.convert(amount, null);
                        }
                );
        //then
        assertEquals("Parameter currency cannot be null", thrown.getMessage());
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenNullAmount(){
        //given
        Currency currency = Currency.EUR;
        //when
        NullPointerException thrown =
                assertThrows(
                        NullPointerException.class,
                        () -> {
                            plnCurrencyConverter.convert(null, currency);
                        }
                );
        //then
        assertEquals("Parameter amount cannot be null", thrown.getMessage());
    }
}