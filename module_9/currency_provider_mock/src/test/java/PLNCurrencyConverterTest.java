import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.internal.matchers.Null;
import org.mockito.junit.MockitoJUnitRunner;


import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


public class PLNCurrencyConverterTest {

    @Mock private CurrencyProvider providerMock;
    private PLNCurrencyConverter converter;

    @Before
    public void setUp() throws Exception {
        converter = new PLNCurrencyConverter(providerMock);
    }

    // nie wiem, czy o takie coś chodziło ?
    @Test
    public void shouldTransferMoney() {
        //given
        Double amount = 20.0;
        Currency currency = Currency.EUR;
        when(providerMock.getExchangeRate(any(Currency.class), eq(Currency.PLN))).thenReturn(10.0);
        //when
       Double result = converter.convert(amount, currency);
        //then
        assertEquals(result, 10.0, 0);

    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForNegativeAmount() {
        //given
        double amount = -2;
        Currency currency = Currency.EUR;
        //when
        try{
            converter.convert(amount, currency);
            Assert.fail();
        }catch(IllegalArgumentException e){
            assertThat(e.getMessage(), equalTo("Parameter amount cannot be a negative number")); // metoda equalTo pochodzi z pakietu hamcrest, tak się powinno tego używac ?
        }
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency() {
        //given
        double amount = 2;
        Currency currency = Currency.PLN;
        //when
        try{
            converter.convert(amount, currency);
            Assert.fail();
        }catch(IllegalArgumentException e){
            assertThat(e.getMessage(), containsString("PLN currency is not supported in his context"));
        }

    }


    @Test
    public void shouldThrowNullPointerExceptionWhenNullCurrency() {
        //given
        double amount = 22;
        //when
        try{
            converter.convert(amount, null);
            Assert.fail();
        } catch(NullPointerException e) {
            assertThat(e.getMessage(), equalTo("Parameter currency cannot be null"));
        }

    }

    @Test
    public void shouldThrowNullPointerExceptionWhenNullAmount() {
        //given
        Currency currency = Currency.EUR;
        //when
        try {
            converter.convert(null, currency);
            Assert.fail();
        } catch(NullPointerException e) {
            assertThat(e.getMessage(), equalTo("Parameter amount cannot be null"));
        }

    }
}