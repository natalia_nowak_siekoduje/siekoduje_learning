import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Test;


import static org.junit.Assert.*;


public class PLNCurrencyConverterTest {

    private PLNCurrencyConverter plnCurrencyConverter = new PLNCurrencyConverter();

    @Test
    public void shouldConvertUsdToPLN() {
        //given
        double amount = 20;
        Currency currency = Currency.USD;
        //when
        Double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertEquals(result, 78.591, 0.001);
    }

    @Test
    public void shouldConvertEurToPLN() {
        //given
        double amount = 20;
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertEquals(result, 87.235, 0.001);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForNegativeAmount() {
        //given
        double amount = -2;
        Currency currency = Currency.EUR;
        //when
        try {
            plnCurrencyConverter.convert(amount, currency);
            Assert.fail("Exception not thrown");
        } catch(IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Parameter amount cannot be a negative number");
        }
    }



    @Test
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency() {
        //given
        double amount = 2;
        Currency currency = Currency.PLN;
        //when
        try {
            plnCurrencyConverter.convert(amount, currency);
            Assert.fail("Exception not thrown.");
        } catch(IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Unsupported currency: " + currency);
        }

    }



    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullCurrency() {
        //given
        double amount = 22;
        //when
        double result = plnCurrencyConverter.convert(amount, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullAmount() {
        //given
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(null, currency);
    }


}