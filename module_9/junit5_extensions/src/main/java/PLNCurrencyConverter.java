import java.util.Objects;

public class PLNCurrencyConverter {
    private static final double EUR_PLN_EXCHANGE_RATE = 4.36176233;
    private static final double USD_PLN_EXCHANGE_DATE = 3.92955101;

    public Double convert(Double amount, Currency currency) {
        Objects.requireNonNull(amount, "Parameter amount cannot be null");
        Objects.requireNonNull(currency, "Parameter currency cannot be null");
        if (amount < 0) {
            throw new IllegalArgumentException("Parameter amount cannot be a negative number");
        }
        switch (currency) {
            case EUR:
                return eurToPln(amount);
            case USD:
                return usdToPln(amount);
            default:
                throw new IllegalArgumentException("Unsupported currency: " + currency);
        }
    }

    private Double eurToPln(Double amount) {
        return amount * EUR_PLN_EXCHANGE_RATE;
    }

    private Double usdToPln(Double amount) {
        return amount * USD_PLN_EXCHANGE_DATE;
    }
}
