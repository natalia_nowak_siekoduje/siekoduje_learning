import org.junit.Test;

import static org.junit.Assert.*;

public class PLNCurrencyConverterTest {
    private PLNCurrencyConverter plnCurrencyConverter = new PLNCurrencyConverter();

    @Test
    public void shouldConvertUsdToPLN() {
        //given
        double amount = 20;
        Currency currency = Currency.USD;
        //when
        Double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertEquals("Result doesn't match", 78.591, result, 0.01);
    }

    @Test
    public void shouldConvertEurToPLN() {
        //given
        double amount = 20;
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertEquals("Result doesn't match", 87.235, result, 0.01);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForNegativeAmount() {
        //given
        double amount = -2;
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(amount, currency);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency() {
        //given
        double amount = 2;
        Currency currency = Currency.PLN;
        //when
        double result = plnCurrencyConverter.convert(amount, currency);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullCurrency() {
        //given
        double amount = 22;
        //when
        double result = plnCurrencyConverter.convert(amount, null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenNullAmount() {
        //given
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(null, currency);
    }

}