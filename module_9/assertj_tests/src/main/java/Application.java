import java.io.File;

public class Application {

    public static void main(String[] args) {
        CompressServiceImpl compressService = new CompressServiceImpl();
        try {
            compressService.compress(new File(System.getProperty("user.dir"), "dir"));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
