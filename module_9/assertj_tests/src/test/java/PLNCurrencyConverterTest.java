import org.assertj.core.api.Assert;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.data.Percentage;
import org.junit.Test;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.*;

public class PLNCurrencyConverterTest {
    private PLNCurrencyConverter plnCurrencyConverter = new PLNCurrencyConverter();

    @Test
    public void shouldConvertUsdToPLN() {
        //given
        double amount = 20;
        Currency currency = Currency.USD;
        //when
        Double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertThat(result).isEqualTo(78.5910202);
    }

    @Test
    public void shouldConvertEurToPLN() {
        //given
        double amount = 20;
        Currency currency = Currency.EUR;
        //when
        double result = plnCurrencyConverter.convert(amount, currency);
        //then
        assertThat(result).isEqualTo(87.23524660000001);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForNegativeAmount() {
        //given
        double amount = -2;
        Currency currency = Currency.EUR;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            plnCurrencyConverter.convert(amount, currency);
                        });
        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertThat(thrown).hasMessage("Parameter amount cannot be a negative number");
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionForUnsupportedCurrency() {
        //given
        double amount = 2;
        Currency currency = Currency.PLN;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            plnCurrencyConverter.convert(amount, currency);
                        });
        // then
        assertThat(thrown).isInstanceOf(IllegalArgumentException.class);
        assertThat(thrown).hasMessage("Unsupported currency: " + currency);
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenNullCurrency() {
        //given
        double amount = 22;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            plnCurrencyConverter.convert(amount, null);
                        });
        // then
        assertThat(thrown).isInstanceOf(NullPointerException.class);
        assertThat(thrown).hasMessage("Parameter currency cannot be null");
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenNullAmount() {
        //given
        Currency currency = Currency.EUR;
        //when
        Throwable thrown =
                catchThrowable(
                        () -> {
                            plnCurrencyConverter.convert(null, currency);
                        });
        // then
        assertThat(thrown).isInstanceOf(NullPointerException.class);
        assertThat(thrown).hasMessage("Parameter amount cannot be null");
    }

}