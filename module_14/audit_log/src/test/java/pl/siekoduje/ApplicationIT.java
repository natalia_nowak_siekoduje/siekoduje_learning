package pl.siekoduje;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.siekoduje.aspect.AuditLogAspect;
import pl.siekoduje.config.AppConfig;
import pl.siekoduje.model.User;
import pl.siekoduje.security.SecurityContext;
import pl.siekoduje.service.SomeService;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class ApplicationIT {

    @Autowired
    SecurityContext securityContext;

    @Autowired
    SomeService someService;

    @Autowired
    AuditLogAspect auditLogAspectSpy;

    @Before
    public void setUp() {
        User user = new User("1234");
        securityContext.setCurrentUser(user);
    }

    @Test
    public void shouldCallSomeMethod() throws Throwable {
        someService.someMethod();
        //then
        ArgumentCaptor<ProceedingJoinPoint> proceedingJoinPointArgumentCaptor = ArgumentCaptor.forClass(ProceedingJoinPoint.class);
        Mockito.verify(auditLogAspectSpy).process(proceedingJoinPointArgumentCaptor.capture());
        ProceedingJoinPoint capturedPJP = proceedingJoinPointArgumentCaptor.getValue();
        Signature signature = (Signature) capturedPJP.getSignature();
        String methodName = signature.getName();
        assertThat(methodName).isEqualTo("someMethod");
    }
}