package pl.siekoduje.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import pl.siekoduje.aspect.AuditLogAspect;

@Configuration

@Import(AppConfig.class)
public class TestConfig {


    @Primary
    @Bean
    public AuditLogAspect auditLogAspectSpy() {
        AuditLogAspect auditLogAspect = new AuditLogAspect();
        return Mockito.spy(auditLogAspect);
    }
}
