package pl.siekoduje.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import pl.siekoduje.security.SecurityContext;

@Aspect
@Slf4j
public class AuditLogAspect {

    @Autowired
    SecurityContext securityContext;

    @Pointcut("@annotation(pl.siekoduje.audit.annotation.AuditLog)")
    public void AuditLog() {
    }

    @Around(value = "AuditLog()")
    public void process(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            joinPoint.proceed();
            log.info("user {} is doing some stuff", securityContext.getCurrentUser());
        } catch (Throwable thrown) {
            log.warn("Something went wrong");
            throw thrown;
        }
    }


}
