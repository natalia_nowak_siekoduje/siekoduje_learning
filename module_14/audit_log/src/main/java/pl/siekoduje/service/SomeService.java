package pl.siekoduje.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.siekoduje.audit.annotation.AuditLog;

@Slf4j
@Component
public class SomeService {

    @AuditLog
    public void someMethod(){
        log.info("Doing some stuff");
    }
}
