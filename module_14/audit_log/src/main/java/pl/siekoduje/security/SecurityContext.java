package pl.siekoduje.security;

import org.springframework.stereotype.Component;
import pl.siekoduje.model.User;

@Component
public class SecurityContext {
    private final ThreadLocal<User> currentUser = new ThreadLocal<>();

    public User getCurrentUser() {
        return currentUser.get();
    }

    public void setCurrentUser(User user) {
        currentUser.set(user);
    }
}
