package pl.sikeoduje.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("databaseProperties")
@PropertySource("classpath:database.properties")
@Getter
public class DatabaseProperties {

    @Value("#{'${database.servers}'.split(',')}")
    private List<String> serversNames;

    @Value("#{${database.connections}}")
    private Map<String, Object> complexMap;
}
