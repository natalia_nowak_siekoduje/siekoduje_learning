package pl.sikeoduje.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {DatabaseProperties.class})
public class DatabasePropertiesIT {

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private DatabaseProperties databaseProperties;


    @Test
    public void shouldReturnServersNames() {
        //when
        List<String> names = databaseProperties.getServersNames();
        //then
        assertThat(names).hasSize(4)
                .contains("eur.1", "eur.2", "nam.1", "asia.1");
    }

    @Test
    public void shouldReturnComplexMap() {
        //when
        Map<String, Object> map = databaseProperties.getComplexMap();
        List<String> names = databaseProperties.getServersNames();
        names.stream()
                .forEach(name -> {
                    assertThat(map).containsKey(name);
                    Map<String, String> server = (Map<String, String>) map.get(name);
                    assertThat(server)
                            .hasSize(3)
                            .containsKeys("conn", "port", "user");
                });

    }

}
