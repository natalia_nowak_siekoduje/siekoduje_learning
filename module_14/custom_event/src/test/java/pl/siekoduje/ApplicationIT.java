package pl.siekoduje;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.siekoduje.model.Order;
import pl.siekoduje.service.OrderService;
import pl.siekoduje.stub.CheckingEventsStub;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Application.class})
public class ApplicationIT {

    private Order order;

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private CheckingEventsStub stub;

    @Autowired
    OrderService service;

    @Before
    public void setUp() {
       order = new Order("Natalia", "Nowak", "...", "aaaa", null);
    }

//     opcja zamiast DirtiesContext -
//    @After
//    public void tearDown() {
//        stub.setNewOrderEventCalled(false);
//        stub.setOrderPackedEventCalled(false);
//        stub.setOrderSentEventCalled(false);
//    }


    @DirtiesContext
    @Test
    public void shouldPublishNewOrderEvent() {
        //given
        //when
        service.newOrder(order);
        //then
        assertThat(stub.isNewOrderEventCalled()).isTrue();
        assertThat(stub.isOrderPackedEventCalled()).isFalse();
        assertThat(stub.isOrderSentEventCalled()).isFalse();
    }

    @DirtiesContext
    @Test
    public void shouldPublishOrderPackedEvent() {
        //given
        //when
        service.packOrder(order);
        //then
        assertThat(stub.isNewOrderEventCalled()).isFalse();
        assertThat(stub.isOrderPackedEventCalled()).isTrue();
        assertThat(stub.isOrderSentEventCalled()).isFalse();
    }

    @DirtiesContext
    @Test
    public void shouldPublishOrderSentEvent() {
        //given
        //when
        service.sendOrder(order);
        //then
        assertThat(stub.isNewOrderEventCalled()).isFalse();
        assertThat(stub.isOrderPackedEventCalled()).isFalse();
        assertThat(stub.isOrderSentEventCalled()).isTrue();
    }

}