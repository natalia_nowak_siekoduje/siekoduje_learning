package pl.siekoduje.stub;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component("stub")
public class CheckingEventsStub {
    private boolean newOrderEventCalled = false;
    private boolean orderSentEventCalled = false;
    private boolean orderPackedEventCalled = false;

    public void onNewOrderEvent() {
        newOrderEventCalled = true;
    }

    public void onOrderSentEvent() {
        orderSentEventCalled = true;
    }

    public void OrderPackedEvent() {
        orderPackedEventCalled = true;
    }
}
