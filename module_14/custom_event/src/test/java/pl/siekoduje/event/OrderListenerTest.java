package pl.siekoduje.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.siekoduje.stub.CheckingEventsStub;

@Slf4j
@RequiredArgsConstructor
@Component
public class OrderListenerTest {

    @Autowired
    private CheckingEventsStub stub;

    @EventListener
    public void onOrderSending(OrderSentEvent event) {
        stub.onOrderSentEvent();
    }

    @EventListener
    public void onOrderPacking(OrderPackedEvent event) {
        stub.OrderPackedEvent();
    }

    @EventListener
    public void onNewOrder(NewOrderEvent event) {
        stub.onNewOrderEvent();
    }
}
