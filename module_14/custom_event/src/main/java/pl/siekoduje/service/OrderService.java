package pl.siekoduje.service;

import pl.siekoduje.model.Order;

public interface OrderService {
    /**
     * Process new order.
     *
     * @param order
     */
    void newOrder(Order order);

    /**
     * Process being packed.
     *
     * @param order
     */
    void packOrder(Order order);

    /**
     * Process order being sent.
     *
     * @param order
     */
    void sendOrder(Order order);
}
