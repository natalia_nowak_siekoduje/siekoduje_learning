package pl.siekoduje.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import pl.siekoduje.event.NewOrderEvent;
import pl.siekoduje.event.OrderListener;
import pl.siekoduje.event.OrderPackedEvent;
import pl.siekoduje.event.OrderSentEvent;
import pl.siekoduje.model.Order;
import pl.siekoduje.model.OrderStatus;

@RequiredArgsConstructor
@Component
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void newOrder(Order order) {
        order.setOrderStatus(OrderStatus.CREATED);
        NewOrderEvent event = new NewOrderEvent(this, order);
        applicationEventPublisher.publishEvent(event);
    }

    @Override
    public void packOrder(Order order) {
        order.setOrderStatus(OrderStatus.PACKED);
        OrderPackedEvent event = new OrderPackedEvent(this, order);
        applicationEventPublisher.publishEvent(event);
    }

    @Override
    public void sendOrder(Order order) {
        order.setOrderStatus(OrderStatus.SENT);
        OrderSentEvent event = new OrderSentEvent(this, order);
        applicationEventPublisher.publishEvent(event);
    }


}
