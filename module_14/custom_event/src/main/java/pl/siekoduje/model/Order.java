package pl.siekoduje.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.List;

@RequiredArgsConstructor
@Getter
@Setter
public class Order {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final String address;
    private final List<String> orderedItems;
    private OrderStatus orderStatus;

    @Override
    public String toString() {
        return "Order{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", orderStatus=" + orderStatus +
                '}';
    }
}
