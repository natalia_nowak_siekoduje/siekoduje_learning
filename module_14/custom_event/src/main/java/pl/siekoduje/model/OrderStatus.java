package pl.siekoduje.model;

public enum OrderStatus {
    CREATED,
    PACKED,
    SENT
}
