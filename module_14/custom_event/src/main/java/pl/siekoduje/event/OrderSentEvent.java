package pl.siekoduje.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import pl.siekoduje.model.Order;

@Getter
public class OrderSentEvent extends ApplicationEvent {
    private final Order order;

    public OrderSentEvent(Object source, Order order) {
        super(source);
        this.order = order;
    }
}
