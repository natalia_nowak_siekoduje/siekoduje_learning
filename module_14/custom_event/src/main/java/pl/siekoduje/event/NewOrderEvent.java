package pl.siekoduje.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import pl.siekoduje.model.Order;

@Getter
public class NewOrderEvent extends ApplicationEvent {
    private final Order order;

    public NewOrderEvent(Object source, Order order) {
        super(source);
        this.order = order;
    }
}
