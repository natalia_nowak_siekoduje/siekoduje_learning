package pl.siekoduje.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component("listener")
public class OrderListener {

    @EventListener
    public void onOrderSending(OrderSentEvent event) {
        log.info("Order {} was sent.", event.getOrder());
    }

    @EventListener
    public void onOrderPacking(OrderPackedEvent event) {
        log.info("Order {} was packed.", event.getOrder());
    }

    @EventListener
    public void onNewOrder(NewOrderEvent event) {
        log.info("New order was registered: {}", event.getOrder());
    }
}
