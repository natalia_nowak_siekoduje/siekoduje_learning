package pl.siekoduje;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import pl.siekoduje.model.Order;
import pl.siekoduje.service.OrderService;

@Configuration
@ComponentScan
public class Application {

    public static void main(String[] args) {

    }

}
