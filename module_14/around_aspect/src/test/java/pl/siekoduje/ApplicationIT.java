package pl.siekoduje;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.siekoduje.config.AppConfig;
import pl.siekoduje.model.Account;
import pl.siekoduje.service.AccountService;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AppConfig.class})
public class ApplicationIT {

    @Autowired
    AccountService service;

    @Test
    public void shouldCreateAccount() {
        //when
        Account result = service.createAccount("Natalia", "1234");
        //then
        assertThat(result.getLogin()).isEqualTo("Natalia");
        assertThat(result.getPassword()).isEqualTo("1234");
    }

}