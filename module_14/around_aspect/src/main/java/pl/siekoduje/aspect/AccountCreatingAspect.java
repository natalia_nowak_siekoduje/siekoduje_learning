package pl.siekoduje.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import pl.siekoduje.model.Account;

@Aspect
@Component
@Slf4j
public class AccountCreatingAspect {
    @Pointcut("execution( public pl.siekoduje.model.Account pl.siekoduje.service..*Service.createAccount(String, String))")
    public void accountCreatingPointcut() {
    }

    @Around(value = "accountCreatingPointcut()")
    public Account aroundAdvice(ProceedingJoinPoint pjp) throws Throwable {
        log.info("Before creating account");
        Account createdAccount = null;
        try {
             createdAccount =  (Account) pjp.proceed();

        } catch (Throwable thrown) {
            log.error("error", thrown);
            throw thrown;
        }
        log.info("After creating account");
        return createdAccount;
    }
}
