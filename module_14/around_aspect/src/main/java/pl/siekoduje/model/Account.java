package pl.siekoduje.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class Account {
    private final String login;
    private final String password;

}
