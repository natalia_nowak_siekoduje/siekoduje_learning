package pl.siekoduje.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import pl.siekoduje.model.Account;

@Slf4j
@Component("accountService")
public class AccountService {
    public Account createAccount(String login, String password) {
        log.info("Creating account {}", login);
        return new Account(login, password);
    }
}
