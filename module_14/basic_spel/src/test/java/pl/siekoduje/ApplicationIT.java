package pl.siekoduje;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.siekoduje.config.EnvironmentInfo;

import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {EnvironmentInfo.class})
public class ApplicationIT {

    private Properties properties;

    @Autowired
    private EnvironmentInfo environmentInfo;

    @Before
    public void setUp() {
        properties = environmentInfo.getSystemProperties();
    }

    @Test
    public void shouldReturnSystemName() {
        //when
        String result = environmentInfo.getSystemNameProperty();
        //then
        assertThat(result).isEqualTo("Windows 10");
    }

    @Test
    public void shouldReturnSystemVersion() {
        //when
        String result = environmentInfo.getSystemVersionProperty();
        //then
        assertThat(result).isEqualTo("10.0");
    }

    @Test
    public void shouldReturnSystemArchitecture() {
        //when
        String result = environmentInfo.getSystemArchitectureProperty();
        //then
        assertThat(result).isEqualTo("amd64");
    }

    @Test
    public void shouldReturnUserDir() {
        //when
        String result = environmentInfo.getUserDirProperty();
        //then
        assertThat(result).isEqualTo(System.getProperty("user.dir"));
    }

    @Test
    public void shouldReturnApplicationName() {
        //when
        String result = environmentInfo.getApplicationName();
        //then
        assertThat(result).isEqualTo("basic_spel");
    }

    @Test
    public void shouldReturnApplicationVersion() {
        //when
        String result = environmentInfo.getApplicationVersion();
        //then
        assertThat(result).isEqualTo("1.0.0");
    }

    @Test
    public void shouldReturnJavaVersion() {
        //when
        String result = environmentInfo.getJavaVersionProperty();
        //then
        assertThat(result).isEqualTo("13.0.2");
    }

}