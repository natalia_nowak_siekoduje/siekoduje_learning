package pl.siekoduje;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.siekoduje.config.EnvironmentInfo;
import pl.siekoduje.utils.EnvironmentPrinter;

public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(EnvironmentInfo.class.getPackageName());
        EnvironmentPrinter printer = context.getBean(EnvironmentPrinter.class);
        printer.logInfo();
    }
}
