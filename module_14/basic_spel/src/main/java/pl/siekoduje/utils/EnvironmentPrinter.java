package pl.siekoduje.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.siekoduje.config.EnvironmentInfo;


@Component
@Slf4j
public class EnvironmentPrinter {

    private final EnvironmentInfo environmentInfo;

    @Autowired
    public EnvironmentPrinter(EnvironmentInfo environmentInfo) {
        this.environmentInfo = environmentInfo;
    }

    public void logInfo() {
        log.info("system name: " + environmentInfo.getSystemNameProperty());
        log.info("system version: " + environmentInfo.getSystemVersionProperty());
        log.info("system architecture: " + environmentInfo.getSystemArchitectureProperty());
        log.info("user directory: " + environmentInfo.getUserDirProperty());
        log.info("application name : " + environmentInfo.getApplicationName());
        log.info("application version: " + environmentInfo.getApplicationVersion());
        log.info("java version: " + environmentInfo.getJavaVersionProperty());
    }
}
