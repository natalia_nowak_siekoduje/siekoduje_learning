package pl.siekoduje.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component
@Getter
@PropertySource("classpath:application.properties")
public class EnvironmentInfo {

    @Value("#{systemProperties}")
    private Properties systemProperties;

    @Value("#{systemProperties['os.name']}")
    private String systemNameProperty;

    @Value("#{systemProperties['os.version']}")
    private String systemVersionProperty;

    @Value("#{systemProperties['os.arch']}")
    private String systemArchitectureProperty;

    @Value("#{systemProperties['user.dir']}")
    private String userDirProperty;

    @Value("${application.name}")
    private String applicationName;

    @Value("${application.version}")
    private String applicationVersion;

    @Value("#{systemProperties['java.version']}")
    private String javaVersionProperty;
}
