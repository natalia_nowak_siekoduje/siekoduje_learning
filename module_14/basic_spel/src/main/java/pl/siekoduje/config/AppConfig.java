package pl.siekoduje.config;


import lombok.Getter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@Getter
@Configuration
@ComponentScan(basePackages = "pl.siekoduje")
public class AppConfig {
}
