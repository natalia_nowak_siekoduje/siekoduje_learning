package pl.siekoduje.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
@Slf4j
public class ContextEventsListener {
    private int refreshCounter = 0;
    private int startCounter = 0;
    private int stopCounter = 0;
    private int closeCounter = 0;
        @EventListener
        public void onContextRefreshedEvent(ContextRefreshedEvent event) {
            refreshCounter++;
            log.info("ContextRefreshedEvent called {} times.", refreshCounter);
        }

        @EventListener
        public void onContextStartedEvent(ContextStartedEvent event) {
            startCounter++;
            log.info("ContextStartedEvent called {} times", startCounter);
        }

        @EventListener
        public void onContextStoppedEvent(ContextStoppedEvent event) {
            stopCounter++;
            log.info("ContextStoppedEvent called {} times", stopCounter);
        }

        @EventListener
        public void onContextClosedEvent(ContextClosedEvent event) {
            closeCounter++;
            log.info("ContextClosedEvent called {} times", closeCounter);
        }
}
