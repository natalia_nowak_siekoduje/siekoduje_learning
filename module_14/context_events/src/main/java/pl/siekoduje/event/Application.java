package pl.siekoduje.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan
@Configuration
@Slf4j
public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(Application.class);
        log.info("Refreshing application context");
        ctx.refresh();
        log.info("Starting application context");
        ctx.start();
        log.info("Stopping application context");
        ctx.stop();
        log.info("Closing application context");
        ctx.close();
    }
}
