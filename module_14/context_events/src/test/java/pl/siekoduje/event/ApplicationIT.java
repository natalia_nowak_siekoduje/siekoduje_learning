package pl.siekoduje.event;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;



public class ApplicationIT {
    private  AnnotationConfigApplicationContext ctx;

    @Autowired
    ContextEventsListener listener;

    @Before
    public  void setUp() {
        ctx = new AnnotationConfigApplicationContext();
        ctx.register(Application.class);
    }

    @Test
    public void shouldThrowIllegalStateExceptionOnTwoTimesRefresh() {
        //when
        ctx.refresh();
        Throwable thrown = catchThrowable(() ->{
            ctx.refresh();
        });
        //then
        assertThat(thrown).isInstanceOf(IllegalStateException.class);
    }

    @Test
    public void shouldCallTwoTimesStart() {
        //when
        ctx.refresh();
        ctx.start();
        ctx.start();

    }

    @Test
    public void shouldCallTwoTimesStop() {
        //when
        ctx.refresh();
        ctx.stop();
        ctx.stop();

    }

    @Test
    public void shouldCallTwoTimesClose() {
        //when
        ctx.refresh();
        ctx.close();
        ctx.close();

    }

    @Test
    public void shouldCallStartAfterStop() {
        //when
        ctx.refresh();
        ctx.start();
        ctx.stop();
        ctx.start();

    }

    @Test
    public void shouldCallStartAfterCloseAndThrowException() {
        //when
        ctx.refresh();
        ctx.start();
        ctx.close();

        Throwable thrown = catchThrowable(() ->{
            ctx.start();
        });
        //then
        assertThat(thrown).isInstanceOf(IllegalStateException.class);

    }
}